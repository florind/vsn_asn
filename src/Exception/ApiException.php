<?php
namespace App\Exception;

use Symfony\Component\HttpFoundation\Response;

abstract class ApiException extends \Exception
{
    protected $message = 'ASN_API_EXCEPTION';
    protected $code = Response::HTTP_INTERNAL_SERVER_ERROR;
    private $details = [];

    public function getDetails()
    {
        return $this->details;
    }

    public function setDetails(array $details)
    {
        $this->details = $details;
    }
}