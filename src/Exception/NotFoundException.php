<?php
namespace App\Exception;

use Symfony\Component\HttpFoundation\Response;

class NotFoundException extends ApiException
{
    protected $message = 'ASN_NOT_FOUND';
    protected $code = Response::HTTP_NOT_FOUND;
}