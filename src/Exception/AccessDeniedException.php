<?php
namespace App\Exception;

use Symfony\Component\HttpFoundation\Response;

class AccessDeniedException extends ApiException
{
    protected $message = 'ASN_ACCESS_DENIED';
    protected $code = Response::HTTP_FORBIDDEN;
}