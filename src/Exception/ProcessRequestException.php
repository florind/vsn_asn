<?php
namespace App\Exception;

use Symfony\Component\HttpFoundation\Response;

class ProcessRequestException extends ApiException
{
    protected $message = 'ASN_REQUEST_PROCESS_ERROR';
    protected $code = Response::HTTP_INTERNAL_SERVER_ERROR;
}