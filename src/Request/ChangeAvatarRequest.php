<?php
namespace App\Request;

use App\Helper\Gender;
use App\Node\Profile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints as Assert;
use App\Validator\Constraints as Custom;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

class ChangeAvatarRequest
{
    /**
     * @Assert\NotBlank(
     *     message=CreateProfileRequest::ERROR_EMPTY
     * )
     */
    private $avatar;

    public function __construct(Request $request)
    {
        $this->avatar = $request->get('avatar');
    }

    public function getAvatar()
    {
        return $this->avatar;
    }
}