<?php
namespace App\Request\Vehicle;

use App\Validator\Constraints\Vehicle;
use Symfony\Component\HttpFoundation\Request;
use App\Node\Car;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

/**
 * @Vehicle()
 */
class UpdateCarRequest implements CarDataInterface
{
    /**
     * @Assert\NotBlank()
     */
    private $carHash;

    private $type;

    private $model;

    private $brand;

    private $body;

    private $year;

    /**
     * @Assert\Choice({Car::FUEL_PETROL, Car::FUEL_DIESEL, Car::FUEL_HYBRID, Car::FUEL_ELECTRIC, Car::FUEL_LPG})
     */
    private $fuel;

    private $engineCapacity;

    private $horsepower;

    private $mileage;

    /**
     * @Assert\Choice({Car::TRANSMISSION_MANUAL, Car::TRANSMISSION_SEMIAUTOMATIC, Car::TRANSMISSION_AUTOMATIC, Car::TRANSMISSION_CVT, Car::TRANSMISSION_DSG, Car::TRANSMISSION_TIPTRONIC})
     */
    private $transmission;

    private $features;

    private $media;

    private $vin;

    public function __construct(Request $request)
    {
        $this->carHash = $request->get('carHash', null);

        $data = json_decode($request->getContent(), true);
        $this->type = array_key_exists(Car::PROP_TYPE, $data) ? $data[Car::PROP_TYPE] : null;
        $this->model = array_key_exists(Car::PROP_MODEL, $data) ? $data[Car::PROP_MODEL] : null;
        $this->brand = array_key_exists(Car::PROP_BRAND, $data) ? $data[Car::PROP_BRAND] : null;
        $this->body = array_key_exists(Car::PROP_BODY, $data) ? $data[Car::PROP_BODY] : null;
        $this->year = array_key_exists(Car::PROP_YEAR, $data) ? $data[Car::PROP_YEAR] : null;
        $this->fuel = array_key_exists(Car::PROP_FUEL, $data) ? $data[Car::PROP_FUEL] : null;
        $this->engineCapacity = array_key_exists(Car::PROP_CC, $data) ? $data[Car::PROP_CC] : null;
        $this->horsepower = array_key_exists(Car::PROP_HP, $data) ? $data[Car::PROP_HP] : null;
        $this->mileage = array_key_exists(Car::PROP_MILEAGE, $data) ? $data[Car::PROP_MILEAGE] : null;
        $this->transmission = array_key_exists(Car::PROP_TRANSMISSION, $data) ? $data[Car::PROP_TRANSMISSION] : null;
        $this->features = array_key_exists(Car::PROP_FEATURES, $data) ? $data[Car::PROP_FEATURES] : null;
        $this->vin = array_key_exists(Car::PROP_VIN, $data) ? $data[Car::PROP_VIN] : null;
        $this->media = array_key_exists(Car::PROP_MEDIA, $data) ? $data[Car::PROP_MEDIA] : [];
    }

    public function getType()
    {
        return $this->type;
    }

    public function getModel()
    {
        return $this->model;
    }

    public function getBrand()
    {
        return $this->brand;
    }

    public function getBody()
    {
        return $this->body;
    }

    public function getYear()
    {
        return $this->year;
    }

    public function getFuel()
    {
        return $this->fuel;
    }

    public function getEngineCapacity()
    {
        return $this->engineCapacity;
    }

    public function getHorsepower()
    {
        return $this->horsepower;
    }

    public function getMileage()
    {
        return $this->mileage;
    }

    public function getTransmission()
    {
        return $this->transmission;
    }

    public function getFeatures()
    {
        return $this->features;
    }

    public function getVin()
    {
        return $this->vin;
    }

    public function getCarHash()
    {
        return $this->carHash;
    }

    public function getMedia()
    {
        return $this->media;
    }

    /**
     * @Assert\Callback()
     * @param ExecutionContextInterface $context
     * @param $payload
     */
    public function validate(ExecutionContextInterface $context, $payload)
    {
        if (
            null === $this->type &&
            null === $this->features &&
            null === $this->transmission &&
            null === $this->mileage &&
            null === $this->horsepower &&
            null === $this->engineCapacity &&
            null === $this->fuel &&
            null === $this->year &&
            null === $this->model &&
            null === $this->brand &&
            null === $this->vin &&
            null === $this->body
        ) {
            $context->buildViolation('ASN_ERROR_AT_LEAST_ONE')
                ->atPath('all')
                ->addViolation();
        }

        if (
            ( null !== $this->type || null !== $this->brand || null !== $this->model ) &&
            in_array(null, [$this->type, $this->brand, $this->model])
        ) {
            $context->buildViolation('ASN_ERROR_TYPE_BRAND_MODEL')
                ->atPath('other')
                ->addViolation();
        }
    }
}