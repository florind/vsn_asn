<?php
namespace App\Request\Vehicle;

use App\Validator\Constraints\Vehicle;
use Symfony\Component\HttpFoundation\Request;
use App\Node\Car;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @Vehicle()
 */
class CreateCarRequest implements CarDataInterface
{
    /**
     * @Assert\NotBlank()
     * @Assert\EqualTo(value=Car::VEHICLE_TYPE)
     */
    private $type;

    /** @Assert\NotBlank() */
    private $model;

    /** @Assert\NotBlank() */
    private $brand;

    private $body;

    /** @Assert\NotBlank() */
    private $year;

    /**
     * @Assert\NotBlank()
     * @Assert\Choice({Car::FUEL_PETROL, Car::FUEL_DIESEL, Car::FUEL_HYBRID, Car::FUEL_ELECTRIC, Car::FUEL_LPG})
     */
    private $fuel;

    /** @Assert\NotBlank() */
    private $engineCapacity;

    /** @Assert\NotBlank() */
    private $horsepower;

    private $mileage;

    /**
     * @Assert\NotBlank()
     * @Assert\Choice({Car::TRANSMISSION_MANUAL, Car::TRANSMISSION_SEMIAUTOMATIC, Car::TRANSMISSION_AUTOMATIC, Car::TRANSMISSION_CVT, Car::TRANSMISSION_DSG, Car::TRANSMISSION_TIPTRONIC})
     */
    private $transmission;

    private $features;

    private $media;

    private $vin;

    public function __construct(Request $request)
    {
        $this->type = $request->get(Car::PROP_TYPE, null);
        $this->model = $request->get(Car::PROP_MODEL, null);
        $this->brand = $request->get(Car::PROP_BRAND, null);
        $this->body = $request->get(Car::PROP_BODY, null);
        $this->year = $request->get(Car::PROP_YEAR, null);
        $this->fuel = $request->get(Car::PROP_FUEL, null);
        $this->engineCapacity = $request->get(Car::PROP_CC, null);
        $this->horsepower = $request->get(Car::PROP_HP, null);
        $this->mileage = $request->get(Car::PROP_MILEAGE, null);
        $this->transmission = $request->get(Car::PROP_TRANSMISSION, null);
        $this->features = $request->get(Car::PROP_FEATURES, null);
        $this->vin = $request->get(Car::PROP_VIN, null);
        $this->media = $request->get(Car::PROP_MEDIA, []);
    }

    public function getType()
    {
        return $this->type;
    }

    public function getModel()
    {
        return $this->model;
    }

    public function getBrand()
    {
        return $this->brand;
    }

    public function getBody()
    {
        return $this->body;
    }

    public function getYear()
    {
        return $this->year;
    }

    public function getFuel()
    {
        return $this->fuel;
    }

    public function getEngineCapacity()
    {
        return $this->engineCapacity;
    }

    public function getHorsepower()
    {
        return $this->horsepower;
    }

    public function getMileage()
    {
        return $this->mileage;
    }

    public function getTransmission()
    {
        return $this->transmission;
    }

    public function getFeatures()
    {
        return $this->features;
    }

    public function getVin()
    {
        return $this->vin;
    }

    public function getMedia()
    {
        return $this->media;
    }
}