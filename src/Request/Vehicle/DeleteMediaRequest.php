<?php
namespace App\Request\Vehicle;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints as Assert;

class DeleteMediaRequest
{
    /** @Assert\NotBlank() */
    private $nodeHash;
    /** @Assert\NotBlank() */
    private $mediaHash;

    public function __construct(Request $request)
    {
        $this->nodeHash = $request->get('nodeHash');
        $this->mediaHash = $request->get('mediaHash');
    }

    public function getNodeHash()
    {
        return $this->nodeHash;
    }

    public function getMediaHash()
    {
        return $this->mediaHash;
    }
}