<?php
namespace App\Request\Vehicle;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints as Assert;

class AddMediaRequest
{
    /** @Assert\NotBlank() */
    private $nodeHash;
    /** @Assert\NotBlank() */
    private $media;

    public function __construct(Request $request)
    {
        $this->nodeHash = $request->get('nodeHash');
        $this->media = $request->get('media', []);
    }

    public function getNodeHash()
    {
        return $this->nodeHash;
    }

    public function getMedia()
    {
        return $this->media;
    }
}