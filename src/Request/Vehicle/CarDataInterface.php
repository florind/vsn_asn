<?php
namespace App\Request\Vehicle;

interface CarDataInterface
{
    function getType();
    function getModel();
    function getBrand();
    function getBody();
    function getYear();
    function getFuel();
    function getEngineCapacity();
    function getHorsepower();
    function getMileage();
    function getTransmission();
    function getFeatures();
    function getVin();
}