<?php
namespace App\Request\Reply;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints as Assert;

class DeleteReplyRequest
{
    /**
     * @Assert\NotBlank()
     */
    private $club;

    /**
     * @Assert\NotBlank()
     */
    private $topic;

    /**
     * @Assert\NotBlank()
     */
    private $comment;

    /**
     * @Assert\NotBlank()
     */
    private $reply;

    public function __construct(Request $request)
    {
        $this->club = $request->get('club');
        $this->topic = $request->get('topic');
        $this->comment = $request->get('comment');
        $this->reply = $request->get('reply');
    }

    public function getClub()
    {
        return $this->club;
    }

    public function getTopic()
    {
        return $this->topic;
    }

    public function getComment()
    {
        return $this->comment;
    }

    public function getReply()
    {
        return $this->reply;
    }
}