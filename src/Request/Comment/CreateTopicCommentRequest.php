<?php
namespace App\Request\Comment;

use App\Node\Comment;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

class CreateTopicCommentRequest
{
    /**
     * @Assert\NotBlank()
     */
    private $club;

    /**
     * @Assert\NotBlank()
     */
    private $topic;

    private $text;
    private $media;

    public function __construct(Request $request)
    {
        $this->club = $request->get('club');
        $this->topic = $request->get('topic');
        $this->text = $request->get(Comment::PROP_TEXT);
        $this->media = $request->get(Comment::PROP_MEDIA, []);
    }

    public function getClub()
    {
        return $this->club;
    }

    public function getTopic()
    {
        return $this->topic;
    }

    public function getText()
    {
        return $this->text;
    }

    public function getMedia()
    {
        return $this->media;
    }

    /**
     * @Assert\Callback()
     */
    public function validate(ExecutionContextInterface $context, $payload)
    {
        if (
            empty($this->text) &&
            empty($this->media)
        ) {
            $context->buildViolation('ASN_ERROR_EMPTY_COMMENT')
                ->atPath('all')
                ->addViolation();
        }
    }
}