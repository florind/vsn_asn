<?php
namespace App\Request\Comment;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

class ListTopicCommentsRequest
{
    /**
     * @Assert\NotBlank()
     */
    private $club;

    /**
     * @Assert\NotBlank()
     */
    private $topic;

    private $page;
    private $limit;
    private $refDate;

    public function __construct(Request $request)
    {
        $this->club = $request->get('club');
        $this->topic = $request->get('topic');
        $this->page = $request->get('page', 1);
        $this->limit = $request->get('limit', 10);
        $this->refDate = $request->get('refDate', (new \DateTime())->format("Y-m-d H:i:s"));
    }

    public function getClub()
    {
        return $this->club;
    }

    public function getTopic()
    {
        return $this->topic;
    }

    public function getPage()
    {
        return $this->page;
    }

    public function getLimit()
    {
        return $this->limit;
    }

    public function getRefDate()
    {
        return $this->refDate;
    }

    public function validate(ExecutionContextInterface $context, $payload)
    {
        if (!is_numeric($this->page) || $this->page < 0) {
            $context->buildViolation('The "page" parameter must be a number bigger than 0.')
                ->atPath('page')
                ->addViolation();
        }

        if (!is_numeric($this->limit) || $this->limit < 0) {
            $context->buildViolation('The "page" parameter must be a number bigger than 0.')
                ->atPath('limit')
                ->addViolation();
        }

        $date = \DateTime::createFromFormat('Y-m-d H:i:s', $this->refDate);

        if (false === $date) {
            $context->buildViolation('The "refDate" parameter must be a a valid date.')
                ->atPath('refDate')
                ->addViolation();
        }
    }
}