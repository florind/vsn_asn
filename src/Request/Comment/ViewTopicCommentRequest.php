<?php
namespace App\Request\Comment;

use App\Node\Comment;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

class ViewTopicCommentRequest
{
    /**
     * @Assert\NotBlank()
     */
    private $club;

    /**
     * @Assert\NotBlank()
     */
    private $topic;

    /**
     * @Assert\NotBlank()
     */
    private $comment;

    public function __construct(Request $request)
    {
        $this->club = $request->get('club');
        $this->topic = $request->get('topic');
        $this->comment = $request->get('comment');
    }

    public function getClub()
    {
        return $this->club;
    }

    public function getTopic()
    {
        return $this->topic;
    }

    public function getComment()
    {
        return $this->comment;
    }
}