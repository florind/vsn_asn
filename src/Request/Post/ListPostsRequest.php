<?php
namespace App\Request\Post;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints as Assert;

class ListPostsRequest
{
    /**
     * @Assert\NotBlank()
     */
    private $profileHash;

    public function __construct(Request $request)
    {
        $this->profileHash = $request->get('profileHash', null);
    }

    public function getProfileHash()
    {
        return $this->profileHash;
    }
}