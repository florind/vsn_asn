<?php
namespace App\Request\Post;

use App\Node\Post;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

class CreatePostRequest
{
    private $text;

    private $media;

    public function __construct(Request $request)
    {
        $this->text = $request->get(Post::PROP_TEXT, null);
        $this->media = $request->get(Post::PROP_MEDIA, []);
    }

    public function getText()
    {
        return $this->text;
    }

    public function getMedia()
    {
        return $this->media;
    }

    /**
     * @Assert\Callback()
     */
    public function validate(ExecutionContextInterface $context, $payload)
    {
        if (
            empty($this->text) &&
            empty($this->media)
        ) {
            $context->buildViolation('ASN_ERROR_EMPTY_POST')
                ->atPath('all')
                ->addViolation();
        }
    }
}