<?php
namespace App\Request\Post;

use App\Node\Post;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

class UpdatePostRequest
{
    /**
     * @Assert\NotBlank()
     */
    private $postHash;

    private $text;

    private $media;

    public function __construct(Request $request)
    {
        $this->postHash = $request->get('postHash', null);

        $data = json_decode($request->getContent(), true);
        $this->text = array_key_exists(Post::PROP_TEXT, $data) ? $data[Post::PROP_TEXT] : null;
        $this->media = array_key_exists(Post::PROP_MEDIA, $data) ? $data[Post::PROP_MEDIA] : [];
    }

    public function getText()
    {
        return $this->text;
    }

    public function getMedia()
    {
        return $this->media;
    }

    public function getPostHash()
    {
        return $this->postHash;
    }

    /**
     * @Assert\Callback()
     */
    public function validate(ExecutionContextInterface $context, $payload)
    {
        if (
            empty($this->text) &&
            empty($this->media)
        ) {
            $context->buildViolation('ASN_ERROR_EMPTY_POST')
                ->atPath('all')
                ->addViolation();
        }
    }
}