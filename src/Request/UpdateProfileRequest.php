<?php
namespace App\Request;

use App\Helper\Gender;
use App\Node\Profile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints as Assert;
use App\Validator\Constraints as Custom;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

class UpdateProfileRequest
{
    const ERROR_UPDATE = "ASN_ERROR_UPDATE_AT_LEAST_ONE";
    const ERROR_TOKEN = "ASN_ERROR_TOKEN";

    /**
     * @Assert\NotBlank(
     *     message=CreateProfileRequest::ERROR_EMPTY
     * )
     */
    private $firstName;

    /**
     * @Assert\NotBlank(
     *     message=CreateProfileRequest::ERROR_EMPTY
     * )
     */
    private $lastName;

    /**
     * @Assert\NotBlank(
     *     message=CreateProfileRequest::ERROR_EMPTY
     * )
     * @Assert\Choice(
     *     choices={App\Helper\Gender::TYPE_MALE, App\Helper\Gender::TYPE_FEMALE},
     *     message=CreateProfileRequest::ERROR_INVALID_GENDER
     * )
     */
    private $gender;

    /**
     * @Assert\NotBlank(
     *     message=CreateProfileRequest::ERROR_EMPTY
     * )
     */
    private $birthday;

    /** @var string
     * @Custom\Locality()
     */
    private $locality;

    public function __construct(Request $request)
    {
        $data = json_decode($request->getContent(), true);
        $this->firstName = array_key_exists(Profile::PROP_FIRST_NAME, $data) ? $data[Profile::PROP_FIRST_NAME] : null;
        $this->lastName = array_key_exists(Profile::PROP_LAST_NAME, $data) ? $data[Profile::PROP_LAST_NAME] : null;
        $this->gender = array_key_exists(Profile::PROP_GENDER, $data) ? $data[Profile::PROP_GENDER] : null;
        $this->birthday = array_key_exists(Profile::PROP_BIRTHDAY, $data) ? $data[Profile::PROP_BIRTHDAY] : null;
        $this->locality = array_key_exists(Profile::PROP_LOCALITY, $data) ? $data[Profile::PROP_LOCALITY] : null;
    }

    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    public function getGender(): ?Gender
    {
        return null === $this->gender ? null : new Gender($this->gender);
    }

    public function getBirthday(): ?\DateTime
    {
        return null === $this->birthday ? null : \DateTime::createFromFormat('Y-m-d', $this->birthday);
    }

    public function getLocality(): ?string
    {
        return $this->locality;
    }

    /**
     * @Assert\Callback()
     */
    public function validate(ExecutionContextInterface $context, $payload)
    {
        if (null !== $this->birthday) {
            $birthday = \DateTime::createFromFormat('Y-m-d', $this->birthday);

            if (empty($birthday)) {
                $context->buildViolation(CreateProfileRequest::ERROR_INVALID_DATE)
                    ->atPath('birthday')
                    ->addViolation();

                return;
            }

            $today = new \DateTime();

            if ($today->diff($birthday)->y <= 16) {
                $context->buildViolation(CreateProfileRequest::ERROR_BIRTHDAY)
                    ->atPath('birthday')
                    ->addViolation();
            }
        }

        if (
            null === $this->firstName &&
            null === $this->lastName &&
            null === $this->gender &&
            null === $this->birthday &&
            null === $this->avatar &&
            null === $this->locality
        ) {
            $context->buildViolation(self::ERROR_UPDATE)
                ->atPath('all')
                ->addViolation();
        }
    }
}