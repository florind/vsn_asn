<?php
namespace App\Request;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints as Assert;

class NodeRelatedRequest
{
    /**
     * @Assert\NotBlank()
     */
    private $nodeHash;

    public function __construct(Request $request)
    {
        $this->nodeHash = $request->get('nodeHash', null);
    }

    public function getNodeHash()
    {
        return $this->nodeHash;
    }

}