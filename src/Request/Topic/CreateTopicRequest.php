<?php
namespace App\Request\Topic;

use App\Node\Topic;
use App\Request\Post\CreatePostRequest;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints as Assert;

class CreateTopicRequest extends CreatePostRequest
{
    /** @Assert\NotBlank() */
    private $club;

    /**
     * @Assert\NotBlank()
     **/
    private $title;

    /**
     * @Assert\NotBlank()
     **/
    private $text;

    private $media;

    public function __construct(Request $request)
    {
        $this->club = $request->get('club');
        $this->title = $request->get(Topic::PROP_TITLE);
        $this->text = $request->get(Topic::PROP_TEXT);
        $this->media = $request->get(Topic::PROP_MEDIA, []);
        parent::__construct($request);
    }

    public function getClub()
    {
        return $this->club;
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function getMedia()
    {
        return $this->media;
    }

    public function getText()
    {
        return $this->text;
    }
}