<?php
namespace App\Request\Topic;

use App\Node\Topic;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

class UpdateTopicRequest
{
    /**
     * @Assert\NotBlank()
     */
    private $topic;

    /**
     * @Assert\NotBlank()
     */
    private $club;

    /**
     * @Assert\NotBlank()
     */
    private $title;

    private $text;

    private $media;

    public function __construct(Request $request)
    {
        $this->topic = $request->get('topic');
        $this->club = $request->get('club');

        $data = json_decode($request->getContent(), true);
        $this->text = array_key_exists(Topic::PROP_TEXT, $data) ? $data[Topic::PROP_TEXT] : null;
        $this->media = array_key_exists(Topic::PROP_MEDIA, $data) ? $data[Topic::PROP_MEDIA] : [];
        $this->title = array_key_exists(Topic::PROP_TITLE, $data) ? $data[Topic::PROP_TITLE] : null;
    }

    public function getText()
    {
        return $this->text;
    }

    public function getMedia()
    {
        return $this->media;
    }

    public function getTopic()
    {
        return $this->topic;
    }

    public function getClub()
    {
        return $this->club;
    }

    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @Assert\Callback()
     */
    public function validate(ExecutionContextInterface $context, $payload)
    {
        if (
            empty($this->text) &&
            empty($this->media) &&
            empty($this->title)
        ) {
            $context->buildViolation('ASN_ERROR_EMPTY_TOPIC')
                ->atPath('all')
                ->addViolation();
        }
    }
}