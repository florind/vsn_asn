<?php
namespace App\Request\Topic;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

class ListTopicsRequest
{
    /**
     * @Assert\NotBlank()
     */
    private $club;

    private $page;
    private $limit;

    public function __construct(Request $request)
    {
        $this->club = $request->get('club');
        $this->page = $request->get('page', 1);
        $this->limit = $request->get('limit', 15);
    }

    public function getClub()
    {
        return $this->club;
    }

    public function getPage()
    {
        return $this->page;
    }

    public function getLimit()
    {
        return $this->limit;
    }

    public function validate(ExecutionContextInterface $context, $payload)
    {
        if (!is_numeric($this->page) || $this->page < 0) {
            $context->buildViolation('The "page" parameter must be a number bigger than 0.')
                ->atPath('page')
                ->addViolation();
        }

        if (!is_numeric($this->limit) || $this->limit < 0) {
            $context->buildViolation('The "page" parameter must be a number bigger than 0.')
                ->atPath('limit')
                ->addViolation();
        }
    }
}