<?php
namespace App\Request\Topic;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints as Assert;

class TopicRequest
{
    /** @Assert\NotBlank() */
    private $club;

    /** @Assert\NotBlank() */
    private $topic;

    public function __construct(Request $request)
    {
        $this->club = $request->get('club');
        $this->topic = $request->get('topic');
    }

    public function getClub()
    {
        return $this->club;
    }

    public function getTopic()
    {
        return $this->topic;
    }
}