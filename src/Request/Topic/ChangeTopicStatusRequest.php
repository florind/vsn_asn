<?php
namespace App\Request\Topic;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints as Assert;

class ChangeTopicStatusRequest
{
    /**
     * @Assert\NotBlank()
     */
    private $club;

    /**
     * @Assert\NotBlank()
     */
    private $topic;

    public function __construct(Request $request)
    {
        $this->topic = $request->get('topic');
        $this->club = $request->get('club');
    }

    public function getTopic()
    {
        return $this->topic;
    }

    public function getClub()
    {
        return $this->club;
    }
}