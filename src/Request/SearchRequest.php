<?php
namespace App\Request;

use App\Node\Comment;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

class SearchRequest
{
    /**
     * @Assert\NotBlank()
     */
    private $phrase;

    private $type;

    public function __construct(Request $request)
    {
        $this->phrase = $request->get('phrase');
        $this->type = $request->get('type');
    }

    public function getPhrase()
    {
        return $this->phrase;
    }

    public function getType()
    {
        return $this->type;
    }
}