<?php
namespace App\Request\Club;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints as Assert;

class BanMemberRequest
{
    /**
     * @Assert\NotBlank()
     */
    private $club;

    /**
     * @Assert\NotBlank()
     */
    private $member;

    public function __construct(Request $request)
    {
        $this->club = $request->get('club');
        $this->member = $request->get('member');
    }

    public function getClub()
    {
        return $this->club;
    }

    public function getMember()
    {
        return $this->member;
    }
}