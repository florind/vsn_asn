<?php
namespace App\Request\Club;

use App\Node\Club;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints as Assert;

class CreateClubRequest
{
    /** @Assert\NotBlank() */
    private $name;

    private $description;

    private $logo;

    public function __construct(Request $request)
    {
        $this->name = $request->get(Club::PROP_NAME);
        $this->description = $request->get(Club::PROP_DESCRIPTION);
        $this->logo = $request->get('logo');
    }

    public function getName()
    {
        return $this->name;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function getLogo()
    {
        return $this->logo;
    }
}