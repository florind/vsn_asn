<?php
namespace App\Request\Club;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

class ListMembersRequest
{
    /**
     * @Assert\NotBlank()
     */
    private $clubHash;

    private $page;
    private $limit;

    public function __construct(Request $request)
    {
        $this->clubHash = $request->get('clubHash');
        $this->page = $request->get('page', 1);
        $this->limit = $request->get('limit', 10);
    }

    public function getClubHash()
    {
        return $this->clubHash;
    }

    public function getPage()
    {
        return $this->page;
    }

    public function getLimit()
    {
        return $this->limit;
    }

    public function validate(ExecutionContextInterface $context, $payload)
    {
        if (!is_numeric($this->page) || $this->page < 0) {
            $context->buildViolation('The "page" parameter must be a number bigger than 0.')
                ->atPath('page')
                ->addViolation();
        }

        if (!is_numeric($this->limit) || $this->limit < 0) {
            $context->buildViolation('The "page" parameter must be a number bigger than 0.')
                ->atPath('limit')
                ->addViolation();
        }
    }
}