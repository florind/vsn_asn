<?php
namespace App\Request;

use App\Helper\Gender;
use App\Node\Profile;
use Symfony\Component\Validator\Context\ExecutionContextInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints as Assert;

class CreateProfileRequest
{
    const ERROR_EMPTY = 'ASN_ERROR_EMPTY';
    const ERROR_INVALID_GENDER = 'ASN_ERROR_GENDER';
    const ERROR_INVALID_DATE = 'ASN_ERROR_DATE';
    const ERROR_BIRTHDAY = 'ASN_ERROR_BIRTHDAY_UNDERAGE';
    const ERROR_LOCALITY = 'ASN_ERROR_LOCALITY';

    /**
     * @Assert\NotBlank(
     *     message=CreateProfileRequest::ERROR_EMPTY
     * )
     */
    private $firstName;

    /**
     * @Assert\NotBlank(
     *     message=CreateProfileRequest::ERROR_EMPTY
     * )
     */
    private $lastName;

    /**
     * @Assert\NotBlank(
     *     message=CreateProfileRequest::ERROR_EMPTY
     * )
     * @Assert\Choice(
     *     choices={App\Helper\Gender::TYPE_MALE, App\Helper\Gender::TYPE_FEMALE},
     *     message=CreateProfileRequest::ERROR_INVALID_GENDER
     * )
     */
    private $gender;

    /**
     * @Assert\NotBlank(
     *     message=CreateProfileRequest::ERROR_EMPTY
     * )
     */
    private $birthday;

    /**
     * @Assert\NotBlank(
     *     message=CreateProfileRequest::ERROR_EMPTY
     * )
     */
    private $userId;

    public function __construct(Request $request)
    {
        $this->firstName = $request->get(Profile::PROP_FIRST_NAME, null);
        $this->lastName = $request->get(Profile::PROP_LAST_NAME, null);
        $this->gender = $request->get(Profile::PROP_GENDER, null);
        $this->birthday = $request->get(Profile::PROP_BIRTHDAY, null);
        $this->userId = $request->get('id', null);
    }

    public function getFirstName(): string
    {
        return $this->firstName;
    }

    public function getLastName(): string
    {
        return $this->lastName;
    }

    public function getGender(): Gender
    {
        return new Gender($this->gender);
    }

    public function getBirthday(): \DateTime
    {
        return \DateTime::createFromFormat('Y-m-d', $this->birthday);
    }

    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * @Assert\Callback()
     */
    public function validate(ExecutionContextInterface $context, $payload)
    {
        $birthday = \DateTime::createFromFormat('Y-m-d', $this->birthday);

        if (empty($birthday)) {
            $context->buildViolation(self::ERROR_INVALID_DATE)
                ->atPath('birthday')
                ->addViolation();

            return;
        }

        $today = new \DateTime();

        if ($today->diff($birthday)->y <= 16) {
            $context->buildViolation(self::ERROR_BIRTHDAY)
                ->atPath('birthday')
                ->addViolation();
        }
    }
}