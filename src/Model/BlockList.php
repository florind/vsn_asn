<?php
namespace App\Model;

class BlockList
{
    private $blockedProfileIds = [];

    public function __construct(array $blockedProfileIds)
    {
        $this->blockedProfileIds = $blockedProfileIds;
    }

    public function has(int $globalId)
    {
        return in_array($globalId, $this->blockedProfileIds);
    }

    public function getBlockedIds()
    {
        return $this->blockedProfileIds;
    }

    public function isEmpty(): bool
    {
        return empty($this->blockedProfileIds);
    }
}