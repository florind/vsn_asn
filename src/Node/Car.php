<?php
namespace App\Node;

class Car extends Node
{
    const NODE_TYPE = 2;

    const VEHICLE_TYPE = 'car';

    const FUEL_PETROL = 'petrol';
    const FUEL_LPG = 'lpg';
    const FUEL_DIESEL = 'diesel';
    const FUEL_ELECTRIC = 'electric';
    const FUEL_HYBRID = 'hybrid';

    const TRANSMISSION_MANUAL = 'manual';
    const TRANSMISSION_SEMIAUTOMATIC = 'semiautomatic';
    const TRANSMISSION_AUTOMATIC = 'automatic';
    const TRANSMISSION_CVT = 'cvt';
    const TRANSMISSION_TIPTRONIC = 'tiptronic';
    const TRANSMISSION_DSG = 'dsg';

    const PROP_TYPE = 'type';
    const PROP_BRAND = 'brand';
    const PROP_MODEL = 'model';
    const PROP_BODY = 'body';
    const PROP_YEAR = 'year';
    const PROP_FUEL = 'fuel';
    const PROP_CC = 'cc';
    const PROP_HP = 'hp';
    const PROP_FEATURES = 'features';
    const PROP_MILEAGE = 'mileage';
    const PROP_TRANSMISSION = 'transmission';
    const PROP_VIN = 'vin';
    const PROP_MEDIA = 'media';

    protected $type = self::NODE_TYPE;

    public function getVehicleType(): ?string
    {
        return $this->getProperty(self::PROP_TYPE);
    }

    public function setVehicleType(string $type): self
    {
        $this->addProperty(self::PROP_TYPE, $type);
        return $this;
    }

    public function getBrand(): ?string
    {
        return $this->getProperty(self::PROP_BRAND);
    }

    public function setBrand(string $brand): self
    {
        $this->addProperty(self::PROP_BRAND, $brand);
        return $this;
    }

    public function getModel(): ?string
    {
        return $this->getProperty(self::PROP_MODEL);
    }

    public function setModel(string $model): self
    {
        $this->addProperty(self::PROP_MODEL, $model);
        return $this;
    }

    public function getBodyType(): ?string
    {
        return $this->getProperty(self::PROP_BODY);
    }

    public function setBodyType(string $bodyType): self
    {
        $this->addProperty(self::PROP_BODY, $bodyType);
        return $this;
    }

    public function getYear(): ?int
    {
        return $this->getProperty(self::PROP_YEAR);
    }

    public function setYear(int $year): self
    {
        $this->addProperty(self::PROP_YEAR, $year);
        return $this;
    }

    public function getFuel(): ?string
    {
        return $this->getProperty(self::PROP_FUEL);
    }

    public function setFuel(string $fuel): self
    {
        $this->addProperty(self::PROP_FUEL, $fuel);
        return $this;
    }

    public function getEngineCapacity(): ?int
    {
        return $this->getProperty(self::PROP_CC);
    }

    public function setEngineCapacity(int $engineCapacity): self
    {
        $this->addProperty(self::PROP_CC, $engineCapacity);
        return $this;
    }

    public function getHorsepower(): ?int
    {
        return $this->getProperty(self::PROP_HP);
    }

    public function setHorsepower(int $horsepower): self
    {
        $this->addProperty(self::PROP_HP, $horsepower);
        return $this;
    }

    public function addFeature(string $feature): self
    {
        $features = $this->getProperty(self::PROP_FEATURES);
        $hash = hash('crc32', str_replace(' ', '', strtolower($feature)));

        $features[$hash] = $feature;

        $this->addProperty(self::PROP_FEATURES, $features);
        return $this;
    }

    public function getFeatures(): ?array
    {
        return $this->getProperty(self::PROP_FEATURES);
    }

    public function setFeatures(array $features): self
    {
        $this->addProperty(self::PROP_FEATURES, $features);
        return $this;
    }

    public function getMileage(): ?int
    {
        return $this->getProperty(self::PROP_MILEAGE);
    }

    public function setMileage(string $mileage): self
    {
        $this->addProperty(self::PROP_MILEAGE, $mileage);
        return $this;
    }

    public function getTransmission(): ?string
    {
        return $this->getProperty(self::PROP_TRANSMISSION);
    }

    public function setTransmission(string $transmission): self
    {
        $this->addProperty(self::PROP_TRANSMISSION, $transmission);
        return $this;
    }

    public function getVin(): ?string
    {
        return $this->getProperty(self::PROP_VIN);
    }

    public function setVin(string $vin): self
    {
        $this->addProperty(self::PROP_VIN, $vin);
        return $this;
    }

}