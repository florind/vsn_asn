<?php
namespace App\Node;

class Reply extends Comment
{
    const NODE_TYPE = 8;
    protected $type = self::NODE_TYPE;
}