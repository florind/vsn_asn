<?php
namespace App\Node;

class Post extends Node
{
    const NODE_TYPE = 3;
    const PROP_TEXT = 'text';
    const PROP_MEDIA = 'media';

    protected $type = self::NODE_TYPE;

    public function getText(): ?string
    {
        return $this->getProperty(self::PROP_TEXT);
    }

    public function setText(string $text): self
    {
        $this->addProperty(self::PROP_TEXT, $text);
        return $this;
    }
}