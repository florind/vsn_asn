<?php
namespace App\Node;

class Club extends Node
{
    const NODE_TYPE = 6;
    const PROP_NAME = 'name';
    const PROP_DESCRIPTION = 'description';
    const PROP_LOGO = 'logo';

    protected $type = self::NODE_TYPE;

    public function getName(): ?string
    {
        return $this->getProperty(self::PROP_NAME);
    }

    public function setName(string $name): self
    {
        $this->addProperty(self::PROP_NAME, $name);
        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->getProperty(self::PROP_DESCRIPTION);
    }

    public function setDescription(string $description): self
    {
        $this->addProperty(self::PROP_DESCRIPTION, $description);
        return $this;
    }

    public function getLogo(): ?string
    {
        return $this->getProperty(self::PROP_LOGO);
    }

    public function setLogo(string $photo): self
    {
        $this->addProperty(self::PROP_LOGO, $photo);
        return $this;
    }
}