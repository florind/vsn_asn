<?php
namespace App\Node;

use App\Helper\Gender;

class Profile extends Node
{
    const NODE_TYPE = 1;

    const PROP_FIRST_NAME = 'first_name';
    const PROP_LAST_NAME = 'last_name';
    const PROP_BIRTHDAY = 'birthday';
    const PROP_GENDER = 'gender';
    const PROP_AVATAR = 'avatar';
    const PROP_LOCALITY = 'locality';

    protected $type = self::NODE_TYPE;

    public function getFirstName(): ?string
    {
        return $this->getProperty(self::PROP_FIRST_NAME);
    }

    public function setFirstName(string $firstName): self
    {
        $this->addProperty(self::PROP_FIRST_NAME, $firstName);
        return $this;
    }

    public function getLastName(): ?string
    {
        return $this->getProperty(self::PROP_LAST_NAME);
    }

    public function setLastName(string $lastName): self
    {
        $this->addProperty(self::PROP_LAST_NAME, $lastName);
        return $this;
    }

    public function getBirthday(): ?string
    {
        return $this->getProperty(self::PROP_BIRTHDAY);
    }

    public function setBirthday(\DateTime $birthday): self
    {
        $this->addProperty(self::PROP_BIRTHDAY, $birthday->format('Y-m-d'));
        return $this;
    }

    public function getGender(): ?string
    {
        return $this->getProperty(self::PROP_GENDER);
    }

    public function setGender(Gender $gender): self
    {
        $this->addProperty(self::PROP_GENDER, (string)$gender);
        return $this;
    }

    public function getAvatar(): ?string
    {
        return $this->getProperty(self::PROP_AVATAR);
    }

    public function setAvatar(string $avatar = null)
    {
        $this->addProperty(self::PROP_AVATAR, $avatar);
        return $this;
    }

    public function getLocality(): ?string
    {
        return $this->getProperty(self::PROP_LOCALITY);
    }

    public function setLocality(string $locality): self
    {
        $this->addProperty(self::PROP_LOCALITY, $locality);
        return $this;
    }
}