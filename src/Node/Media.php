<?php
namespace App\Node;

class Media extends Node
{
    const NODE_TYPE = 5;
    const PROP_FILENAME = 'filename';
    const PROP_TYPE = 'type';
    const MEDIA_TYPE_PHOTO = 'photo';
    const MEDIA_TYPE_VIDEO = 'video';
    const MEDIA_TYPE_AUDIO = 'audio';

    private $mediaTypeExtensions = [
        self::MEDIA_TYPE_PHOTO => [
            '.bmp', '.jpg', '.jpeg', '.jif', 'jiff', '.gif', '.png', '.tif', '.tiff'
        ],
        self::MEDIA_TYPE_VIDEO => [
            '.avi', '.flv', '.wmv', '.mov', '.mp4', '.m4p', '.mpg', '.mpeg', '.3gp', '.mkv'
        ],
        self::MEDIA_TYPE_AUDIO => [
            '.mp3', '.wav'
        ]
    ];

    protected $type = self::NODE_TYPE;

    public function getFilename()
    {
        return $this->getProperty(self::PROP_FILENAME);
    }

    public function setFilename(string $path)
    {
        $this->addProperty(self::PROP_FILENAME, $path);
        $this->setMediaType($this->guessMediaType($path));
        return $this;
    }

    public function getMediaType()
    {
        return $this->getProperty(self::PROP_TYPE);
    }

    public function setMediaType(string $type)
    {
        $this->addProperty(self::PROP_TYPE, $type);
    }

    private function guessMediaType(string $filename)
    {
        $found = [];

        if (preg_match('/\.[0-9a-z]+$/', $filename, $found)) {
            $extension = reset($found);
            foreach ($this->mediaTypeExtensions as $type => $extensions) {
                if (in_array($extension, $extensions)) {
                    return $type;
                }
            }
        }

        return null;
    }
}