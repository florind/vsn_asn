<?php
namespace App\Node;

class Topic extends Node
{
    const NODE_TYPE = 7;

    const PROP_TITLE = 'title';
    const PROP_TEXT = 'text';
    const PROP_MEDIA = 'media';
    const PROP_LOCKED = 'locked';

    protected $type = self::NODE_TYPE;

    public function getTitle(): string
    {
        return $this->getProperty(self::PROP_TITLE);
    }

    public function setTitle(string $title)
    {
        $this->addProperty(self::PROP_TITLE, $title);
        return $this;
    }

    public function getText(): ?string
    {
        return $this->getProperty(self::PROP_TEXT);
    }

    public function setText(string $text): self
    {
        $this->addProperty(self::PROP_TEXT, $text);
        return $this;
    }

    public function unlock()
    {
        $this->addProperty(self::PROP_LOCKED, false);
        return $this;
    }

    public function lock()
    {
        $this->addProperty(self::PROP_LOCKED, true);
        return $this;
    }

    public function isLocked()
    {
        return $this->getProperty(self::PROP_LOCKED) === true;
    }
}