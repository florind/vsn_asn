<?php
namespace App\Node;

abstract class Node
{
    private $id;
    protected $type;
    protected $properties = [];
    private $created;
    private $updated;

    const FIELD_ID = 'id';
    const FIELD_TYPE = 'type';
    const FIELD_DATA = 'data';
    const FIELD_CREATED = 'created';
    const FIELD_UPDATED = 'updated';

    public static function fields(): array
    {
        return [
            self::FIELD_ID,
            self::FIELD_TYPE,
            self::FIELD_DATA,
            self::FIELD_CREATED,
            self::FIELD_UPDATED
        ];
    }

    public function getId()
    {
        return $this->id;
    }

    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    public function setType($type)
    {
        $this->type = $type;
        return $this;
    }

    public function getType()
    {
        return $this->type;
    }

    public function getProperties(): array
    {
        return $this->properties;
    }

    protected function addProperty($key, $value): self
    {
        $this->properties[$key] = $value;
        return $this;
    }

    protected function getProperty($key)
    {
        return array_key_exists($key, $this->properties) ? $this->properties[$key] : null;
    }

    public function setProperties(array $properties): self
    {
        $this->properties = $properties;
        return $this;
    }

    public function getCreated()
    {
        return $this->created;
    }

    public function getUpdated()
    {
        return $this->updated;
    }

    public function setCreated($created)
    {
        $this->created = $created;
        return $this;
    }

    public function setUpdated($updated)
    {
        $this->updated = $updated;
        return $this;
    }

}