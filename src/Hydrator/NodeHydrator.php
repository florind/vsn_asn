<?php
namespace App\Hydrator;

use App\Node\Node;
use Psr\Log\InvalidArgumentException;

class NodeHydrator
{
    private $typeMap = [];

    public function register(int $code, string $class)
    {
        $this->typeMap[$code] = $class;
    }

    public function hydrate(array $nodeData): Node
    {
        foreach (Node::fields() as $field) {
            if (!array_key_exists($field, $nodeData)) {
                throw new InvalidArgumentException('Cannot hydrate node with invalid structure.');
            }
        }

        if (!isset($this->typeMap[$nodeData[Node::FIELD_TYPE]])) {
            throw new InvalidArgumentException(sprintf('Unsupported node type: %s', $nodeData[Node::FIELD_TYPE]));
        }

        $node = $this->typeMap[$nodeData[Node::FIELD_TYPE]];

        return (new $node)
            ->setId($nodeData[Node::FIELD_ID])
            ->setType($nodeData[Node::FIELD_TYPE])
            ->setProperties(json_decode($nodeData[Node::FIELD_DATA], true))
            ->setCreated($nodeData[Node::FIELD_CREATED])
            ->setUpdated($nodeData[Node::FIELD_UPDATED]);
    }
}