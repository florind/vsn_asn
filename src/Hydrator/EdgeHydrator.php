<?php
namespace App\Hydrator;

use App\Edge\Edge;
use Psr\Log\InvalidArgumentException;

class EdgeHydrator
{
    public function hydrate(array $edgeData): Edge
    {
        foreach (Edge::fields() as $field) {
            if (!array_key_exists($field, $edgeData)) {
                throw new InvalidArgumentException('Cannot hydrate edge with invalid structure.');
            }
        }

        return (new Edge())
            ->setType($edgeData[Edge::FIELD_TYPE])
            ->setId($edgeData[Edge::FIELD_ID])
            ->setCreated($edgeData[Edge::FIELD_CREATED])
            ->setOwnerNode($edgeData[Edge::FIELD_OWNER_NODE])
            ->setInverseNode($edgeData[Edge::FIELD_INVERSE_NODE])
            ->setInverseNodeType($edgeData[Edge::FIELD_INVERSE_NODE_TYPE]);
    }
}