<?php

use Phinx\Migration\AbstractMigration;

class CreateSearchCatalog extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $this->table('catalog', ['id' => false, 'primary_key' => 'id'])
            ->addColumn('id', 'biginteger', ['null' => false])
            ->addColumn('type', 'integer', ['length' => \Phinx\Db\Adapter\PostgresAdapter::INT_SMALL, 'null' => false])
            ->addColumn('content', 'text', ['null' => false])
            ->addColumn('owner', 'biginteger', ['null' => false])
            ->addIndex('content', ['type' => 'fulltext'])
            ->create();
    }
}
