<?php
use Phinx\Migration\AbstractMigration;

class CreateNodeTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $table = $this->table('node');
        $table->addColumn('type', 'integer', ['length' => \Phinx\Db\Adapter\PostgresAdapter::INT_SMALL, 'null' => false])
            ->addColumn('data', 'text', ['null' => false, 'collation' => 'utf8_unicode_ci'])
            ->addColumn('created', 'datetime', ['null' => false])
            ->addColumn('updated', 'datetime', ['null' => true, 'default' => null])
            ->create();
    }
}
