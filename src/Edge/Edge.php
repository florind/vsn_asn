<?php
namespace App\Edge;

class Edge
{
    const TYPE_CREATED = 'created';
    const TYPE_CREATED_BY = 'created_by';

    const TYPE_MODERATES = 'moderates';
    const TYPE_MODERATED_BY = 'moderated_by';

    const TYPE_FOLLOWED = 'followed';
    const TYPE_FOLLOWED_BY = 'followed_by';
    const TYPE_LIKED = 'liked';
    const TYPE_LIKED_BY = 'liked_by';
    const TYPE_BLOCKED = 'blocked';
    const TYPE_BLOCKED_BY = 'blocked_by';
    const TYPE_HOLDS = 'holds';
    const TYPE_HELD_BY = 'held_by';

    const FIELD_ID = 'id';
    const FIELD_TYPE = 'type';
    const FIELD_OWNER_NODE = 'owner_node';
    const FIELD_INVERSE_NODE = 'inverse_node';
    const FIELD_INVERSE_NODE_TYPE = 'inverse_node_type';
    const FIELD_CREATED = 'created';

    private $id;
    private $type;
    private $ownerNode;
    private $inverseNode;
    private $inverseNodeType;
    private $created;

    public static function fields()
    {
        return [
            self::FIELD_ID,
            self::FIELD_TYPE,
            self::FIELD_OWNER_NODE,
            self::FIELD_INVERSE_NODE,
            self::FIELD_INVERSE_NODE_TYPE,
            self::FIELD_CREATED
        ];
    }

    public function getId()
    {
        return $this->id;
    }

    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    public function getType()
    {
        return $this->type;
    }

    public function getOwnerNode()
    {
        return $this->ownerNode;
    }

    public function setOwnerNode($ownerNode)
    {
        $this->ownerNode = $ownerNode;
        return $this;
    }

    public function getInverseNode()
    {
        return $this->inverseNode;
    }

    public function setInverseNode($inverseNode)
    {
        $this->inverseNode = $inverseNode;
        return $this;
    }

    public function getInverseNodeType()
    {
        return $this->inverseNodeType;
    }

    public function setInverseNodeType($inverseNodeType)
    {
        $this->inverseNodeType = $inverseNodeType;
        return $this;
    }

    public function getCreated()
    {
        return $this->created;
    }

    public function setCreated($created)
    {
        $this->created = $created;
        return $this;
    }

    public function setType($type)
    {
        $this->type = $type;
        return $this;
    }
}