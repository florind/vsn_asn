<?php
namespace App\ViewModel;

use App\Node\Topic;

class TopicSearchResultViewModel implements \JsonSerializable
{
    private $view = [];

    public function __construct(string $encodedId, Topic $topic)
    {
        $this->view = [
            'hash' => $encodedId,
            'title' => $topic->getTitle(),
            'text' => substr($topic->getText(), 0, 150) . "..."
        ];
    }

    function jsonSerialize()
    {
        return $this->view;
    }
}