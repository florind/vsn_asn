<?php
namespace App\ViewModel;

class ReplyListViewModel implements \JsonSerializable
{
    private $view = [];

    public function __construct(int $count, array $replies)
    {
        $this->view = [
            'replies' => $replies,
            'count' => $count
        ];
    }

    function jsonSerialize()
    {
        return $this->view;
    }
}