<?php
namespace App\ViewModel;

use App\Node\Profile;

class FullProfileViewModel implements \JsonSerializable
{
    private $view = [];

    public function __construct(
        string $encodedId,
        Profile $profile,
        int $cars,
        int $followers,
        int $follows,
        int $memberships,
        array $context
    ){
        $this->view['profile'] = ['hash' => $encodedId];
        $this->view['profile'] = array_merge($this->view['profile'], $profile->getProperties());
        $this->view['cars'] = $cars;
        $this->view['followers'] = $followers;
        $this->view['follows'] = $follows;
        $this->view['memberships'] = $memberships;
        $this->view['context'] = $context;
    }

    function jsonSerialize()
    {
        return $this->view;
    }
}