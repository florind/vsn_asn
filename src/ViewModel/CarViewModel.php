<?php
namespace App\ViewModel;

use App\Node\Car;

class CarViewModel implements \JsonSerializable
{
    private $view = [];

    public function __construct(
        string $encodedId,
        Car $car,
        array $media,
        int $likes,
        int $comments,
        array $relationships
    ) {
        $this->view = [
            'car' => ['hash' => $encodedId],
            'media' => $media,
            'likes' => $likes,
            'comments' => $comments,
            'context' => $relationships
        ];

        $this->view['car'] = array_merge($this->view['car'], $car->getProperties());
    }

    function jsonSerialize()
    {
        return $this->view;
    }
}