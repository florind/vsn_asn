<?php
namespace App\ViewModel;

use App\Node\Topic;

class TopicViewModel implements \JsonSerializable
{
    private $view = [];

    public function __construct(string $encodedId, Topic $topic, MinimalProfileViewModel $author, array $media, int $likes, int $comments, array $permissions)
    {
        $this->view = [
            'hash' => $encodedId,
            'title' => $topic->getTitle(),
            'text' => $topic->getText(),
            'media' => $media,
            'author' => $author,
            'timestamp' => $topic->getCreated(),
            'locked' => $topic->isLocked(),
            'likes' => $likes,
            'comments' => $comments,
            'permissions' => $permissions
        ];
    }

    function jsonSerialize()
    {
        return $this->view;
    }
}