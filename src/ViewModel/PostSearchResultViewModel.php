<?php
namespace App\ViewModel;

use App\Node\Post;

class PostSearchResultViewModel implements \JsonSerializable
{
    private $view = [];

    public function __construct(string $encodedId, Post $post)
    {
        $this->view = [
            'hash' => $encodedId,
            'text' => substr($post->getText(), 0, 150) . "..."
        ];
    }

    function jsonSerialize()
    {
        return $this->view;
    }
}