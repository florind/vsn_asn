<?php
namespace App\ViewModel;

class MemberListViewModel implements \JsonSerializable
{
    private $view = [];

    public function __construct(int $count, array $topics)
    {
        $this->view = [
            'members' => $topics,
            'count' => $count
        ];
    }

    function jsonSerialize()
    {
        return $this->view;
    }
}