<?php
namespace App\ViewModel;

use App\Node\Club;

class MinimalClubViewModel implements \JsonSerializable
{
    private $view = [];

    public function __construct(string $encodedId, Club $club, int $memberCount, array $permissions)
    {
        $this->view = [
            'hash' => $encodedId,
            'name' => $club->getName(),
            'logo' => $club->getLogo(),
            'member_count' => $memberCount,
            'permissions' => $permissions
        ];
    }

    function jsonSerialize()
    {
        return $this->view;
    }
}