<?php
namespace App\ViewModel;

use App\Node\Club;

class ClubViewModel implements \JsonSerializable
{
    private $view = [];

    public function __construct(
        string $encodedId,
        Club $club,
        MinimalProfileViewModel $profile,
        int $memberCount,
        int $topicCount,
        array $permissions
    ) {
        $this->view = [
            'hash' => $encodedId,
            'name' => $club->getName(),
            'description' => $club->getDescription(),
            'logo' => $club->getLogo(),
            'created' => $club->getCreated(),
            'member_count' => $memberCount,
            'topic_count' => $topicCount,
            'founder' => $profile,
            'permissions' => $permissions
        ];
    }

    function jsonSerialize()
    {
        return $this->view;
    }
}