<?php
namespace App\ViewModel;

use App\Node\Profile;

class MinimalProfileViewModel implements \JsonSerializable
{
    private $view = [];

    public function __construct(string $encodedId, Profile $profile, array $permissions = [])
    {
        $this->view = [
            'hash' => $encodedId,
            'first_name' => $profile->getFirstName(),
            'last_name' => $profile->getLastName(),
            'avatar' => $profile->getAvatar(),
            'locality' => $profile->getLocality(),
            'permissions' => $permissions
        ];
    }

    function jsonSerialize()
    {
        return $this->view;
    }
}