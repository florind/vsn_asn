<?php
namespace App\ViewModel;

use App\Node\Media;

class MediaViewModel implements \JsonSerializable
{
    private $view = [];

    public function __construct(string $encodedId, Media $media)
    {
        $this->view = [
            'hash' => $encodedId,
            'filename' => $media->getFilename(),
            'type' => $media->getMediaType(),
            'created' => $media->getCreated()
        ];
    }

    function jsonSerialize()
    {
        return $this->view;
    }
}