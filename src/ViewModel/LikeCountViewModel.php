<?php
namespace App\ViewModel;

class LikeCountViewModel implements \JsonSerializable
{
    private $view = [];

    public function __construct(int $count) {
        $this->view = [
            'likes' => $count
        ];
    }

    function jsonSerialize()
    {
        return $this->view;
    }
}