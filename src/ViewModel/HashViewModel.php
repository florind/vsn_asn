<?php
namespace App\ViewModel;

class HashViewModel implements \JsonSerializable
{
    private $view = [];

    public function __construct(string $hash)
    {
        $this->view['hash'] = $hash;
    }

    function jsonSerialize()
    {
        return $this->view;
    }
}