<?php
namespace App\ViewModel;

use App\Node\Profile;

class ClubMemberViewModel implements \JsonSerializable
{
    private $view = [];

    public function __construct(string $encodedId, Profile $profile, string $joinDate, array $permissions)
    {
        $this->view = [
            'hash' => $encodedId,
            'first_name' => $profile->getFirstName(),
            'last_name' => $profile->getLastName(),
            'avatar' => $profile->getAvatar(),
            'locality' => $profile->getLocality(),
            'timestamp' => $joinDate,
            'permissions' => $permissions
        ];
    }

    function jsonSerialize()
    {
        return $this->view;
    }
}