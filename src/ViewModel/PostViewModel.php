<?php
namespace App\ViewModel;

use App\Node\Post;

class PostViewModel implements \JsonSerializable
{
    private $view = [];

    public function __construct(
        string $encodedId,
        Post $post,
        MinimalProfileViewModel $poster,
        int $likes,
        int $comments,
        array $media,
        array $relationships
    ) {
        $this->view = [
            'post' => [
                'hash' => $encodedId,
                'text' => $post->getText(),
                'media' => $media,
                'poster' => $poster,
                'timestamp' => $post->getCreated()
            ],
            'likes' => $likes,
            'comments' => $comments,
            'context' => $relationships
        ];
    }

    function jsonSerialize()
    {
        return $this->view;
    }
}