<?php
namespace App\ViewModel;

use App\Node\Comment;

class CommentViewModel implements \JsonSerializable
{
    private $view = [];

    public function __construct(
        string $encodedId,
        Comment $comment,
        array $media,
        MinimalProfileViewModel $author,
        int $likes,
        int $replies,
        array $permissions
    ) {
        $this->view = [
            'hash' => $encodedId,
            'text' => $comment->getText(),
            'media' => $media,
            'timestamp' => $comment->getCreated(),
            'author' => $author,
            'likes' => $likes,
            'replies' => $replies,
            'permissions' => $permissions
        ];
    }

    public function get($key)
    {
        return array_key_exists($key, $this->view) ? $this->view[$key] : null;
    }

    function jsonSerialize()
    {
        return $this->view;
    }
}