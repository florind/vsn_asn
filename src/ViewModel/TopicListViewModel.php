<?php
namespace App\ViewModel;

class TopicListViewModel implements \JsonSerializable
{
    private $view = [];

    public function __construct(int $count, array $topics)
    {
        $this->view = [
            'topics' => $topics,
            'count' => $count
        ];
    }

    function jsonSerialize()
    {
        return $this->view;
    }
}