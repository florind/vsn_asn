<?php
namespace App\ViewModel;

class CommentListViewModel implements \JsonSerializable
{
    private $view = [];

    public function __construct(int $count, array $comments)
    {
        $this->view = [
            'comments' => $comments,
            'count' => $count
        ];
    }

    function jsonSerialize()
    {
        return $this->view;
    }
}