<?php
namespace App\Controller;

use App\Request\NodeRelatedRequest;
use App\Request\Post\CreatePostRequest;
use App\Request\Post\ListPostsRequest;
use App\Request\Post\UpdatePostRequest;
use App\Service\PostService;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class PostController extends ApiController
{
    public function list(Request $request, PostService $postService)
    {
        $model = new ListPostsRequest($request);
        $this->validateRequest($model);

        $this->approveView($model->getProfileHash());
        $posts = $postService->list($model, $this->getViewer());

        return new JsonResponse($posts);
    }

    public function create(Request $request, PostService $postService)
    {
        $model = new CreatePostRequest($request);
        $this->validateRequest($model);
        $postHash = $postService->create($model, $this->getViewer());

        return new JsonResponse($postHash);
    }

    public function view(Request $request, PostService $service)
    {
        $model = new NodeRelatedRequest($request);
        $this->validateRequest($model);

        $this->approveView($model->getNodeHash());
        $postData = $service->view($model, $this->getViewer());

        return new JsonResponse($postData);
    }

    public function update(Request $request, PostService $postService)
    {
        $model = new UpdatePostRequest($request);
        $this->validateRequest($model);

        $this->approveEdit($model->getPostHash());
        $postService->update($model, $this->getViewer());

        return new JsonResponse();
    }

    public function delete(Request $request, PostService $postService)
    {
        $model = new NodeRelatedRequest($request);
        $this->validateRequest($model);

        $this->approveEdit($model->getNodeHash());
        $postService->delete($model, $this->getViewer());

        return new JsonResponse();
    }

}