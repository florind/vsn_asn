<?php
namespace App\Controller;

use App\Request\NodeRelatedRequest;
use App\Request\Vehicle\AddMediaRequest;
use App\Request\Vehicle\CreateCarRequest;
use App\Request\Vehicle\DeleteMediaRequest;
use App\Request\Vehicle\UpdateCarRequest;
use App\Service\CarService;
use App\Service\NodePermissions;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class CarController extends ApiController
{
    public function create(Request $request, CarService $service)
    {
        $model = new CreateCarRequest($request);
        $this->validateRequest($model);
        $service->create($model, $this->getViewer());

        return new JsonResponse();
    }

    public function view(Request $request, CarService $service)
    {
        $model = new NodeRelatedRequest($request);
        $this->validateRequest($model);

        $this->approveView($model->getNodeHash());
        $data = $service->view($model, $this->getViewer());

        return new JsonResponse($data);
    }

    public function profileCars(Request $request, CarService $service)
    {
        $model = new NodeRelatedRequest($request);
        $this->validateRequest($model);

        $this->approveView($model->getNodeHash());
        $data = $service->list($model, $this->getViewer());

        return new JsonResponse($data);
    }

    public function update(Request $request, CarService $service)
    {
        $model = new UpdateCarRequest($request);
        $this->validateRequest($model);

        $this->approveEdit($model->getCarHash());
        $service->update($model, $this->getViewer());

        return new JsonResponse();
    }

    public function delete(Request $request, CarService $service)
    {
        $model = new NodeRelatedRequest($request);
        $this->validateRequest($model);

        $this->approveEdit($model->getNodeHash());
        $service->delete($model, $this->getViewer());

        return new JsonResponse();
    }

    public function getMedia(Request $request, CarService $service)
    {
        $model = new NodeRelatedRequest($request);
        $this->validateRequest($model);

        $this->approveView($model->getNodeHash());
        $photos = $service->getMedia($model);

        return new JsonResponse($photos);
    }

    public function addMedia(Request $request, CarService $service)
    {
        $model = new AddMediaRequest($request);
        $this->validateRequest($model);

        $this->approveEdit($model->getNodeHash());
        $service->addMedia($model, $this->getViewer());

        return new JsonResponse();
    }

    public function deleteMedia(Request $request, CarService $service)
    {
        $model = new DeleteMediaRequest($request);
        $this->validateRequest($model);

        $this->approveEdit($model->getMediaHash());
        $service->deleteMedia($model, $this->getViewer());

        return new JsonResponse();
    }
}