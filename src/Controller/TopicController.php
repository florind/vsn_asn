<?php
namespace App\Controller;

use App\Helper\Id64Bit;
use App\Request\Topic\CreateTopicRequest;
use App\Request\Topic\DeleteTopicRequest;
use App\Request\Topic\TopicRequest;
use App\Request\Topic\ListTopicsRequest;
use App\Request\Topic\ChangeTopicStatusRequest;
use App\Request\Topic\UpdateTopicRequest;
use App\Service\AccessControl\TopicAccessControl;
use App\Service\FollowService;
use App\Service\NodePermissions;
use App\Service\TopicService;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class TopicController extends ApiController
{
    public function create(Request $request, TopicService $service, TopicAccessControl $accessControl)
    {
        $model = new CreateTopicRequest($request);
        $this->validateRequest($model);
        $accessControl->approveCreate($model->getClub(), $this->getViewer());
        $topicHash = $service->create($model, $this->getViewer());

        return new JsonResponse($topicHash);
    }

    public function view(Request $request, TopicService $service, TopicAccessControl $accessControl)
    {
        $model = new TopicRequest($request);
        $this->validateRequest($model);
        $accessControl->approveView($model->getClub(), $model->getTopic(), $this->getViewer());
        $topic = $service->view($model, $this->getViewer());

        return new JsonResponse($topic);
    }

    public function update(Request $request, TopicService $service, TopicAccessControl $accessControl)
    {
        $model = new UpdateTopicRequest($request);
        $this->validateRequest($model);
        $accessControl->approveEdit($model->getClub(), $model->getTopic(), $this->getViewer());
        $service->update($model, $this->getViewer());

        return new JsonResponse();
    }

    public function delete(Request $request, TopicService $service, TopicAccessControl $accessControl)
    {
        $model = new TopicRequest($request);
        $this->validateRequest($model);
        $accessControl->approveDelete($model->getClub(), $model->getTopic(), $this->getViewer());
        $service->delete($model, $this->getViewer());

        return new JsonResponse();
    }

    public function list(Request $request, TopicService $service, TopicAccessControl $accessControl)
    {
        $model = new ListTopicsRequest($request);
        $this->validateRequest($model);
        $accessControl->approveList($model->getClub(), $this->getViewer());
        $topics = $service->list($model, $this->getViewer());
        return new JsonResponse($topics);
    }

    public function unlock(Request $request, TopicService $service, TopicAccessControl $accessControl)
    {
        $model = new ChangeTopicStatusRequest($request);
        $this->validateRequest($model);
        $accessControl->approveLock($model->getClub(), $model->getTopic(), $this->getViewer());
        $permissions = $service->unlock($model->getTopic(), $this->getViewer());

        return new JsonResponse($permissions);
    }

    public function lock(Request $request, TopicService $service, TopicAccessControl $accessControl)
    {
        $model = new ChangeTopicStatusRequest($request);
        $this->validateRequest($model);
        $accessControl->approveLock($model->getClub(), $model->getTopic(), $this->getViewer());
        $permissions = $service->lock($model->getTopic(), $this->getViewer());

        return new JsonResponse($permissions);
    }

    public function follow(Request $request, TopicService $service, TopicAccessControl $accessControl)
    {
        $model = new TopicRequest($request);
        $this->validateRequest($model);
        $accessControl->approveView($model->getClub(), $model->getTopic(), $this->getViewer());
        $permissions = $service->follow($model->getTopic(), $this->getViewer());

        return new JsonResponse($permissions);
    }

    public function unfollow(Request $request, TopicService $service, TopicAccessControl $accessControl)
    {
        $model = new TopicRequest($request);
        $this->validateRequest($model);
        $accessControl->approveView($model->getClub(), $model->getTopic(), $this->getViewer());
        $permissions = $service->unfollow($model->getTopic(), $this->getViewer());

        return new JsonResponse($permissions);
    }
}