<?php
namespace App\Controller;

use Jose\Component\Core\Converter\StandardConverter;
use Jose\Component\Signature\Serializer\CompactSerializer;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;

class TestController extends Controller
{
    public function index()
    {
        $jsonConverter = new StandardConverter();

        $payload = $jsonConverter->encode([
            'iat' => time(),
            'nbf' => time(),
            'exp' => time() + 86400,
            'iss' => 'Test',
            'aud' => 'test',
            'uid' => 123456
        ]);

        $jwsBuilder = $this->get('jose.jws_builder.base');
        $serializer = new CompactSerializer($jsonConverter);

        $jwk = $this->get('jose.key.vsn_user_key');

        $jws = $jwsBuilder->create()
        ->withPayload($payload)
        ->addSignature($jwk, ['alg' => 'HS256'])
        ->build();

        $token = $serializer->serialize($jws);

        //verification
        $jwsVerifier = $this->get('jose.jws_verifier.base');

        $jws = $serializer->unserialize($token);
        $valid = $jwsVerifier->verifyWithKey($jws, $jwk, 0);

        return new JsonResponse(['token' => $token, 'valid' => $valid, 'jws' =>$jsonConverter->decode($jws->getPayload())]);
    }
}