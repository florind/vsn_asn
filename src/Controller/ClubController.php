<?php
namespace App\Controller;

use App\Request\Club\BanMemberRequest;
use App\Request\Club\CreateClubRequest;
use App\Request\Club\ListMembersRequest;
use App\Request\NodeRelatedRequest;
use App\Service\AccessControl\ClubAccessControl;
use App\Service\ClubService;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class ClubController extends ApiController
{
    public function list(Request $request, ClubService $service)
    {
        $list = $service->list($this->getViewer());
        return new JsonResponse($list);
    }

    public function create(Request $request, ClubService $service)
    {
        $model = new CreateClubRequest($request);
        $service->create($model, $this->getViewer());

        return new JsonResponse();
    }

    public function view(Request $request, ClubService $service)
    {
        $model = new NodeRelatedRequest($request);
        $club = $service->view($model->getNodeHash(), $this->getViewer());

        return new JsonResponse($club);
    }

    public function join(Request $request, ClubService $service)
    {
        $model = new NodeRelatedRequest($request);
        $this->validateRequest($model);
        $service->join($model, $this->getViewer());

        return new JsonResponse();
    }

    public function leave(Request $request, ClubService $service)
    {
        $model = new NodeRelatedRequest($request);
        $this->validateRequest($model);
        $service->leave($model, $this->getViewer());

        return new JsonResponse();
    }

    public function members(Request $request, ClubService $service)
    {
        $model = new ListMembersRequest($request);
        $this->validateRequest($model);
        $members = $service->members($model, $this->getViewer());

        return new JsonResponse($members);
    }

    public function banMember(Request $request, ClubService $service, ClubAccessControl $accessControl)
    {
        $model = new BanMemberRequest($request);
        $this->validateRequest($model);
        $accessControl->approveBan($model->getClub(), $this->getViewer(), $model->getMember());
        $service->banMember($model, $this->getViewer());

        return new JsonResponse();
    }
}