<?php
namespace App\Controller;

use App\Request\NodeRelatedRequest;
use App\Request\Vehicle\AddMediaRequest;
use App\Request\Vehicle\DeleteMediaRequest;
use App\Service\MediaRequestService;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class MediaController extends ApiController
{
    public function view(Request $request, MediaRequestService $service)
    {
        $model = new NodeRelatedRequest($request);
        $this->validateRequest($model);

        $this->approveView($model->getNodeHash());
        $photos = $service->get($model);

        return new JsonResponse($photos);
    }

    public function add(Request $request, MediaRequestService $service)
    {
        $model = new AddMediaRequest($request);
        $this->validateRequest($model);

        $this->approveEdit($model->getNodeHash());
        $service->add($model, $this->getViewer());

        return new JsonResponse();
    }

    public function delete(Request $request, MediaRequestService $service)
    {
        $model = new DeleteMediaRequest($request);
        $this->validateRequest($model);

        $this->approveEdit($model->getMediaHash());
        $service->delete($model, $this->getViewer());

        return new JsonResponse();
    }
}