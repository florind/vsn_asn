<?php
namespace App\Controller;

use App\Request\Comment\CreateTopicCommentRequest;
use App\Request\Comment\DeleteCommentRequest;
use App\Request\Comment\EditTopicCommentRequest;
use App\Request\Comment\ListTopicCommentsRequest;
use App\Request\Comment\ViewTopicCommentRequest;
use App\Service\AccessControl\TopicCommentAccessControl;
use App\Service\CommentService;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class CommentController extends ApiController
{
    public function list(Request $request, CommentService $commentService, TopicCommentAccessControl $accessControl)
    {
        $model = new ListTopicCommentsRequest($request);
        $this->validateRequest($model);
        $accessControl->approveList($model->getClub(), $model->getTopic(), $this->getViewer());
        $comments = $commentService->list($model, $this->getViewer());

        return new JsonResponse($comments);
    }

    public function view(Request $request, CommentService $commentService, TopicCommentAccessControl $accessControl)
    {
        $model = new ViewTopicCommentRequest($request);
        $this->validateRequest($model);
        $accessControl->approveView($model->getClub(), $model->getTopic(), $model->getComment(), $this->getViewer());
        $comments = $commentService->view($model, $this->getViewer());

        return new JsonResponse($comments);
    }

    public function create(Request $request, CommentService $commentService, TopicCommentAccessControl $accessControl)
    {
        $model = new CreateTopicCommentRequest($request);
        $this->validateRequest($model);
        $accessControl->approveCreate($model->getClub(), $model->getTopic(), $this->getViewer());
        $commentService->create($model, $this->getViewer());

        return new JsonResponse();
    }

    public function update(Request $request, CommentService $commentService, TopicCommentAccessControl $accessControl)
    {
        $model = new EditTopicCommentRequest($request);
        $this->validateRequest($model);
        $accessControl->approveEdit($model->getClub(), $model->getTopic(), $model->getComment(), $this->getViewer());
        $commentService->update($model, $this->getViewer());

        return new JsonResponse();
    }

    public function delete(Request $request, CommentService $commentService, TopicCommentAccessControl $accessControl)
    {
        $model = new DeleteCommentRequest($request);
        $this->validateRequest($model);
        $accessControl->approveDelete($model->getClub(), $model->getTopic(), $model->getComment(), $this->getViewer());
        $commentService->delete($model, $this->getViewer());

        return new JsonResponse();
    }

    public function follow(Request $request, CommentService $commentService, TopicCommentAccessControl $accessControl)
    {
        $model = new ViewTopicCommentRequest($request);
        $this->validateRequest($model);
        $accessControl->approveView($model->getClub(), $model->getTopic(), $model->getComment(), $this->getViewer());
        $permissions = $commentService->follow($model, $this->getViewer());

        return new JsonResponse($permissions);
    }

    public function unfollow(Request $request, CommentService $commentService, TopicCommentAccessControl $accessControl)
    {
        $model = new ViewTopicCommentRequest($request);
        $this->validateRequest($model);
        $accessControl->approveView($model->getClub(), $model->getTopic(), $model->getComment(), $this->getViewer());
        $permissions = $commentService->unfollow($model, $this->getViewer());

        return new JsonResponse($permissions);
    }
}