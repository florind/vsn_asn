<?php
namespace App\Controller;

use App\Request\SearchRequest;
use App\Service\SearchService;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class SearchController extends ApiController
{
    public function search(Request $request, SearchService $service)
    {
        $model = new SearchRequest($request);
        $this->validateRequest($model);
        $nodes = $service->search($model, $this->getViewer());

        return new JsonResponse($nodes);
    }
}