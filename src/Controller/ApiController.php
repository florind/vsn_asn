<?php
namespace App\Controller;

use App\Helper\JWT;
use App\Service\NodePermissions;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Validator\ConstraintViolationListInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class ApiController extends Controller
{
    /** @var  JWT */
    private $jwt;
    private $validator;

    public function __construct(ValidatorInterface $validator)
    {
        $this->validator = $validator;
    }

    protected function validateRequest($request)
    {
        $violations = $this->validator->validate($request);

        if ($violations->count()) {
            return $this->buildViolationResponse($violations);
        }
    }

    protected function buildViolationResponse(ConstraintViolationListInterface $violations)
    {
        $errors = [];
        foreach ($violations as $violation) {
            $errors[$violation->getPropertyPath()] = $violation->getMessage();
        }

        throw new BadRequestHttpException(json_encode([
            'error' => 'ASN_VALIDATION_ERROR',
            'details' => $errors
        ]));
    }

    protected function getViewer(): string
    {
        if (empty($this->jwt)) {
            throw new \RuntimeException('The JWT is not set!');
        }

        return $this->jwt->getProfileHash();
    }

    protected function approveView(string $nodeHash)
    {
        //@todo - remove this and it's dependencies
    }

    protected function approveEdit(string $nodeHash)
    {
        //@todo - remove this and it's dependencies
    }

    public function setJWT(JWT $jwt)
    {
        $this->jwt = $jwt;
    }
}