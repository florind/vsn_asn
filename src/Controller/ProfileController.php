<?php
namespace App\Controller;

use App\Request\ChangeAvatarRequest;
use App\Request\CreateProfileRequest;
use App\Request\NodeRelatedRequest;
use App\Request\UpdateProfileRequest;
use App\Request\Vehicle\DeleteMediaRequest;
use App\Service\ProfileService;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class ProfileController extends ApiController
{
    public function create(Request $request, ProfileService $profileService)
    {
        $model = new CreateProfileRequest($request);
        $this->validateRequest($model);
        $response = $profileService->create($model);

        return new JsonResponse($response);
    }

    public function view(Request $request, ProfileService $profileService)
    {
        $model = new NodeRelatedRequest($request);
        $this->validateRequest($model);

        $this->approveView($model->getNodeHash());
        $response = $profileService->view($model, $this->getViewer());

        return new JsonResponse($response);
    }

    public function update(Request $request, ProfileService $profileService)
    {
        $model = new UpdateProfileRequest($request);
        $this->validateRequest($model);
        $profileService->update($model, $this->getViewer());

        return new JsonResponse();
    }

    public function changeAvatar(Request $request, ProfileService $profileService)
    {
        $model = new ChangeAvatarRequest($request);
        $this->validateRequest($model);
        $profileService->changeAvatar($model, $this->getViewer());

        return new JsonResponse();
    }

    public function follow(Request $request, ProfileService $profileService)
    {
        $model = new NodeRelatedRequest($request);
        $this->validateRequest($model);
        $this->approveView($model->getNodeHash());
        $profileService->follow($model, $this->getViewer());

        return new JsonResponse();
    }

    public function unfollow(Request $request, ProfileService $profileService)
    {
        $model = new NodeRelatedRequest($request);
        $this->validateRequest($model);
        $this->approveView($model->getNodeHash());
        $profileService->unfollow($model, $this->getViewer());

        return new JsonResponse();
    }

    public function listFollowers(Request $request, ProfileService $profileService)
    {
        $model = new NodeRelatedRequest($request);
        $this->validateRequest($model);
        $this->approveView($model->getNodeHash());
        $list = $profileService->listFollowers($model, $this->getViewer());

        return new JsonResponse($list);
    }

    public function listFollowing(Request $request, ProfileService $profileService)
    {
        $model = new NodeRelatedRequest($request);
        $this->validateRequest($model);
        $this->approveView($model->getNodeHash());
        $list = $profileService->listFollowing($model, $this->getViewer());

        return new JsonResponse($list);
    }

    public function block(Request $request, ProfileService $profileService)
    {
        $model = new NodeRelatedRequest($request);
        $this->validateRequest($model);
        $this->approveView($model->getNodeHash());
        $profileService->block($model, $this->getViewer());

        return new JsonResponse();
    }

    public function unblock(Request $request, ProfileService $profileService)
    {
        $model = new NodeRelatedRequest($request);
        $this->validateRequest($model);
        $this->approveView($model->getNodeHash());
        $profileService->unblock($model, $this->getViewer());

        return new JsonResponse();
    }

    public function listBlocked(ProfileService $profileService)
    {
        $list = $profileService->listBlocked($this->getViewer());

        return new JsonResponse($list);
    }

    public function listClubMemberships(Request $request, ProfileService $profileService)
    {
        $model = new NodeRelatedRequest($request);
        $this->validateRequest($model);
        $this->approveView($model->getNodeHash());
        $list = $profileService->listClubMemberships($model, $this->getViewer());

        return new JsonResponse($list);
    }

    public function like(Request $request, ProfileService $profileService)
    {
        $model = new NodeRelatedRequest($request);
        $this->validateRequest($model);
        $this->approveView($model->getNodeHash());
        $count = $profileService->like($model, $this->getViewer());

        return new JsonResponse($count);
    }

    public function unlike(Request $request, ProfileService $profileService)
    {
        $model = new NodeRelatedRequest($request);
        $this->validateRequest($model);
        $this->approveView($model->getNodeHash());
        $count = $profileService->unlike($model, $this->getViewer());

        return new JsonResponse($count);
    }

    public function listLikes(Request $request, ProfileService $profileService)
    {
        $model = new NodeRelatedRequest($request);
        $this->validateRequest($model);
        $this->approveView($model->getNodeHash());
        $profiles = $profileService->listLikes($model, $this->getViewer());

        return new JsonResponse($profiles);
    }

    public function deleteMedia(Request $request, ProfileService $profileService)
    {
        $model = new DeleteMediaRequest($request);
        $this->validateRequest($model);

        $this->approveEdit($model->getMediaHash());
        $profileService->deleteMedia($model, $this->getViewer());

        return new JsonResponse();
    }
}