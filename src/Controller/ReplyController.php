<?php
namespace App\Controller;

use App\Request\Reply\CreateTopicReplyRequest;
use App\Request\Reply\DeleteReplyRequest;
use App\Request\Reply\EditTopicReplyRequest;
use App\Request\Reply\ListTopicRepliesRequest;
use App\Request\Reply\ViewTopicReplyRequest;
use App\Service\AccessControl\TopicReplyAccessControl;
use App\Service\ReplyService;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class ReplyController extends ApiController
{
    public function list(Request $request, ReplyService $replyService, TopicReplyAccessControl $accessControl)
    {
        $model = new ListTopicRepliesRequest($request);
        $this->validateRequest($model);

        $accessControl->approveList(
            $model->getClub(),
            $model->getTopic(),
            $model->getComment(),
            $this->getViewer()
        );

        $replies = $replyService->list($model, $this->getViewer());

        return new JsonResponse($replies);
    }

    public function view(Request $request, ReplyService $replyService, TopicReplyAccessControl $accessControl)
    {
        $model = new ViewTopicReplyRequest($request);
        $this->validateRequest($model);

        $accessControl->approveView(
            $model->getClub(),
            $model->getTopic(),
            $model->getComment(),
            $model->getReply(),
            $this->getViewer()
        );

        $reply = $replyService->get($model->getReply(), $this->getViewer());

        return new JsonResponse($reply);
    }

    public function create(Request $request, ReplyService $replyService, TopicReplyAccessControl $accessControl)
    {
        $model = new CreateTopicReplyRequest($request);
        $this->validateRequest($model);

        $accessControl->approveCreate(
            $model->getClub(),
            $model->getTopic(),
            $model->getComment(),
            $this->getViewer()
        );

        $replyService->create($model, $this->getViewer());

        return new JsonResponse();
    }

    public function update(Request $request, ReplyService $replyService, TopicReplyAccessControl $accessControl)
    {
        $model = new EditTopicReplyRequest($request);
        $this->validateRequest($model);

        $accessControl->approveEdit(
            $model->getClub(),
            $model->getTopic(),
            $model->getComment(),
            $model->getReply(),
            $this->getViewer()
        );

        $replyService->update($model, $this->getViewer());

        return new JsonResponse();
    }

    public function delete(Request $request, ReplyService $replyService, TopicReplyAccessControl $accessControl)
    {
        $model = new DeleteReplyRequest($request);
        $this->validateRequest($model);

        $accessControl->approveDelete(
            $model->getClub(),
            $model->getTopic(),
            $model->getComment(),
            $model->getReply(),
            $this->getViewer()
        );

        $replyService->delete($model, $this->getViewer());

        return new JsonResponse();
    }

}