<?php
namespace App\Helper;

use App\Exception\NotFoundException;

class Id64Bit
{
    private $shard;
    private $type;
    private $local;
    private $global;

    public static function decode(int $globalId)
    {
        $shard = $globalId >> 46 & 0xFFFF;
        $type = $globalId >> 36 & 0x3FF;
        $id = $globalId >> 0 & 0xFFFFFFFFF;

        return new self($globalId, $shard, $type, $id);
    }

    public static function encode(int $shard, int $type, int $localId)
    {
        $globalId = ($shard << 46) | ($type << 36) | ($localId << 0);

        return new self($globalId, $shard, $type, $localId);
    }

    private function __construct(int $globalId, int $shard, int $type, int $localId)
    {
        if (
            0 === $globalId &&
            0 === $shard &&
            0 === $type &&
            0 === $localId
        ) {
            throw new NotFoundException();
        }

        $this->global = $globalId;
        $this->shard = $shard;
        $this->type = $type;
        $this->local = $localId;
    }

    public function __toString()
    {
        return (string)$this->global;
    }

    public function getShard(): int
    {
        return $this->shard;
    }

    public function getType(): int
    {
        return $this->type;
    }

    public function getLocal(): int
    {
        return $this->local;
    }

    public function getGlobal(): int
    {
        return $this->global;
    }

}