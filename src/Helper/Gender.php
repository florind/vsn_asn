<?php
namespace App\Helper;

use Psr\Log\InvalidArgumentException;

class Gender
{
    const TYPE_MALE = 'male';
    const TYPE_FEMALE = 'female';

    private $type;

    public function __construct(string $type)
    {
        if (!in_array($type, [self::TYPE_MALE, self::TYPE_FEMALE])) {
            throw new InvalidArgumentException(sprintf('Invalid gender "%s"', $type));
        }

        $this->type = $type;
    }

    public function __toString()
    {
        return $this->type;
    }
}