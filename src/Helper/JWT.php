<?php
namespace App\Helper;

use App\Exception\InvalidTokenException;
use Jose\Component\Core\Converter\StandardConverter;
use Jose\Component\Signature\Serializer\CompactSerializer;

class JWT
{
    private $signedToken;
    private $payload;

    public function __construct(string $token)
    {
        if (empty($token)) {
            throw new InvalidTokenException();
        }

        $converter = new StandardConverter();
        $serializer = new CompactSerializer($converter);

        $this->signedToken = $serializer->unserialize($token);
        $this->payload = $converter->decode($this->signedToken->getPayload());

        //validate token
        if (
            empty($this->payload) ||
            !array_key_exists('pid', $this->payload) ||
            !array_key_exists('iat', $this->payload) ||
            !array_key_exists('exp', $this->payload)
        ) {
            throw new InvalidTokenException();
        }

        if ($this->payload['exp'] <= time()) {
            throw new InvalidTokenException();
        }
    }

    public function getProfileHash()
    {
        return $this->payload['pid'];
    }

    public function getSigned()
    {
        return $this->signedToken;
    }
}