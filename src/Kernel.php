<?php

namespace App;

use App\Edge\Edge;
use App\Hydrator\EdgeHydrator;
use App\Hydrator\NodeHydrator;
use App\Node\Node;
use App\Service\Encoder\NodeEncoderInterface;
use Symfony\Bundle\FrameworkBundle\Kernel\MicroKernelTrait;
use Symfony\Component\Config\Loader\LoaderInterface;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;
use Symfony\Component\HttpKernel\Kernel as BaseKernel;
use Symfony\Component\Routing\RouteCollectionBuilder;

class Kernel extends BaseKernel implements CompilerPassInterface
{
    use MicroKernelTrait;

    const CONFIG_EXTS = '.{php,xml,yaml,yml}';

    public function getCacheDir()
    {
        return $this->getProjectDir().'/var/cache/'.$this->environment;
    }

    public function getLogDir()
    {
        return $this->getProjectDir().'/var/log';
    }

    public function registerBundles()
    {
        $contents = require $this->getProjectDir().'/config/bundles.php';
        foreach ($contents as $class => $envs) {
            if (isset($envs['all']) || isset($envs[$this->environment])) {
                yield new $class();
            }
        }
    }

    protected function configureContainer(ContainerBuilder $container, LoaderInterface $loader)
    {
        // Feel free to remove the "container.autowiring.strict_mode" parameter
        // if you are using symfony/dependency-injection 4.0+ as it's the default behavior
        $container->setParameter('container.autowiring.strict_mode', true);
        $container->setParameter('container.dumper.inline_class_loader', true);
        $confDir = $this->getProjectDir().'/config';

        $loader->load($confDir.'/{packages}/*'.self::CONFIG_EXTS, 'glob');
        $loader->load($confDir.'/{packages}/'.$this->environment.'/**/*'.self::CONFIG_EXTS, 'glob');
        $loader->load($confDir.'/{services}'.self::CONFIG_EXTS, 'glob');
        $loader->load($confDir.'/{services}_'.$this->environment.self::CONFIG_EXTS, 'glob');
    }

    protected function configureRoutes(RouteCollectionBuilder $routes)
    {
        $confDir = $this->getProjectDir().'/config';

        $routes->import($confDir.'/{routes}/*'.self::CONFIG_EXTS, '/', 'glob');
        $routes->import($confDir.'/{routes}/'.$this->environment.'/**/*'.self::CONFIG_EXTS, '/', 'glob');
        $routes->import($confDir.'/{routes}'.self::CONFIG_EXTS, '/', 'glob');
    }

    protected function build(ContainerBuilder $container)
    {
        $container->registerForAutoconfiguration(Node::class)
            ->addTag('asn.node');
    }

    public function process(ContainerBuilder $container)
    {
        if ($container->has(NodeHydrator::class)) {
            $this->register(
                'asn.node',
                'NODE_TYPE',
                NodeHydrator::class,
                $container
            );
        }
    }

    /**
     * register nodes for hydration
     * @param string $tag
     * @param string $constant
     * @param string $service
     * @param ContainerBuilder $container
     */
    private function register(string $tag, string $constant, string $service, ContainerBuilder $container)
    {
        $definition = $container->findDefinition($service);
        $tagged = $container->findTaggedServiceIds($tag);

        foreach ($tagged as $id => $tags) {
            $ref = new \ReflectionClass($id);
            $definition->addMethodCall('register', [$ref->getConstant($constant), $id]);
        }
    }
}
