<?php
namespace App\EventSubscriber;

use App\Exception\ApiException;
use Doctrine\DBAL\DBALException;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\KernelEvents;

class CustomExceptionSubscriber implements EventSubscriberInterface
{
    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::EXCEPTION => 'exceptionJsonResponse'
        ];
    }

    public function exceptionJsonResponse(GetResponseForExceptionEvent $event)
    {
        $ex = $event->getException();

        if ($ex instanceof ApiException) {
            $event->setResponse(new JsonResponse(
                ['error' => $ex->getMessage()],
                $ex->getCode()
            ));
        } elseif ($ex instanceof BadRequestHttpException) {
            $event->setResponse(new JsonResponse($ex->getMessage(), Response::HTTP_BAD_REQUEST, [], true));
        } else {
            $event->setResponse(new JsonResponse(null, Response::HTTP_INTERNAL_SERVER_ERROR));
        }
    }
}