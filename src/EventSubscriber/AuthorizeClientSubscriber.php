<?php
namespace App\EventSubscriber;

use App\Controller\ApiController;
use App\Exception\AccessDeniedException;
use App\Exception\InvalidTokenException;
use App\Helper\JWT;
use App\Service\JWTSignatureValidatorService;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\FilterControllerEvent;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\KernelEvents;

class AuthorizeClientSubscriber implements EventSubscriberInterface
{
    private $accessKey;
    private $jwtValidator;

    public function __construct(string $accessKey, JWTSignatureValidatorService $jwtValidator)
    {
        $this->accessKey = $accessKey;
        $this->jwtValidator = $jwtValidator;
    }

    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::CONTROLLER => 'authorizeRequest'
        ];
    }

    public function authorizeRequest(FilterControllerEvent $event)
    {
        $eventCtrl = $event->getController();
        $controller = reset($eventCtrl);

        if ($controller instanceof ApiController) {
            list($apiKey, $jwt) = $this->extractFromBearerToken($event->getRequest());

            $this->validateApiKey($apiKey);

            if ($this->shouldValidateJWT($event)) {
                $jwt = $this->validateJWT($jwt);
                $controller->setJWT($jwt);
            }
        }
    }

    private function validateApiKey(string $apiKey)
    {
        if (empty($apiKey) || $this->accessKey !== $apiKey) {
            throw new NotFoundHttpException();
        }
    }

    private function validateJWT(string $jwt): JWT
    {
        try {
            $jwt = new JWT($jwt);
            $this->jwtValidator->validate($jwt);
            return $jwt;
        } catch (InvalidTokenException $ex) {
            throw new AccessDeniedException();
        }
    }

    private function extractFromBearerToken(Request $request)
    {
        $authorization = $request->headers->get('Authorization');

        if (empty($authorization)) {
            throw new AccessDeniedException();
        }

        $key = str_replace('Bearer ', '', $authorization);

        return explode(' ', base64_decode($key));
    }

    private function shouldValidateJWT(FilterControllerEvent $event)
    {
        return $event->getRequest()->attributes->get('validateJWT', true);
    }
}