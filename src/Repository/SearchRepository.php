<?php
namespace App\Repository;

use App\Helper\Id64Bit;
use App\Node\Node;
use App\Node\Post;
use App\Node\Profile;
use App\Node\Topic;
use Doctrine\DBAL\Connection;

class SearchRepository
{
    private $connection;

    public function __construct(Connection $connection)
    {
        $this->connection = $connection;
    }

    public function index(Id64Bit $id, Node $node, int $ownerId)
    {
        if ($node instanceof Profile) {
            $this->indexProfile($id, $node, $ownerId);
        }

        if ($node instanceof Post) {
            $this->indexPost($id, $node, $ownerId);
        }

        if ($node instanceof Topic) {
            $this->indexTopic($id, $node, $ownerId);
        }
    }

    public function updateIndex(Id64Bit $id, Node $node)
    {
        if ($node instanceof Profile) {
            $this->updateProfileIndex($id, $node);
        }

        if ($node instanceof Post) {
            $this->updatePostIndex($id, $node);
        }

        if ($node instanceof Topic) {
            $this->updateTopicIndex($id, $node);
        }
    }

    public function dropIndex(Id64Bit $id)
    {
        $this->connection->createQueryBuilder()
            ->delete('catalog')
            ->where('id = :id')
            ->setParameter('id', $id->getGlobal())
            ->execute();
    }

    public function search(string $term, int $type = null, array $ignoreOwners = [])
    {
        $qb = $this->connection->createQueryBuilder()
            ->select('id')
            ->from('catalog');

        if (null !== $type) {
            $qb->where('type = :type');
        }

        if (!empty($ignoreOwners)) {
            $qb->andWhere('owner NOT IN (:ignored)')
                ->setParameter('ignored', $ignoreOwners, Connection::PARAM_INT_ARRAY);
        }

        $results = $qb->andWhere('MATCH(content) AGAINST(:search)')
            ->setParameter('type', $type)
            ->setParameter('search', $term)
            ->execute()->fetchAll(\PDO::FETCH_COLUMN);

        return is_array($results) ? $results : [];
    }

    private function indexProfile(Id64Bit $id, Profile $node, $ownerId)
    {
        $this->connection->createQueryBuilder()
            ->insert('catalog')
            ->values([
                'id' => ':id',
                'type' => ':type',
                'content' => ':content',
                'owner' => ':owner'
            ])
            ->setParameter('id', $id->getGlobal())
            ->setParameter('type', $id->getType())
            ->setParameter('owner', $ownerId)
            ->setParameter('content', $node->getFirstName() . ' ' . $node->getLastName())
            ->execute();
    }

    private function indexPost(Id64Bit $id, Post $node, $ownerId)
    {
        $this->connection->createQueryBuilder()
            ->insert('catalog')
            ->values([
                'id' => ':id',
                'type' => ':type',
                'content' => ':content',
                'owner' => ':owner'
            ])
            ->setParameter('id', $id)
            ->setParameter('type', $id->getType())
            ->setParameter('content', $node->getText())
            ->setParameter('owner', $ownerId)
            ->execute();
    }

    private function indexTopic(Id64Bit $id, Topic $node, $ownerId)
    {
        $this->connection->createQueryBuilder()
            ->insert('catalog')
            ->values([
                'id' => ':id',
                'type' => ':type',
                'content' => ':content',
                'owner' => ':owner'
            ])
            ->setParameter('id', $id)
            ->setParameter('type', $id->getType())
            ->setParameter('content', $node->getTitle() . ' ' . $node->getText())
            ->setParameter('owner', $ownerId)
            ->execute();
    }

    private function updateProfileIndex(Id64Bit $id, Profile $node)
    {
        $this->connection->createQueryBuilder()
            ->update('catalog')
            ->set('content', ':content')
            ->where('id = :id')
            ->setParameter('id', $id)
            ->setParameter('content', $node->getFirstName() . ' ' . $node->getLastName())
            ->execute();
    }

    private function updatePostIndex(Id64Bit $id, Post $node)
    {
        $this->connection->createQueryBuilder()
            ->update('catalog')
            ->set('content', ':content')
            ->where('id = :id')
            ->setParameter('id', $id)
            ->setParameter('content', $node->getText())
            ->execute();
    }

    private function updateTopicIndex(Id64Bit $id, Topic $node)
    {
        $this->connection->createQueryBuilder()
            ->update('catalog')
            ->set('content', ':content')
            ->where('id = :id')
            ->setParameter('id', $id)
            ->setParameter('content', $node->getTitle() . ' ' . $node->getText())
            ->execute();
    }
}