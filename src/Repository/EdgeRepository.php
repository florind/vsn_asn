<?php
namespace App\Repository;

use App\Edge\Edge;
use App\Exception\NotFoundException;
use App\Helper\Id64Bit;
use App\Hydrator\EdgeHydrator;
use App\Service\ShardManager;
use Doctrine\DBAL\Connection;

class EdgeRepository
{
    /** @var  EdgeHydrator */
    private $hydrator;

    /** @var  ShardManager */
    private $shardManager;

    private $inverseMap = [
        Edge::TYPE_CREATED => Edge::TYPE_CREATED_BY,
        Edge::TYPE_BLOCKED => Edge::TYPE_BLOCKED_BY,
        Edge::TYPE_FOLLOWED => Edge::TYPE_FOLLOWED_BY,
        Edge::TYPE_LIKED => Edge::TYPE_LIKED_BY,
        Edge::TYPE_HOLDS => Edge::TYPE_HELD_BY,
        Edge::TYPE_MODERATES => Edge::TYPE_MODERATED_BY,
    ];

    public function __construct(EdgeHydrator $hydrator, ShardManager $shardManager)
    {
        $this->hydrator = $hydrator;
        $this->shardManager = $shardManager;
    }

    public function add(Id64Bit $owner, string $type, Id64Bit $inverse)
    {
        $this->insert(
                $this->shardManager->connect($owner->getShard()),
                $owner->getGlobal(),
                $type,
                $inverse->getGlobal(),
                $inverse->getType()
            );

        if (array_key_exists($type, $this->inverseMap)) {
            $this->insert(
                    $this->shardManager->connect($inverse->getShard()),
                    $inverse->getGlobal(),
                    $this->inverseMap[$type],
                    $owner->getGlobal(),
                    $owner->getType()
                );
        }
    }

    private function insert(Connection $connection, $owner, $type, $inverse, $inverseType)
    {
        $connection->createQueryBuilder()
            ->insert('edge')
            ->setValue('type', ':type')
            ->setValue('owner_node', ':owner_node')
            ->setValue('inverse_node', ':inverse_node')
            ->setValue('inverse_node_type', ':inverse_node_type')
            ->setValue('created', ':created')
            ->setParameters([
                'type' => $type,
                'owner_node' => $owner,
                'inverse_node' => $inverse,
                'inverse_node_type' => $inverseType,
                'created' => (new \DateTime())->format('Y-m-d H:i:s')
            ])->execute();
    }

    public function drop(Id64Bit $ownerNode, string $type, Id64Bit $inverseNode)
    {
        $this->delete(
                $this->shardManager->connect($ownerNode->getShard()),
                $ownerNode->getGlobal(),
                $type,
                $inverseNode->getGlobal(),
                $inverseNode->getType()
            );

        if (array_key_exists($type, $this->inverseMap)) {
            $this->delete(
                    $this->shardManager->connect($inverseNode->getShard()),
                    $inverseNode->getGlobal(),
                    $this->inverseMap[$type],
                    $ownerNode->getGlobal(),
                    $ownerNode->getType()
                );
        }
    }

    private function delete(Connection $connection, $owner, $type, $inverse, $inverseType)
    {
        return (bool) $connection->createQueryBuilder()
            ->delete('edge')
            ->where('owner_node=:owner_node AND inverse_node=:inverse_node AND type=:type AND inverse_node_type=:inverse_node_type')
            ->setParameters([
                'owner_node' => $owner,
                'inverse_node' => $inverse,
                'inverse_node_type' => $inverseType,
                'type' => $type
            ])->execute();
    }

    public function exists(Id64Bit $ownerNode, string $type, Id64Bit $inverseNode): bool
    {
        $edge = $this->shardManager->connect($ownerNode->getShard())->createQueryBuilder()
            ->select('id')
            ->from('edge')
            ->where('owner_node=:owner_node')
            ->andWhere('inverse_node=:inverse_node')
            ->andWhere('type=:type')
            ->andWhere('inverse_node_type=:inverse_node_type')
            ->orderBy('created', 'DESC')
            ->setParameters([
                'owner_node' => $ownerNode->getGlobal(),
                'inverse_node' => $inverseNode->getGlobal(),
                'inverse_node_type' => $inverseNode->getType(),
                'type' => $type
            ])->execute()->fetch(\PDO::FETCH_ASSOC);

        return $edge ? true : false;
    }

    public function findOne(
        Id64Bit $ownerNode,
        $type,
        int $inverseNodeType
    ) {
        $result = $this->shardManager->connect($ownerNode->getShard())->createQueryBuilder()
            ->select('*')
            ->from('edge')
            ->where('owner_node = :node AND type = :type')
            ->andWhere('inverse_node_type = :inverse_node_type')
            ->setParameter('node', $ownerNode->getGlobal())
            ->setParameter('inverse_node_type', $inverseNodeType)
            ->setParameter('type', $type)
            ->execute()->fetch(\PDO::FETCH_ASSOC);

        if (false === $result || empty($result)) {
            throw new NotFoundException();
        }

        return $this->hydrator->hydrate($result);
    }

    public function find(
        Id64Bit $ownerNode,
        $type,
        int $inverseNodeType,
        int $page = null,
        int $limit = null,
        string $order = 'ASC'
    ): array {
        $types = is_array($type) ? $type : [$type];
        $qb = $this->shardManager->connect($ownerNode->getShard())->createQueryBuilder()
            ->select('*')
            ->from('edge')
            ->where('owner_node = :node AND type IN (:types)')
            ->andWhere('inverse_node_type = :inverse_node_type')
            ->setParameter('node', $ownerNode->getGlobal())
            ->setParameter('inverse_node_type', $inverseNodeType)
            ->setParameter('types', $types, Connection::PARAM_STR_ARRAY)
            ->orderBy('created', $order);

        if (!in_array(null, [$page, $limit])) {
            $qb->setFirstResult(--$page * $limit)->setMaxResults($limit);
        }

        $results = $qb->execute()->fetchAll(\PDO::FETCH_ASSOC);

        $edges = [];

        foreach ($results as $result) {
            $edges[] = $this->hydrator->hydrate($result);
        }

        return $edges;
    }

    public function findInverseNodeIds(Id64Bit $ownerNode, string $type, int $inverseNodeType): array
    {
        return $this->shardManager->connect($ownerNode->getShard())->createQueryBuilder()
            ->select('inverse_node')
            ->from('edge')
            ->where('owner_node = :node AND type = :type AND inverse_node_type = :inverse_node_type')
            ->setParameter('node', $ownerNode->getGlobal())
            ->setParameter('type', $type)
            ->setParameter('inverse_node_type', $inverseNodeType)
            ->orderBy('created', 'DESC')
            ->execute()->fetchAll(\PDO::FETCH_COLUMN);
    }

    public function findBatch(int $shard, array $ownerNodes, string $type, int $inverseNodeType, int $linkedNode = null): array
    {
        $qb = $this->shardManager->connect($shard)->createQueryBuilder()
            ->select('*')
            ->from('edge')
            ->where('owner_node IN (:nodes) AND type = :type AND inverse_node_type=:inverse_node_type')
            ->setParameter('nodes', $ownerNodes, Connection::PARAM_STR_ARRAY)
            ->setParameter('type', $type)
            ->setParameter('inverse_node_type', $inverseNodeType);

        if (null !== $linkedNode) {
            $qb->andWhere('inverse_node = :linked_node')
                ->setParameter('linked_node', $linkedNode);
        }

        $qb->orderBy('created', 'DESC');

        $edges = [];

        $results = $qb->execute()->fetchAll(\PDO::FETCH_ASSOC);

        foreach ($results as $result) {
            $edges[] = $this->hydrator->hydrate($result);
        }

        return $edges;
    }

    public function findCreatorId(Id64Bit $node)
    {
        $result = $this->shardManager->connect($node->getShard())->createQueryBuilder()
            ->select('inverse_node')
            ->from('edge')
            ->where('owner_node = :node AND type = :type')
            ->setParameters([
                'node' => $node->getGlobal(),
                'type' => Edge::TYPE_CREATED_BY
            ])->execute()->fetchColumn();

        if (!$result) {
            throw new NotFoundException();
        }

        return $result;
    }

    public function count(Id64Bit $ownerNode, string $type, int $inverseNodeType, array $ignoreLinkedNodeIds = [])
    {
        $qb = $this->shardManager->connect($ownerNode->getShard())->createQueryBuilder()
            ->select('COUNT(id)')
            ->from('edge')
            ->where('owner_node = :node AND type = :type AND inverse_node_type = :inverse_node_type')
            ->setParameters([
                'node' => $ownerNode->getGlobal(),
                'type' => $type,
                'inverse_node_type' => $inverseNodeType
            ]);

        if (!empty($ignoreLinkedNodeIds)) {
            $qb->andWhere('inverse_node NOT IN (:ignoreList)')
                ->setParameter('ignoreList', $ignoreLinkedNodeIds, Connection::PARAM_INT_ARRAY);
        }

        return $qb->execute()->fetchColumn();
    }

    public function getRelationship(Id64Bit $ownerNode, Id64Bit $inverseNode)
    {
        return $this->shardManager->connect($ownerNode->getShard())->createQueryBuilder()
            ->select('type')
            ->from('edge')
            ->where('owner_node = :owner_node AND inverse_node = :inverse_node')
            ->setParameters([
                'owner_node' => $ownerNode->getGlobal(),
                'inverse_node' => $inverseNode->getGlobal()
            ])->execute()->fetchAll(\PDO::FETCH_COLUMN);
    }
}