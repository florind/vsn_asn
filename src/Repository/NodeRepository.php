<?php
namespace App\Repository;

use App\Exception\NotFoundException;
use App\Helper\Id64Bit;
use App\Hydrator\NodeHydrator;
use App\Node\Node;
use App\Service\ShardManager;
use Doctrine\DBAL\Connection;

class NodeRepository
{
    private $hydrator;
    private $shardManager;

    public function __construct(NodeHydrator $hydrator, ShardManager $shardManager)
    {
        $this->hydrator = $hydrator;
        $this->shardManager = $shardManager;
    }

    public function addToShard(Node $node, int $shard): Id64Bit
    {
        $connection = $this->shardManager->connect($shard);

        $connection->createQueryBuilder()
            ->insert('node')
            ->setValue('type', ':type')
            ->setValue('data', ':data')
            ->setValue('created', ':created')
            ->setValue('updated', ':updated')
            ->setParameters([
                'type' => $node->getType(),
                'data' => json_encode($node->getProperties()),
                'created' => (new \DateTime())->format('Y-m-d H:i:s'),
                'updated' => (new \DateTime())->format('Y-m-d H:i:s')
            ])->execute();

        return Id64Bit::encode($shard, $node->getType(), $connection->lastInsertId());
    }

    public function update(Id64Bit $id, Node $node)
    {
        $this->shardManager->connect($id->getShard())->createQueryBuilder()
            ->update('node')
            ->set('data', ':data')
            ->set('updated', ':updated')
            ->where('id = :id')
            ->setParameters([
                'id' => $id->getLocal(),
                'data' => json_encode($node->getProperties()),
                'updated' => (new \DateTime())->format('Y-m-d H:i:s')
            ])->execute();
    }

    public function drop(Id64Bit $id): bool
    {
        return (bool) $this->shardManager->connect($id->getShard())->createQueryBuilder()
            ->delete('node')
            ->where('id = :id')
            ->setParameter('id', $id->getLocal())->execute();
    }

    public function batchDrop(array $nodeIds): bool
    {
        $shards = [];

        /** @var Id64Bit $id */
        foreach ($nodeIds as $id) {
            $shards[$id->getShard()] = $id->getGlobal();
        }

        return (bool) $this->shardManager->connect($id->getShard())->createQueryBuilder()
            ->delete('node')
            ->where('id = :id')
            ->setParameter('id', $id->getLocal())->execute();
    }

    public function findOne(Id64Bit $id)
    {
        $node = $this->shardManager->connect($id->getShard())->createQueryBuilder()
            ->select('*')
            ->from('node')
            ->where('id=:id')
            ->setParameter('id', $id->getLocal())
            ->execute()->fetch(\PDO::FETCH_ASSOC);

        if (false === $node) {
            throw new NotFoundException();
        }

        return $this->hydrator->hydrate($node);
    }

    public function find(array $globalIds): array
    {
        $ids = $nodes = $results = [];

        foreach ($globalIds as $id) {
            if (!$id instanceof Id64Bit) {
                throw new \InvalidArgumentException('argument must be an array of Id64Bit objects');
            }
            $ids[$id->getShard()][] = $id->getLocal();
        }

        foreach ($ids as $shard => $localIds) {
            if (!array_key_exists($shard, $results)) {
                $results[$shard] = [];
            }
            $results[$shard] = array_merge($results[$shard], $this->shardManager->connect($shard)->createQueryBuilder()
                ->select('*')
                ->from('node')
                ->where('id IN (:ids)')
                ->setParameter('ids', $localIds, Connection::PARAM_INT_ARRAY)
                ->execute()->fetchAll(\PDO::FETCH_ASSOC));
        }

        foreach ($results as $shard => $entries) {
            foreach ($entries as $node) {
                $globalId = Id64Bit::encode($shard, $node['type'], $node['id']);
                $nodes[$globalId->getGlobal()] = $this->hydrator->hydrate($node);
            }
        }

        return $nodes;
    }
}