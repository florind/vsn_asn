<?php
namespace App\Repository;

use Doctrine\DBAL\Connection;

class LocalityRepository
{
    private $connection;

    public function __construct(Connection $connection)
    {
        $this->connection = $connection;
    }

    public function exists(string $name)
    {
        $count = $this->connection->createQueryBuilder()
            ->select('count(id) as count')
            ->from('locality')
            ->where('name = :name')
            ->setParameter('name', $name)
            ->execute()->fetchColumn();

        return (bool)$count;
    }
}