<?php
namespace App\Repository;

use Doctrine\DBAL\Connection;

class VehicleRepository
{
    private $connection;

    public function __construct(Connection $connection)
    {
        $this->connection = $connection;
    }

    public function exists(string $type, string $brand, string $model)
    {
        $count = $this->connection->createQueryBuilder()
            ->select('count(id) as count')
            ->from('vehicle')
            ->where('type = :type AND brand = :brand AND model = :model')
            ->setParameters([
                'type' => $type,
                'brand' => $brand,
                'model' => $model
            ])
            ->execute()->fetchColumn();

        return (bool)$count;
    }
}