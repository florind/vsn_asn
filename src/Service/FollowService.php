<?php
namespace App\Service;

use App\Edge\Edge;
use App\Exception\ProcessRequestException;
use App\Helper\Id64Bit;
use App\Repository\EdgeRepository;
use Doctrine\DBAL\DBALException;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;

class FollowService
{
    private $edgeRepository;
    private $idEncoder;

    public function __construct(
        HashIdService $hashIdService,
        EdgeRepository $edgeRepository
    ) {
        $this->edgeRepository = $edgeRepository;
        $this->idEncoder = $hashIdService;
    }

    public function follow(Id64Bit $followedId, Id64Bit $followerId)
    {
        if ($followerId->getGlobal() === $followedId->getGlobal()) {
            return;
        }

        try {
            $this->edgeRepository->add($followerId, Edge::TYPE_FOLLOWED, $followedId);
        } catch (UniqueConstraintViolationException $ex) {
            return;
        } catch (DBALException $ex) {
            throw new ProcessRequestException();
        }
    }

    public function unfollow(string $nodeHash, string $profileHash)
    {
        $followerId = Id64Bit::decode($this->idEncoder->decode($profileHash));
        $followedId = Id64Bit::decode($this->idEncoder->decode($nodeHash));

        if ($followerId->getGlobal() === $followedId->getGlobal()) {
            return;
        }

        try {
            $this->edgeRepository->drop($followerId, Edge::TYPE_FOLLOWED, $followedId);
        } catch (DBALException $exception) {
            throw new ProcessRequestException();
        }
    }
}