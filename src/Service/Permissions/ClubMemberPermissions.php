<?php
namespace App\Service\Permissions;

use App\Edge\Edge;
use App\Helper\Id64Bit;
use App\Repository\EdgeRepository;
use App\Service\Permissions\PermissionsInterface as Perms;

class ClubMemberPermissions implements Perms
{
    private $edgeRepository;

    public function __construct(EdgeRepository $edgeRepository)
    {
        $this->edgeRepository = $edgeRepository;
    }

    public function list(Id64Bit $viewer, Id64Bit $club, Id64Bit $member)
    {
        $viewerClubEdges = $this->edgeRepository->getRelationship($viewer, $club);
        $memberClubEdges = $this->edgeRepository->getRelationship($member, $club);
        $viewerMemberEdges = $this->edgeRepository->getRelationship($viewer, $member);

        if (
            in_array(Edge::TYPE_BLOCKED_BY, $viewerClubEdges) ||
            in_array(Edge::TYPE_BLOCKED, $viewerMemberEdges) ||
            in_array(Edge::TYPE_BLOCKED_BY, $viewerMemberEdges)
        ) {
            return [];
        }

        $permissions = [Perms::VIEW];

        if (empty($viewerMemberEdges)) {
            $permissions[] = Perms::FOLLOW;
        }

        if (
            in_array(Edge::TYPE_MODERATES, $viewerClubEdges) &&
            !in_array(Edge::TYPE_MODERATES, $memberClubEdges)
        ) {
            $permissions[] = Perms::BLOCK;
        }

        if (in_array(Edge::TYPE_FOLLOWED, $viewerMemberEdges)) {
            $permissions[] = Perms::UNFOLLOW;
        }

        return $permissions;
    }
}