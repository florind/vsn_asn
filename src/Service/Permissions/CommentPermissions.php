<?php
namespace App\Service\Permissions;

use App\Edge\Edge;
use App\Helper\Id64Bit;
use App\Node\Club;
use App\Node\Topic;
use App\Repository\EdgeRepository;
use App\Repository\NodeRepository;
use App\Service\Permissions\PermissionsInterface as Perms;

class CommentPermissions implements Perms
{
    private $edgeRepository;
    private $nodeRepository;

    public function __construct(EdgeRepository $edgeRepository, NodeRepository $nodeRepository)
    {
        $this->edgeRepository = $edgeRepository;
        $this->nodeRepository = $nodeRepository;
    }

    public function list(Id64Bit $viewer, Id64Bit $comment)
    {
        /** @var Topic $topicNode */
        $topicEdge = $this->edgeRepository->findOne($comment, Edge::TYPE_HELD_BY, Topic::NODE_TYPE);
        $topic = Id64Bit::decode($topicEdge->getInverseNode());
        $topicNode = $this->nodeRepository->findOne($topic);

        $clubEdge = $this->edgeRepository->findOne($topic, Edge::TYPE_HELD_BY, Club::NODE_TYPE);
        $club = Id64Bit::decode($clubEdge->getInverseNode());

        $clubEdges = $this->edgeRepository->getRelationship($viewer, $club);
        $commentEdges = $this->edgeRepository->getRelationship($viewer, $comment);

        if (in_array(Edge::TYPE_BLOCKED_BY, $clubEdges)) {
            return [];
        }

        $permissions = [Perms::VIEW];

        if (!$topicNode->isLocked()) {
            if (in_array(Edge::TYPE_CREATED, $commentEdges)) {
                $permissions[] = Perms::EDIT;
                $permissions[] = Perms::DELETE;
            }

            $permissions[] = in_array(Edge::TYPE_FOLLOWED, $commentEdges) ? Perms::UNFOLLOW : Perms::FOLLOW;
        }

        $permissions[] = in_array(Edge::TYPE_LIKED, $commentEdges) ? Perms::UNLIKE : Perms::LIKE;

        return $permissions;
    }
}