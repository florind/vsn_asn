<?php
namespace App\Service\Permissions;

use App\Edge\Edge;
use App\Helper\Id64Bit;
use App\Repository\EdgeRepository;
use App\Service\Permissions\PermissionsInterface as Perms;

class ClubPermissions implements Perms
{
    private $edgeRepository;

    public function __construct(EdgeRepository $edgeRepository)
    {
        $this->edgeRepository = $edgeRepository;
    }

    public function list(Id64Bit $viewer, Id64Bit $club)
    {
        $edges = $this->edgeRepository->getRelationship($viewer, $club);

        if (in_array(Edge::TYPE_BLOCKED_BY, $edges)) {
            return [];
        }

        $permissions = [Perms::VIEW];

        if (empty($edges)) {
            $permissions[] = Perms::FOLLOW;
        }

        if (in_array(Edge::TYPE_FOLLOWED, $edges)) {
            $permissions[] = Perms::UNFOLLOW;
        }

        if (in_array(Edge::TYPE_CREATED, $edges)) {
            $permissions[] = Perms::EDIT;
            $permissions[] = Perms::DELETE;
        }

        return $permissions;
    }
}