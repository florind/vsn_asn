<?php
namespace App\Service\Permissions;

interface PermissionsInterface
{
    const VIEW = 'VIEW';
    const EDIT = 'EDIT';
    const DELETE = 'DELETE';
    const FOLLOW = 'FOLLOW';
    const UNFOLLOW = 'UNFOLLOW';
    const BLOCK = 'BLOCK';
    const UNBLOCK = 'UNBLOCK';
    const LOCK = 'LOCK';
    const UNLOCK = 'UNLOCK';
    const LIKE = 'LIKE';
    const UNLIKE = 'UNLIKE';
}