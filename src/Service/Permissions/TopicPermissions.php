<?php
namespace App\Service\Permissions;

use App\Edge\Edge;
use App\Helper\Id64Bit;
use App\Node\Club;
use App\Node\Topic;
use App\Repository\EdgeRepository;
use App\Repository\NodeRepository;
use App\Service\Permissions\PermissionsInterface as Perms;

class TopicPermissions implements Perms
{
    private $edgeRepository;
    private $nodeRepository;

    public function __construct(EdgeRepository $edgeRepository, NodeRepository $nodeRepository)
    {
        $this->edgeRepository = $edgeRepository;
        $this->nodeRepository = $nodeRepository;
    }

    public function list(Id64Bit $viewer, Id64Bit $topic)
    {
        /** @var Topic $topicNode */
        $topicNode = $this->nodeRepository->findOne($topic);

        $clubEdge = $this->edgeRepository->findOne($topic, Edge::TYPE_HELD_BY, Club::NODE_TYPE);
        $club = Id64Bit::decode($clubEdge->getInverseNode());

        $clubEdges = $this->edgeRepository->getRelationship($viewer, $club);
        $topicEdges = $this->edgeRepository->getRelationship($viewer, $topic);

        if (in_array(Edge::TYPE_BLOCKED_BY, $clubEdges)) {
            return [];
        }

        $permissions = [Perms::VIEW];

        if (!$topicNode->isLocked()) {
            if (in_array(Edge::TYPE_CREATED, $topicEdges)) {
                $permissions[] = Perms::EDIT;
                $permissions[] = Perms::DELETE;
            }

            $permissions[] = in_array(Edge::TYPE_FOLLOWED, $topicEdges) ? Perms::UNFOLLOW : Perms::FOLLOW;
        }

        $permissions[] = in_array(Edge::TYPE_LIKED, $topicEdges) ? Perms::UNLIKE : Perms::LIKE;

        if (in_array(Edge::TYPE_MODERATES, $clubEdges)) {
            $permissions[] = $topicNode->isLocked() ? Perms::UNLOCK : Perms::LOCK;
        }

        return $permissions;
    }
}