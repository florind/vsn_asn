<?php
namespace App\Service\Permissions;

use App\Edge\Edge;
use App\Helper\Id64Bit;
use App\Repository\EdgeRepository;
use App\Service\Permissions\PermissionsInterface as Perms;

class ProfilePermissions implements Perms
{
    private $edgeRepository;

    public function __construct(EdgeRepository $edgeRepository)
    {
        $this->edgeRepository = $edgeRepository;
    }

    public function list(Id64Bit $viewer, Id64Bit $profile)
    {
        if ($viewer->getGlobal() === $profile->getGlobal()) {
            return [Perms::VIEW, Perms::EDIT, Perms::DELETE];
        }

        $edges = $this->edgeRepository->getRelationship($viewer, $profile);

        if (in_array(Edge::TYPE_BLOCKED_BY, $edges)) {
            return [];
        }

        $permissions = [Perms::VIEW, Perms::BLOCK];

        if (empty($edges)) {
            $permissions[] = Perms::FOLLOW;
        }

        if (in_array(Edge::TYPE_FOLLOWED, $edges)) {
            $permissions[] = Perms::UNFOLLOW;
        }

        if (in_array(Edge::TYPE_BLOCKED, $edges)) {
            $permissions[] = Perms::UNBLOCK;
        }

        return $permissions;
    }
}