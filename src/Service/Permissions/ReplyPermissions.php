<?php
namespace App\Service\Permissions;

use App\Edge\Edge;
use App\Helper\Id64Bit;
use App\Node\Club;
use App\Node\Topic;
use App\Repository\EdgeRepository;
use App\Repository\NodeRepository;
use App\Service\Permissions\PermissionsInterface as Perms;

class ReplyPermissions implements Perms
{
    private $edgeRepository;
    private $nodeRepository;

    public function __construct(EdgeRepository $edgeRepository, NodeRepository $nodeRepository)
    {
        $this->edgeRepository = $edgeRepository;
        $this->nodeRepository = $nodeRepository;
    }

    public function list(Id64Bit $viewer, Id64Bit $reply)
    {
        /** @var Topic $topicNode */
        $topicEdge = $this->edgeRepository->findOne($reply, Edge::TYPE_HELD_BY, Topic::NODE_TYPE);
        $topic = Id64Bit::decode($topicEdge->getInverseNode());
        $topicNode = $this->nodeRepository->findOne($topic);

        $clubEdge = $this->edgeRepository->findOne($topic, Edge::TYPE_HELD_BY, Club::NODE_TYPE);
        $club = Id64Bit::decode($clubEdge->getInverseNode());

        $clubEdges = $this->edgeRepository->getRelationship($viewer, $club);
        $replyEdges = $this->edgeRepository->getRelationship($viewer, $reply);

        if (in_array(Edge::TYPE_BLOCKED_BY, $clubEdges)) {
            return [];
        }

        $permissions = [Perms::VIEW];

        $permissions[] = in_array(Edge::TYPE_LIKED, $replyEdges) ? Perms::UNLIKE : Perms::LIKE;

        if (
            in_array(Edge::TYPE_CREATED, $replyEdges) &&
            !$topicNode->isLocked()
        ) {
            $permissions[] = Perms::EDIT;
            $permissions[] = Perms::DELETE;
        }

        if (in_array(Edge::TYPE_LIKED, $replyEdges)) {
            $permissions[] = Perms::UNLIKE;
        }

        return $permissions;
    }
}