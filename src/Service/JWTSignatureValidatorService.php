<?php
namespace App\Service;

use App\Exception\InvalidTokenException;
use App\Helper\JWT;
use Jose\Component\Core\JWK;
use Jose\Component\Signature\JWSVerifier;

class JWTSignatureValidatorService
{
    private $signatureKey;
    private $signatureVerifier;

    public function __construct(JWK $key, JWSVerifier $signatureVerifier)
    {
        $this->signatureKey = $key;
        $this->signatureVerifier = $signatureVerifier;
    }

    public function validate(JWT $token)
    {
        if (!$this->signatureVerifier->verifyWithKey($token->getSigned(), $this->signatureKey, 0)){
            throw new InvalidTokenException();
        }
    }
}