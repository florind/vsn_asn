<?php
namespace App\Service;

use App\Edge\Edge;
use App\Exception\NotFoundException;
use App\Helper\Id64Bit;
use App\Node\Club;
use App\Node\Profile;
use App\Node\Topic;
use App\Repository\EdgeRepository;
use App\Repository\NodeRepository;
use App\Request\Club\BanMemberRequest;
use App\Request\Club\CreateClubRequest;
use App\Request\Club\ListMembersRequest;
use App\Request\NodeRelatedRequest;
use App\Service\Permissions\ClubMemberPermissions;
use App\Service\Permissions\ClubPermissions;
use App\ViewModel\ClubViewModel;
use App\ViewModel\ClubMemberViewModel;
use App\ViewModel\MemberListViewModel;
use App\ViewModel\MinimalClubViewModel;
use App\ViewModel\MinimalProfileViewModel;

class ClubService
{
    private $nodeRepository;
    private $edgeRepository;
    private $idEncoder;
    private $relationshipService;
    private $followService;
    private $clubPermissions;
    private $clubMemberPermissions;

    public function __construct(
        HashIdService $hashIdService,
        NodeRepository $nodeRepository,
        EdgeRepository $edgeRepository,
        FollowService $followService,
        NodesRelationDescriber $relationshipService,
        ClubPermissions $clubPermissions,
        ClubMemberPermissions $clubMemberPermissions
    ) {
        $this->idEncoder = $hashIdService;
        $this->nodeRepository = $nodeRepository;
        $this->edgeRepository = $edgeRepository;
        $this->followService = $followService;
        $this->relationshipService = $relationshipService;
        $this->clubPermissions = $clubPermissions;
        $this->clubMemberPermissions = $clubMemberPermissions;
    }

    public function view(string $clubHash, string $profileHash)
    {
        $viewerId = Id64Bit::decode($this->idEncoder->decode($profileHash));

        $clubId = Id64Bit::decode($this->idEncoder->decode($clubHash));
        $club = $this->nodeRepository->findOne($clubId);

        $ownerId = $this->edgeRepository->findCreatorId($clubId);
        $owner = $this->nodeRepository->findOne(Id64Bit::decode($ownerId));

        $topicCount = $this->edgeRepository->count($clubId, Edge::TYPE_HOLDS, Topic::NODE_TYPE);
        $memberCount = $this->edgeRepository->count($clubId, Edge::TYPE_FOLLOWED_BY, Profile::NODE_TYPE);

        return new ClubViewModel(
            $clubHash,
            $club,
            new MinimalProfileViewModel($this->idEncoder->encode($ownerId), $owner),
            $memberCount,
            $topicCount,
            $this->clubPermissions->list($viewerId, $clubId)
        );
    }

    public function list(string $profileHash)
    {
        $profileId = Id64Bit::decode($this->idEncoder->decode($profileHash));

        $clubEdges = $this->edgeRepository->find($profileId, Edge::TYPE_FOLLOWED, Club::NODE_TYPE);
        $clubIds = $clubs = [];

        /** @var Edge $edge */
        foreach ($clubEdges as $edge) {
            $clubIds[] = Id64Bit::decode($edge->getInverseNode());
        }

        if (empty($clubIds)) {
            return [];
        }

        $clubNodes = $this->nodeRepository->find($clubIds);

        foreach ($clubNodes as $id => $node) {
            $id64Bit = Id64Bit::decode($id);
            $clubs[] = new MinimalClubViewModel(
                $this->idEncoder->encode($id),
                $node,
                $this->edgeRepository->count($id64Bit, Edge::TYPE_FOLLOWED_BY, Profile::NODE_TYPE),
                $this->clubPermissions->list($profileId, $id64Bit)
            );
        }

       return $clubs;
    }

    public function create(CreateClubRequest $model, string $profileHash)
    {
        $profileId = Id64Bit::decode($this->idEncoder->decode($profileHash));

        $club = (new Club())->setName($model->getName());

        if (null !== $model->getDescription()) {
            $club->setDescription($model->getDescription());
        }

        if (null !== $model->getLogo()) {
            $club->setLogo($model->getLogo());
        }

        $clubId = $this->nodeRepository->addToShard($club, $profileId->getShard());
        $this->edgeRepository->add($profileId, Edge::TYPE_CREATED, $clubId);
        $this->edgeRepository->add($profileId, Edge::TYPE_FOLLOWED, $clubId);
        $this->edgeRepository->add($profileId, Edge::TYPE_MODERATES, $clubId);

    }

    public function join(NodeRelatedRequest $model, string $profileHash)
    {
        $club = Id64Bit::decode($this->idEncoder->decode($model->getNodeHash()));
        $viewer = Id64Bit::decode($this->idEncoder->decode($profileHash));

        $this->followService->follow($club, $viewer);
    }
    
    public function leave(NodeRelatedRequest $model, string $profileHash)
    {
        $this->followService->unfollow($model->getNodeHash(), $profileHash);
    }

    public function members(ListMembersRequest $model, string $viewerHash)
    {
        $viewer = Id64Bit::decode($this->idEncoder->decode($viewerHash));
        $clubId = Id64Bit::decode($this->idEncoder->decode($model->getClubHash()));
        $members = $memberIds = $joinDates = [];

        $memberCount = $this->edgeRepository->count($clubId, Edge::TYPE_FOLLOWED, Profile::NODE_TYPE);

        if ($clubId->getType() !== Club::NODE_TYPE) {
            throw new NotFoundException();
        }

        $edges = $this->edgeRepository->find($clubId,
            Edge::TYPE_FOLLOWED_BY,
            Profile::NODE_TYPE,
            $model->getPage(),
            $model->getLimit()
        );

        /** @var Edge $edge */
        foreach ($edges as $edge) {
            $memberIds[] = Id64Bit::decode($edge->getInverseNode());
            $joinDates[$edge->getInverseNode()] = $edge->getCreated();
        }

        $profiles = $this->nodeRepository->find($memberIds);

        foreach ($profiles as $id => $profile) {
            $members[] = new ClubMemberViewModel(
                $this->idEncoder->encode($id),
                $profile,
                $joinDates[$id],
                $this->clubMemberPermissions->list($viewer, $clubId, Id64Bit::decode($id))
            );
        }

        return new MemberListViewModel($memberCount, $members);
    }

    public function banMember(BanMemberRequest $model, string $viewer)
    {
        $clubId = Id64Bit::decode($this->idEncoder->decode($model->getClub()));
        $member = Id64Bit::decode($this->idEncoder->decode($model->getMember()));

        $this->edgeRepository->add($clubId, Edge::TYPE_BLOCKED, $member);
        //@todo - maybe also add a way to know who banned whom from the club.
    }
}