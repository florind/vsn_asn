<?php
namespace App\Service;

use App\Edge\Edge;
use App\Exception\AccessDeniedException;
use App\Exception\ProcessRequestException;
use App\Helper\Id64Bit;
use App\Node\Comment;
use App\Node\Profile;
use App\Node\Topic;
use App\Repository\EdgeRepository;
use App\Repository\NodeRepository;
use App\Request\Comment\CreateTopicCommentRequest;
use App\Request\Comment\DeleteCommentRequest;
use App\Request\Comment\EditTopicCommentRequest;
use App\Request\Comment\ListTopicCommentsRequest;
use App\Request\Comment\ViewTopicCommentRequest;
use App\Service\Permissions\CommentPermissions;
use App\ViewModel\CommentListViewModel;
use App\ViewModel\CommentViewModel;
use App\ViewModel\MinimalProfileViewModel;
use Doctrine\DBAL\DBALException;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;

class CommentService
{
    private $nodeRepository;
    private $edgeRepository;
    private $idEncoder;
    private $likeService;
    private $blockListService;
    private $mediaService;
    private $replyService;
    private $permissions;
    private $followService;

    public function __construct(
        HashIdService $hashIdService,
        NodeRepository $nodeRepository,
        EdgeRepository $edgeRepository,
        LikeService $likeService,
        BlockListService $blockListService,
        MediaService $mediaService,
        ReplyService $replyService,
        CommentPermissions $permissions,
        FollowService $followService
    ) {
        $this->nodeRepository = $nodeRepository;
        $this->edgeRepository = $edgeRepository;
        $this->idEncoder = $hashIdService;
        $this->likeService = $likeService;
        $this->blockListService = $blockListService;
        $this->mediaService = $mediaService;
        $this->replyService = $replyService;
        $this->permissions = $permissions;
        $this->followService = $followService;
    }

    public function list(ListTopicCommentsRequest $request, string $viewerHash)
    {
        $topicId = Id64Bit::decode($this->idEncoder->decode($request->getTopic()));
        $viewerId = Id64Bit::decode($this->idEncoder->decode($viewerHash));

        $commentsEdges = $this->edgeRepository->find(
            $topicId,
            Edge::TYPE_HOLDS,
            Comment::NODE_TYPE,
            $request->getPage(),
            $request->getLimit()
        );

        $commentCount = $this->edgeRepository->count($topicId, Edge::TYPE_HOLDS, Comment::NODE_TYPE);

        $commentIds = [];

        /** @var Edge $edge */
        foreach ($commentsEdges as $edge) {
            $commentIds[] = Id64Bit::decode($edge->getInverseNode());
        }

        $comments = $this->nodeRepository->find($commentIds);

        $profileEdges = $this->edgeRepository->findBatch(
            $topicId->getShard(),
            array_keys($comments),
            Edge::TYPE_CREATED_BY,
            Profile::NODE_TYPE
        );

        $posterIds = [];

        foreach ($profileEdges as $edge) {
            $posterIds[] = Id64Bit::decode($edge->getInverseNode());
        }

        $profiles = $this->nodeRepository->find($posterIds);

        $results = [];

        $blockList = $this->blockListService->fetchFor($viewerId->getGlobal());

        /** @var Edge $edge */
        foreach ($profileEdges as $edge) {
            if ($blockList->has($edge->getInverseNode())) {
                unset($comments[$edge->getOwnerNode()]);
                continue;
            }

            $profileId = Id64Bit::decode($edge->getInverseNode());
            $commentId = Id64Bit::decode($edge->getOwnerNode());

            /** @var Profile $profile */
            $profile = $profiles[$profileId->getGlobal()];
            /** @var Comment $comment */
            $comment = $comments[$commentId->getGlobal()];



            $results[] = new CommentViewModel(
                $this->idEncoder->encode($commentId->getGlobal()),
                $comment,
                $this->mediaService->get($commentId),
                new MinimalProfileViewModel($this->idEncoder->encode($profileId->getGlobal()), $profile),
                $this->likeService->countFor($commentId, $viewerId),
                $this->replyService->countFor($commentId, $viewerId),
                $this->permissions->list($viewerId, $commentId)
            );
        }

        usort($results, function(CommentViewModel $a, CommentViewModel $b){
            if ($a->get('timestamp') === $b->get('timestamp')) {
                return 0;
            }

            return $a->get('timestamp') < $b->get('timestamp') ? -1 : 1;
        });

        return new CommentListViewModel($commentCount, $results);
    }

    public function view(ViewTopicCommentRequest $request, string $viewerHash)
    {
        $commentId = Id64Bit::decode($this->idEncoder->decode($request->getComment()));
        $viewerId = Id64Bit::decode($this->idEncoder->decode($viewerHash));

        $comment = $this->nodeRepository->findOne($commentId);
        $ownerId = Id64Bit::decode($this->edgeRepository->findCreatorId($commentId));
        $owner = $this->nodeRepository->findOne($ownerId);

        return new CommentViewModel(
            $this->idEncoder->encode($commentId->getGlobal()),
            $comment,
            $this->mediaService->get($commentId),
            new MinimalProfileViewModel($this->idEncoder->encode($ownerId->getGlobal()), $owner),
            $this->likeService->countFor($commentId, $viewerId),
            $this->replyService->countFor($commentId, $viewerId),
            $this->permissions->list($viewerId, $commentId)
        );
    }

    public function create(CreateTopicCommentRequest $request, string $profileHash)
    {
        $profileId = Id64Bit::decode($this->idEncoder->decode($profileHash));
        $topicId = Id64Bit::decode($this->idEncoder->decode($request->getTopic()));

        /** @var Topic $topic */
        $topic = $this->nodeRepository->findOne($topicId);

        if ($topic->isLocked()) {
            throw new AccessDeniedException();
        }


        $comment = (new Comment())->setText($request->getText());
        $commentId = $this->nodeRepository->addToShard($comment, $topicId->getShard());

        try {
            $this->edgeRepository->add($profileId, Edge::TYPE_CREATED, $commentId);
            $this->edgeRepository->add($topicId, Edge::TYPE_HOLDS, $commentId);

            $this->followService->follow($topicId, $profileId);
            $this->followService->follow($commentId, $profileId);

            if (!empty($request->getMedia())) {
                $this->mediaService->add($request->getMedia(), $commentId, $profileId);
            }
        }  catch (DBALException $ex) {
            throw new ProcessRequestException();
        }
    }

    public function update(EditTopicCommentRequest $request, string $profileHash)
    {
        $profileId = Id64Bit::decode($this->idEncoder->decode($profileHash));
        $commentId = Id64Bit::decode($this->idEncoder->decode($request->getComment()));
        $topicId = Id64Bit::decode($this->idEncoder->decode($request->getTopic()));

        /** @var Topic $topic */
        $topic = $this->nodeRepository->findOne($topicId);

        if ($topic->isLocked()) {
            throw new AccessDeniedException();
        }

        $comment = (new Comment())->setText($request->getText());
        $this->nodeRepository->update($commentId, $comment);

        if (!empty($request->getMedia())) {
            $this->mediaService->add($request->getMedia(), $commentId, $profileId);
        }
    }

    public function delete(DeleteCommentRequest $request, string $profileHash)
    {
        $profileId = Id64Bit::decode($this->idEncoder->decode($profileHash));
        $commentId = Id64Bit::decode($this->idEncoder->decode($request->getComment()));
        $topicId = Id64Bit::decode($this->idEncoder->decode($request->getTopic()));

        /** @var Topic $topic */
        $topic = $this->nodeRepository->findOne($topicId);

        if ($topic->isLocked()) {
            throw new AccessDeniedException();
        }

        try {
            $this->edgeRepository->drop($profileId, Edge::TYPE_CREATED, $commentId);
            $this->edgeRepository->drop($topicId, Edge::TYPE_HOLDS, $commentId);
            $this->nodeRepository->drop($commentId);
        } catch (\Exception $ex) {
            throw new ProcessRequestException();
        }
    }

    public function follow(ViewTopicCommentRequest $request, string $viewer)
    {
        $commentId = Id64Bit::decode($this->idEncoder->decode($request->getComment()));
        $viewerId = Id64Bit::decode($this->idEncoder->decode($viewer));
        $this->followService->follow($commentId, $viewerId);

        return $this->permissions->list($viewerId, $commentId);
    }

    public function unfollow(ViewTopicCommentRequest $request, string $viewer)
    {
        $commentId = Id64Bit::decode($this->idEncoder->decode($request->getComment()));
        $viewerId = Id64Bit::decode($this->idEncoder->decode($viewer));
        $this->followService->unfollow($request->getComment(), $viewer);

        return $this->permissions->list($viewerId, $commentId);
    }

    public function countFor(Id64Bit $nodeId, Id64Bit $viewerId): int
    {
        $blockList = $this->blockListService->fetchFor($viewerId->getGlobal());
        $commentIds = $this->edgeRepository->findInverseNodeIds($nodeId, Edge::TYPE_HOLDS, Comment::NODE_TYPE);
        $posterEdges = $this->edgeRepository->findBatch($nodeId->getShard(), $commentIds, Edge::TYPE_CREATED_BY, Profile::NODE_TYPE);

        $count = 0;

        /** @var Edge $edge */
        foreach ($posterEdges as $edge) {
            if (!$blockList->has($edge->getInverseNode())) {
                $count++;
            }
        }

        return $count;
    }
}