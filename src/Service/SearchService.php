<?php
namespace App\Service;

use App\Helper\Id64Bit;
use App\Node\Node;
use App\Node\Post;
use App\Node\Profile;
use App\Node\Topic;
use App\Repository\NodeRepository;
use App\Repository\SearchRepository;
use App\Request\SearchRequest;
use App\ViewModel\MinimalProfileViewModel;
use App\ViewModel\PostSearchResultViewModel;
use App\ViewModel\TopicSearchResultViewModel;

class SearchService
{
    private $searchRepository;
    private $nodeRepository;
    private $hashIdService;
    private $blockListService;

    public function __construct(
        SearchRepository $searchRepository,
        NodeRepository $nodeRepository,
        HashIdService $hashIdService,
        BlockListService $blockListService
    ) {
        $this->searchRepository = $searchRepository;
        $this->nodeRepository = $nodeRepository;
        $this->hashIdService = $hashIdService;
        $this->blockListService = $blockListService;
    }

    public function search(SearchRequest $request, string $viewerHash)
    {
        $blockList = $this->blockListService->fetchFor(
            $this->hashIdService->decode($viewerHash)
        );

        $ids = $this->searchRepository->search($request->getPhrase(), $request->getType(), $blockList->getBlockedIds());

        if (!is_array($ids) && is_numeric($ids)) {
            $ids = [$ids];
        }

        $nodeIds = $found = $results = [];

        foreach ($ids as $id) {
            $nodeIds[] = Id64Bit::decode($id);
        }

        $nodes = $this->nodeRepository->find($nodeIds);

        /**
        * @var int $id
        * @var Node $node
         */
        foreach ($nodes as $id => $node) {
            $encodedId = $this->hashIdService->encode($id);
            $type = $node->getType();
            switch ($type) {
                case Profile::NODE_TYPE:
                    $results[$type][] = new MinimalProfileViewModel($encodedId, $node);
                    break;
                case Topic::NODE_TYPE:
                    $results[$type][] = new TopicSearchResultViewModel($encodedId, $node);
                    break;
                case Post::NODE_TYPE:
                    $results[$type][] = new PostSearchResultViewModel($encodedId, $node);
                    break;
            }
        }

        return $results;
    }
}