<?php
namespace App\Service;

use App\Edge\Edge;
use App\Exception\NotFoundException;
use App\Exception\ProcessRequestException;
use App\Helper\Gender;
use App\Helper\Id64Bit;
use App\Node\Car;
use App\Node\Club;
use App\Node\Profile;
use App\Repository\EdgeRepository;
use App\Repository\NodeRepository;
use App\Repository\SearchRepository;
use App\Request\ChangeAvatarRequest;
use App\Request\CreateProfileRequest;
use App\Request\NodeRelatedRequest;
use App\Request\UpdateProfileRequest;
use App\Request\Vehicle\DeleteMediaRequest;
use App\Service\Permissions\ClubPermissions;
use App\ViewModel\FullProfileViewModel;
use App\ViewModel\HashViewModel;
use App\ViewModel\LikeCountViewModel;
use App\ViewModel\MinimalClubViewModel;
use App\ViewModel\MinimalProfileViewModel;
use Doctrine\DBAL\DBALException;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;

class ProfileService
{
    private $nodeRepository;
    private $edgeRepository;
    private $idEncoder;
    private $shardManager;
    private $likeService;
    private $searchRepository;
    private $blockListService;
    private $relationshipService;
    private $followService;
    private $mediaService;
    private $clubPermissions;

    public function __construct(
        HashIdService $hashIdService,
        ShardManager $shardManager,
        NodeRepository $nodeRepository,
        EdgeRepository $edgeRepository,
        LikeService $likeService,
        SearchRepository $searchRepository,
        BlockListService $blockListService,
        NodesRelationDescriber $relationshipService,
        FollowService $followService,
        MediaService $mediaService,
        ClubPermissions $clubPermissions
    ) {
        $this->nodeRepository = $nodeRepository;
        $this->edgeRepository = $edgeRepository;
        $this->idEncoder = $hashIdService;
        $this->shardManager = $shardManager;
        $this->likeService = $likeService;
        $this->searchRepository = $searchRepository;
        $this->blockListService = $blockListService;
        $this->relationshipService = $relationshipService;
        $this->followService = $followService;
        $this->mediaService = $mediaService;
        $this->clubPermissions = $clubPermissions;
    }

    public function create(CreateProfileRequest $request)
    {
        $shard = $this->shardManager->getStorageShard($request->getUserId());

        $profile = (new Profile())
            ->setFirstName($request->getFirstName())
            ->setLastName($request->getLastName())
            ->setGender($request->getGender())
            ->setBirthday($request->getBirthday());

        $profileId = $this->nodeRepository->addToShard($profile, $shard);
        $this->searchRepository->index($profileId, $profile, $profileId->getGlobal());

        return new HashViewModel($this->idEncoder->encode($profileId->getGlobal()));
    }

    public function view(NodeRelatedRequest $request, string $viewerHash): FullProfileViewModel
    {
        $profileId = Id64Bit::decode($this->idEncoder->decode($request->getNodeHash()));
        $viewerId = $this->idEncoder->decode($viewerHash);

        if ($profileId->getType() !== Profile::NODE_TYPE) {
            throw new NotFoundException();
        }

        $relationContext = $this->relationshipService->describe(Id64Bit::decode($viewerId), $profileId);

        /** @var Profile $profileNode */
        $profileNode = $this->nodeRepository->findOne($profileId);

        $carsCount = $this->edgeRepository->count($profileId, Edge::TYPE_CREATED, Car::NODE_TYPE);
        $follows = $this->edgeRepository->find($profileId, Edge::TYPE_FOLLOWED, Profile::NODE_TYPE);
        $followers = $this->edgeRepository->find($profileId, Edge::TYPE_FOLLOWED_BY, Profile::NODE_TYPE);
        $followsCount = $followersCount = 0;

        $blockList = $this->blockListService->fetchFor($viewerId);

        /** @var Edge $edge */
        foreach ($follows as $edge) {
            if (!$blockList->has($edge->getInverseNode())) {
                ++$followsCount;
            }
        }

        /** @var Edge $edge */
        foreach ($followers as $edge) {
            if (!$blockList->has($edge->getInverseNode())) {
                ++$followersCount;
            }
        }

        $membershipsCount = $this->edgeRepository->count($profileId, Edge::TYPE_FOLLOWED, Club::NODE_TYPE);

        return new FullProfileViewModel(
            $request->getNodeHash(),
            $profileNode,
            $carsCount,
            $followersCount,
            $followsCount,
            $membershipsCount,
            $relationContext
        );
    }

    public function update(UpdateProfileRequest $request, string $profileHash)
    {
        $profileId = Id64Bit::decode($this->idEncoder->decode($profileHash));

        $profile = $this->populateFromRequest($request, new Profile());
        $this->nodeRepository->update($profileId, $profile);

        $this->searchRepository->updateIndex($profileId, $profile);
    }

    public function changeAvatar(ChangeAvatarRequest $request, string $profileHash)
    {
        $profileId = Id64Bit::decode($this->idEncoder->decode($profileHash));
        $profile = $this->nodeRepository->findOne($profileId);
        $profile->setAvatar($request->getAvatar());

        $this->nodeRepository->update($profileId, $profile);
        $this->searchRepository->updateIndex($profileId, $profile);
    }

    public function follow(NodeRelatedRequest $request, string $profileHash)
    {
        $profile = Id64Bit::decode($this->idEncoder->decode($request->getNodeHash()));
        $viewer = Id64Bit::decode($this->idEncoder->decode($profileHash));

        $this->followService->follow($profile, $viewer);
    }

    public function unfollow(NodeRelatedRequest $request, string $profileHash)
    {
        $this->followService->unfollow($request->getNodeHash(), $profileHash);
    }

    public function listFollowers(NodeRelatedRequest $request, string $viewerHash)
    {
        $profileId = Id64Bit::decode($this->idEncoder->decode($request->getNodeHash()));
        $viewerId = $this->idEncoder->decode($viewerHash);

        $blockList = $this->blockListService->fetchFor($viewerId);
        $followedByEdges = $this->edgeRepository->find($profileId, Edge::TYPE_FOLLOWED_BY, Profile::NODE_TYPE);
        $followersIds = $followers = [];

        /** @var Edge $edge */
        foreach ($followedByEdges as $edge) {
            if ($blockList->has($edge->getInverseNode())) {
                continue;
            }

            $followersIds[] = Id64Bit::decode($edge->getInverseNode());
        }

        $followerNodes = $this->nodeRepository->find($followersIds);

        foreach ($followerNodes as $id => $node) {
            $encodedId = $this->idEncoder->encode($id);
            $followers[] = new MinimalProfileViewModel($encodedId, $node);
        }

        return $followers;
    }

    public function listFollowing(NodeRelatedRequest $request, string $viewerHash)
    {
        $profileId = Id64Bit::decode($this->idEncoder->decode($request->getNodeHash()));
        $viewerId = $this->idEncoder->decode($viewerHash);

        $blockList = $this->blockListService->fetchFor($viewerId);
        $edges = $this->edgeRepository->find($profileId, Edge::TYPE_FOLLOWED, Profile::NODE_TYPE);
        $followingIds = $following = [];

        /** @var Edge $edge */
        foreach ($edges as $edge) {
            if ($blockList->has($edge->getInverseNode())) {
                continue;
            }

            $followingIds[] = Id64Bit::decode($edge->getInverseNode());
        }

        $followingNodes = $this->nodeRepository->find($followingIds);

        foreach ($followingNodes as $id => $node) {
            $encodedId = $this->idEncoder->encode($id);
            $following[] = new MinimalProfileViewModel($encodedId, $node);
        }

        return $following;
    }

    public function block(NodeRelatedRequest $request, string $blockerHash)
    {
        $blocker = Id64Bit::decode($this->idEncoder->decode($blockerHash));
        $blocked = Id64Bit::decode($this->idEncoder->decode($request->getNodeHash()));

        if (
            $blocker->getType() !== Profile::NODE_TYPE ||
            $blocked->getType() !== Profile::NODE_TYPE ||
            $blocker->getGlobal() === $blocked->getGlobal()
        ) {
            throw new NotFoundException();
        }

        $this->edgeRepository->add($blocker, Edge::TYPE_BLOCKED, $blocked);
    }

    public function unblock(NodeRelatedRequest $request, string $blockerHash)
    {
        $blocker = Id64Bit::decode($this->idEncoder->decode($blockerHash));
        $blocked = Id64Bit::decode($this->idEncoder->decode($request->getNodeHash()));

        $this->edgeRepository->drop($blocker, Edge::TYPE_BLOCKED, $blocked);
    }

    public function listBlocked(string $profileHash)
    {
        $blocker = Id64Bit::decode($this->idEncoder->decode($profileHash));

        if ($blocker->getType() !== Profile::NODE_TYPE) {
            throw new NotFoundException();
        }

        $edges = $this->edgeRepository->find($blocker, Edge::TYPE_BLOCKED, Profile::NODE_TYPE);

        $profileIds = $profiles = [];

        /** @var Edge $edge */
        foreach ($edges as $edge) {
            $profileIds[] = Id64Bit::decode($edge->getInverseNode());
        }

        $profileNodes = $this->nodeRepository->find($profileIds);

        foreach ($profileNodes as $id => $node) {
            $encodedId = $this->idEncoder->encode($id);
            $profiles[] = new MinimalProfileViewModel($encodedId, $node);
        }

        return $profiles;
    }

    public function listClubMemberships(NodeRelatedRequest $request, string $viewerHash)
    {
        $profileId = Id64Bit::decode($this->idEncoder->decode($request->getNodeHash()));
        $viewerId = Id64Bit::decode($this->idEncoder->decode($viewerHash));

        $blockList = $this->blockListService->fetchFor($viewerId->getGlobal());
        $edges = $this->edgeRepository->find($profileId, Edge::TYPE_FOLLOWED, Club::NODE_TYPE);
        $clubs = $clubIds = [];

        /** @var Edge $edge */
        foreach ($edges as $edge) {
            $clubIds[] = Id64Bit::decode($edge->getInverseNode());
        }

        $clubNodes = $this->nodeRepository->find($clubIds);

        foreach ($clubNodes as $id => $node) {
            $id64Bit = Id64Bit::decode($id);
            $encodedId = $this->idEncoder->encode($id);
            $memberCount = $this->edgeRepository->count($id64Bit, Edge::TYPE_FOLLOWED_BY, Profile::NODE_TYPE, $blockList->getBlockedIds());
            $clubs[] = new MinimalClubViewModel($encodedId, $node, $memberCount, $this->clubPermissions->list($viewerId, $id64Bit));
        }

        return $clubs;
    }

    public function like(NodeRelatedRequest $request, string $profileHash)
    {
        $profileId = Id64Bit::decode($this->idEncoder->decode($profileHash));
        $nodeId = Id64Bit::decode($this->idEncoder->decode($request->getNodeHash()));

        $count = $this->likeService->like($profileId, $nodeId);
        return new LikeCountViewModel($count);
    }

    public function unlike(NodeRelatedRequest $request, string $profileHash)
    {
        $profileId = Id64Bit::decode($this->idEncoder->decode($profileHash));
        $nodeId = Id64Bit::decode($this->idEncoder->decode($request->getNodeHash()));

        $count = $this->likeService->unlike($profileId, $nodeId);

        return new LikeCountViewModel($count);
    }

    public function listLikes(NodeRelatedRequest $request, string $viewerHash)
    {
        $nodeId = Id64Bit::decode($this->idEncoder->decode($request->getNodeHash()));
        $viewerId = Id64Bit::decode($this->idEncoder->decode($viewerHash));

        return $this->likeService->list($nodeId, $viewerId);
    }

    private function populateFromRequest(UpdateProfileRequest $request, Profile $profile): Profile
    {
        if (null !== $request->getFirstName()) {
            $profile->setFirstName($request->getFirstName());
        }

        if (null !== $request->getLastName()) {
            $profile->setLastName($request->getLastName());
        }

        if ($request->getBirthday() instanceof \DateTime) {
            $profile->setBirthday($request->getBirthday());
        }

        if ($request->getGender() instanceof Gender) {
            $profile->setGender($request->getGender());
        }

        if (null !== $request->getLocality()) {
            $profile->setLocality($request->getLocality());
        }

        return $profile;
    }
}