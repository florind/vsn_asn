<?php
namespace App\Service;

use App\Edge\Edge;
use App\Exception\NotFoundException;
use App\Exception\ProcessRequestException;
use App\Helper\Id64Bit;
use App\Node\Car;
use App\Repository\EdgeRepository;
use App\Repository\NodeRepository;
use App\Request\NodeRelatedRequest;
use App\Request\Vehicle\AddMediaRequest;
use App\Request\Vehicle\CarDataInterface;
use App\Request\Vehicle\CreateCarRequest;
use App\Request\Vehicle\DeleteMediaRequest;
use App\Request\Vehicle\UpdateCarRequest;
use App\ViewModel\CarViewModel;

class CarService
{
    private $nodeRepository;
    private $edgeRepository;
    private $idEncoder;
    private $likeService;
    private $commentService;
    private $nodesRelationDescriber;
    private $mediaService;

    public function __construct(
        HashIdService $hashIdService,
        NodeRepository $nodeRepository,
        EdgeRepository $edgeRepository,
        LikeService $likeService,
        CommentService $commentService,
        NodesRelationDescriber $nodesRelationDescriber,
        MediaService $mediaService
    ) {
        $this->idEncoder = $hashIdService;
        $this->nodeRepository = $nodeRepository;
        $this->edgeRepository = $edgeRepository;
        $this->likeService = $likeService;
        $this->commentService = $commentService;
        $this->nodesRelationDescriber = $nodesRelationDescriber;
        $this->mediaService = $mediaService;
    }

    public function create(CreateCarRequest $model, string $profileHash)
    {
        $profileId = Id64Bit::decode($this->idEncoder->decode($profileHash));

        $car = $this->populateCar(new Car(), $model);

        try {
            $carId = $this->nodeRepository->addToShard($car, $profileId->getShard());
            $this->edgeRepository->add($profileId, Edge::TYPE_CREATED, $carId);

            if (!empty($model->getMedia())) {
                $this->mediaService->add($model->getMedia(), $carId, $profileId);
            }
        } catch (\Exception $ex) {
            throw new ProcessRequestException();
        }
    }

    public function update(UpdateCarRequest $model, string $profileHash)
    {
        $profileId = Id64Bit::decode($this->idEncoder->decode($profileHash));
        $carId = Id64Bit::decode($this->idEncoder->decode($model->getCarHash()));

        $car = $this->populateCar(new Car(), $model);
        $this->nodeRepository->update($carId, $car);

        if (!empty($model->getMedia())) {
            $this->mediaService->add($model->getMedia(), $carId, $profileId);
        }
    }

    public function delete(NodeRelatedRequest $model, string $profileHash)
    {
        $profileId = Id64Bit::decode($this->idEncoder->decode($profileHash));
        $carId = Id64Bit::decode($this->idEncoder->decode($model->getNodeHash()));

        try {
            $this->nodeRepository->drop($carId);
            $this->edgeRepository->drop($profileId, Edge::TYPE_CREATED, $carId);
        } catch (\Exception $exception) {
            throw new ProcessRequestException();
        }
    }

    public function view(NodeRelatedRequest $model, string $viewerHash): CarViewModel
    {
        $carId = Id64Bit::decode($this->idEncoder->decode($model->getNodeHash()));
        $viewerId = Id64Bit::decode($this->idEncoder->decode($viewerHash));

        if ($carId->getType() !== Car::NODE_TYPE) {
            throw new NotFoundException();
        }

        /** @var Car $car */
        $car = $this->nodeRepository->findOne($carId);
        $relationContext = $this->nodesRelationDescriber->describe($viewerId, $carId);

        return new CarViewModel(
            $model->getNodeHash(),
            $car,
            $this->mediaService->get($carId),
            $this->likeService->countFor($carId, $viewerId),
            $this->commentService->countFor($carId, $viewerId),
            $relationContext
        );
    }

    public function list(NodeRelatedRequest $model, string $viewerHash)
    {
        $profileId = Id64Bit::decode($this->idEncoder->decode($model->getNodeHash()));
        $viewerId = Id64Bit::decode($this->idEncoder->decode($viewerHash));

        $carEdges = $this->edgeRepository->find($profileId, Edge::TYPE_CREATED, Car::NODE_TYPE);

        if (empty($carEdges)) {
            return [];
        }

        $carIds = $cars = [];

        /** @var Edge $edge */
        foreach ($carEdges as $edge) {
            $carIds[] = Id64Bit::decode($edge->getInverseNode());
        }

        $carNodes = $this->nodeRepository->find($carIds);

        /** @var Car $car */
        foreach ($carNodes as $id => $car) {
            $carId = Id64Bit::decode($id);
            $encodedId = $this->idEncoder->encode($carId->getGlobal());

            $relationContext = $this->nodesRelationDescriber->describe($viewerId, $carId);

            $cars[] = new CarViewModel(
                $encodedId,
                $car,
                $this->mediaService->get($carId),
                $this->likeService->countFor($carId, $viewerId),
                $this->commentService->countFor($carId, $viewerId),
                $relationContext
            );
        }

        return $cars;
    }

    private function populateCar(Car $car, CarDataInterface $data): Car
    {
        if (null !== $data->getModel()) {
            $car->setModel($data->getModel());
        }

        if (null !== $data->getBrand()) {
            $car->setBrand($data->getBrand());
        }

        if (null !== $data->getYear()) {
            $car->setYear($data->getYear());
        }

        if (null !== $data->getFuel()) {
            $car->setFuel($data->getFuel());
        }

        if (null !== $data->getEngineCapacity()) {
            $car->setEngineCapacity($data->getEngineCapacity());
        }

        if (null !== $data->getHorsepower()) {
            $car->setHorsepower($data->getHorsepower());
        }

        if (null !== $data->getTransmission()) {
            $car->setTransmission($data->getTransmission());
        }

        if (null !== $data->getBody()) {
            $car->setBodyType($data->getBody());
        }

        if (null !== $data->getMileage()) {
            $car->setMileage($data->getMileage());
        }

        if (null !== $data->getVin()) {
            $car->setVin($data->getVin());
        }

        if (
            null !== $data->getFeatures() &&
            is_array($data->getFeatures())
        ) {
            foreach ($data->getFeatures() as $feature) {
                $car->addFeature($feature);
            }
        }

        return $car;
    }
}