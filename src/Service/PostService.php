<?php
namespace App\Service;

use App\Edge\Edge;
use App\Exception\NotFoundException;
use App\Exception\ProcessRequestException;
use App\Helper\Id64Bit;
use App\Node\Post;
use App\Node\Profile;
use App\Repository\EdgeRepository;
use App\Repository\NodeRepository;
use App\Repository\SearchRepository;
use App\Request\NodeRelatedRequest;
use App\Request\Post\CreatePostRequest;
use App\Request\Post\ListPostsRequest;
use App\Request\Post\UpdatePostRequest;
use App\ViewModel\HashViewModel;
use App\ViewModel\MinimalProfileViewModel;
use App\ViewModel\PostViewModel;

class PostService
{
    private $nodeRepository;
    private $edgeRepository;
    private $idEncoder;
    private $likeService;
    private $commentService;
    private $searchRepository;
    private $mediaService;
    private $relationDescriber;

    public function __construct(
        HashIdService $hashIdService,
        NodeRepository $nodeRepository,
        EdgeRepository $edgeRepository,
        LikeService $likeService,
        CommentService $commentService,
        SearchRepository $searchRepository,
        MediaService $mediaService,
        NodesRelationDescriber $relationDescriber
    ) {
        $this->nodeRepository = $nodeRepository;
        $this->edgeRepository = $edgeRepository;
        $this->idEncoder = $hashIdService;
        $this->likeService = $likeService;
        $this->commentService = $commentService;
        $this->searchRepository = $searchRepository;
        $this->mediaService = $mediaService;
        $this->relationDescriber = $relationDescriber;
    }

    public function view(NodeRelatedRequest $model, string $viewerHash): PostViewModel
    {
        $postId = Id64Bit::decode($this->idEncoder->decode($model->getNodeHash()));
        $viewerId = Id64Bit::decode($this->idEncoder->decode($viewerHash));

        if ($postId->getType() !== Post::NODE_TYPE) {
            throw new NotFoundException();
        }

        /** @var Post $postNode */
        $postNode = $this->nodeRepository->findOne($postId);
        $ownerId = $this->edgeRepository->findCreatorId($postId);
        $ownerId = Id64Bit::decode($ownerId);
        $owner = $this->nodeRepository->findOne($ownerId);

        return new PostViewModel(
            $model->getNodeHash(),
            $postNode,
            new MinimalProfileViewModel($this->idEncoder->encode($ownerId->getGlobal()), $owner),
            $this->likeService->countFor($postId, $viewerId),
            $this->commentService->countFor($postId, $viewerId),
            $this->mediaService->get($postId),
            $this->relationDescriber->describe($viewerId, $postId)
        );
    }

    public function list(ListPostsRequest $request, string $viewerHash)
    {
        $profileId = Id64Bit::decode($this->idEncoder->decode($request->getProfileHash()));
        $viewerId = Id64Bit::decode($this->idEncoder->decode($viewerHash));

        /** @var Profile $profile */
        $profile = $this->nodeRepository->findOne($profileId);
        $minimalProfileView = new MinimalProfileViewModel($request->getProfileHash(), $profile);

        $edges = $this->edgeRepository->find($profileId, Edge::TYPE_CREATED, Post::NODE_TYPE);

        if (empty($edges)) {
            return [];
        }

        $postIds = [];

        /** @var Edge $edge */
        foreach ($edges as $edge) {
            $postIds[] = Id64Bit::decode($edge->getInverseNode());
        }

        $nodes = $this->nodeRepository->find($postIds);

        $posts = [];

        /** @var Post $node */
        foreach ($nodes as $id => $node) {
            $id = Id64Bit::decode($id);
            $encodedId = $this->idEncoder->encode($id->getGlobal());

            $posts[] = new PostViewModel(
                $encodedId,
                $node,
                $minimalProfileView,
                $this->likeService->countFor($id, $viewerId),
                $this->commentService->countFor($id, $viewerId),
                $this->mediaService->get($id),
                $this->relationDescriber->describe($viewerId, $id)
            );
        }

        return $posts;
    }

    public function create(CreatePostRequest $request, string $profileHash)
    {
        $profileId = Id64Bit::decode($this->idEncoder->decode($profileHash));
        $post = (new Post())->setText($request->getText());

        try {
            $postId = $this->nodeRepository->addToShard($post, $profileId->getShard());
            $this->edgeRepository->add($profileId, Edge::TYPE_CREATED, $postId);

            $this->searchRepository->index($postId, $post, $profileId->getGlobal());

            if (!empty($request->getMedia())) {
                $this->mediaService->add($request->getMedia(), $postId, $profileId);
            }

            return new HashViewModel($this->idEncoder->encode($postId->getGlobal()));
        } catch (\Exception $ex) {
            throw new ProcessRequestException();
        }
    }

    public function update(UpdatePostRequest $request, string $profileHash)
    {
        $postId = Id64Bit::decode($this->idEncoder->decode($request->getPostHash()));
        $profileId = Id64Bit::decode($this->idEncoder->decode($profileHash));

        $post = (new Post)->setText($request->getText());
        $this->nodeRepository->update($postId, $post);

        if (!empty($request->getMedia())) {
            $this->mediaService->add($request->getMedia(), $postId, $profileId);
        }

        $this->searchRepository->updateIndex($postId, $post);
    }

    public function delete(NodeRelatedRequest $request, string $profileHash)
    {
        $profileId = Id64Bit::decode($this->idEncoder->decode($profileHash));
        $postId = Id64Bit::decode($this->idEncoder->decode($request->getNodeHash()));

        try {
            $this->edgeRepository->drop($profileId, Edge::TYPE_CREATED, $postId);
            $this->nodeRepository->drop($postId);

            $this->searchRepository->dropIndex($postId);
        } catch (\Exception $exception) {
            throw new ProcessRequestException();
        }
    }
}