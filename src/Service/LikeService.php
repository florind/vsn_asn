<?php
namespace App\Service;

use App\Edge\Edge;
use App\Exception\NotFoundException;
use App\Exception\ProcessRequestException;
use App\Helper\Id64Bit;
use App\Node\Profile;
use App\Repository\EdgeRepository;
use App\Repository\NodeRepository;
use App\Service\Permissions\ProfilePermissions;
use App\ViewModel\MinimalProfileViewModel;
use Doctrine\DBAL\DBALException;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;

class LikeService
{
    private $nodeRepository;
    private $edgeRepository;
    private $idEncoder;
    private $likeableNodeTypes = [];
    private $blockListService;
    private $profilePermissions;

    public function __construct(
        HashIdService $hashIdService,
        NodeRepository $nodeRepository,
        EdgeRepository $edgeRepository,
        ProfilePermissions $profilePermissions,
        BlockListService $blockListService,
        array $likeConfig
    ) {
        $this->nodeRepository = $nodeRepository;
        $this->edgeRepository = $edgeRepository;
        $this->idEncoder = $hashIdService;
        $this->blockListService = $blockListService;
        $this->profilePermissions = $profilePermissions;

        foreach ($likeConfig as $entry) {
            $this->likeableNodeTypes[] = $entry::NODE_TYPE;
        }
    }

    public function like(Id64Bit $profileId, Id64Bit $nodeId)
    {
        if (!$this->canLike($nodeId->getType())) {
            throw new NotFoundException();
        }

        try {
            $this->edgeRepository->add($profileId, Edge::TYPE_LIKED, $nodeId);
            return $this->countFor($nodeId, $profileId);
        } catch (UniqueConstraintViolationException $ex) {
            throw new ProcessRequestException();
        }
    }

    public function unlike(Id64Bit $profileId, Id64Bit $nodeId)
    {
        if (!$this->canLike($nodeId->getType())) {
            throw new NotFoundException();
        }

        try {
            $this->edgeRepository->drop($profileId, Edge::TYPE_LIKED, $nodeId);
            return $this->countFor($nodeId, $profileId);
        } catch (DBALException $ex) {
            throw new ProcessRequestException();
        }
    }

    public function countFor(Id64Bit $nodeId, Id64Bit $viewerId)
    {
        $blockList = $this->blockListService->fetchFor($viewerId->getGlobal());
        return (int)$this->edgeRepository->count($nodeId, Edge::TYPE_LIKED_BY, Profile::NODE_TYPE, $blockList->getBlockedIds());
    }

    public function list(Id64Bit $nodeId, Id64Bit $viewerId)
    {
        $blockList = $this->blockListService->fetchFor($viewerId->getGlobal());
        $likes = $this->edgeRepository->find($nodeId, Edge::TYPE_LIKED_BY, Profile::NODE_TYPE);
        $ids = $profiles = $results = [];

        /** @var Edge $like */
        foreach ($likes as $like) {
            if ($blockList->has($like->getInverseNode())) {
                continue;
            }
            $ids[] = Id64Bit::decode($like->getInverseNode());
        }

        $profiles = $this->nodeRepository->find($ids);

        foreach ($profiles as $id => $profile) {
            $encodedId = $this->idEncoder->encode($id);
            $permissions = $this->profilePermissions->list($viewerId, Id64Bit::decode($id));
            $results[] = new MinimalProfileViewModel($encodedId, $profile, $permissions);
        }

        return $results;
    }

    private function canLike(int $type)
    {
        return in_array($type, $this->likeableNodeTypes);
    }
}