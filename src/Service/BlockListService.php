<?php
namespace App\Service;

use App\Edge\Edge;
use App\Helper\Id64Bit;
use App\Model\BlockList;
use App\Node\Profile;
use App\Repository\EdgeRepository;

class BlockListService
{
    const VIEW_PERMISSION = 'VIEW_NODE';
    const EDIT_PERMISSION = 'EDIT_NODE';

    private $edgeRepository;

    public function __construct(EdgeRepository $edgeRepository) {
        $this->edgeRepository = $edgeRepository;
    }

    public function fetchFor(int $profileGlobalId)
    {
        $profileId = Id64Bit::decode($profileGlobalId);
        $edges = $this->edgeRepository->find($profileId, [Edge::TYPE_BLOCKED, Edge::TYPE_BLOCKED_BY], Profile::NODE_TYPE);

        $blockedIds = [];

        /** @var Edge $edge */
        foreach ($edges as $edge) {
            $blockedIds[] = $edge->getInverseNode();
        }

        return new BlockList($blockedIds);
    }
}