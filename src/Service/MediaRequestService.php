<?php
namespace App\Service;

use App\Helper\Id64Bit;
use App\Request\NodeRelatedRequest;
use App\Request\Vehicle\AddMediaRequest;
use App\Request\Vehicle\DeleteMediaRequest;

class MediaRequestService
{
    private $encoder;
    private $mediaService;

    public function __construct(HashIdService $encoder, MediaService $mediaService)
    {
        $this->encoder = $encoder;
        $this->mediaService = $mediaService;
    }

    public function get(NodeRelatedRequest $model)
    {
        $nodeId = Id64Bit::decode($this->encoder->decode($model->getNodeHash()));
        return $this->mediaService->get($nodeId);
    }

    public function add(AddMediaRequest $model, string $viewerHash)
    {
        $media = $model->getMedia();
        $profileId = Id64Bit::decode($this->encoder->decode($viewerHash));
        $nodeId = Id64Bit::decode($this->encoder->decode($model->getNodeHash()));

        $this->mediaService->add($media, $nodeId, $profileId);
    }

    public function delete(DeleteMediaRequest $model, string $viewerHash)
    {
        $profileId = Id64Bit::decode($this->encoder->decode($viewerHash));
        $nodeId = Id64Bit::decode($this->encoder->decode($model->getNodeHash()));
        $mediaId = Id64Bit::decode($this->encoder->decode($model->getMediaHash()));

        $this->mediaService->delete($mediaId, $profileId, $nodeId);
    }
}