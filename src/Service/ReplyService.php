<?php
namespace App\Service;

use App\Edge\Edge;
use App\Exception\AccessDeniedException;
use App\Exception\ProcessRequestException;
use App\Helper\Id64Bit;
use App\Node\Comment;
use App\Node\Profile;
use App\Node\Reply;
use App\Node\Topic;
use App\Repository\EdgeRepository;
use App\Repository\NodeRepository;
use App\Request\Reply\CreateTopicReplyRequest;
use App\Request\Reply\DeleteReplyRequest;
use App\Request\Reply\EditTopicReplyRequest;
use App\Request\Reply\ListTopicRepliesRequest;
use App\Service\Permissions\ReplyPermissions;
use App\ViewModel\CommentViewModel;
use App\ViewModel\MinimalProfileViewModel;
use App\ViewModel\ReplyListViewModel;
use Doctrine\DBAL\DBALException;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;

class ReplyService
{
    private $nodeRepository;
    private $edgeRepository;
    private $idEncoder;
    private $likeService;
    private $blockListService;
    private $mediaService;
    private $permissions;
    private $followService;

    public function __construct(
        HashIdService $hashIdService,
        NodeRepository $nodeRepository,
        EdgeRepository $edgeRepository,
        LikeService $likeService,
        BlockListService $blockListService,
        MediaService $mediaService,
        ReplyPermissions $permissions,
        FollowService $followService
    ) {
        $this->nodeRepository = $nodeRepository;
        $this->edgeRepository = $edgeRepository;
        $this->idEncoder = $hashIdService;
        $this->likeService = $likeService;
        $this->blockListService = $blockListService;
        $this->mediaService = $mediaService;
        $this->permissions = $permissions;
        $this->followService = $followService;
    }

    public function list(ListTopicRepliesRequest $request, string $viewerHash)
    {
        $nodeId = Id64Bit::decode($this->idEncoder->decode($request->getTopic()));
        $commentId = Id64Bit::decode($this->idEncoder->decode($request->getComment()));
        $viewerId = Id64Bit::decode($this->idEncoder->decode($viewerHash));

        $replyEdges = $this->edgeRepository->find(
            $commentId,
            Edge::TYPE_HOLDS,
            Reply::NODE_TYPE,
            $request->getPage(),
            $request->getLimit()
        );

        $replyCount = $this->edgeRepository->count($commentId, Edge::TYPE_HOLDS, Reply::NODE_TYPE);

        $replyIds = [];

        /** @var Edge $edge */
        foreach ($replyEdges as $edge) {
            $replyIds[] = Id64Bit::decode($edge->getInverseNode());
        }

        $replies = $this->nodeRepository->find($replyIds);

        $profileEdges = $this->edgeRepository->findBatch(
            $nodeId->getShard(),
            array_keys($replies),
            Edge::TYPE_CREATED_BY,
            Profile::NODE_TYPE
        );

        $profileIds = [];

        foreach ($profileEdges as $edge) {
            $profileIds[] = Id64Bit::decode($edge->getInverseNode());
        }

        $profiles = $this->nodeRepository->find($profileIds);

        $results = [];

        $blockList = $this->blockListService->fetchFor($viewerId->getGlobal());

        /** @var Edge $edge */
        foreach ($profileEdges as $edge) {
            if ($blockList->has($edge->getInverseNode())) {
                unset($replies[$edge->getOwnerNode()]);
                continue;
            }

            $profileId = Id64Bit::decode($edge->getInverseNode());
            $replyId = Id64Bit::decode($edge->getOwnerNode());

            /** @var Profile $profile */
            $profile = $profiles[$profileId->getGlobal()];
            /** @var Comment $reply */
            $reply = $replies[$replyId->getGlobal()];

            $results[] = new CommentViewModel(
                $this->idEncoder->encode($replyId->getGlobal()),
                $reply,
                $this->mediaService->get($replyId),
                new MinimalProfileViewModel($this->idEncoder->encode($profileId->getGlobal()), $profile),
                $this->likeService->countFor($replyId, $viewerId),
                0,
                $this->permissions->list($viewerId, $replyId)
            );
        }

        usort($results, function(CommentViewModel $a, CommentViewModel $b){
            if ($a->get('timestamp') === $b->get('timestamp')) {
                return 0;
            }

            return $a->get('timestamp') < $b->get('timestamp') ? -1 : 1;
        });

        return new ReplyListViewModel($replyCount, $results);
    }

    public function get(string $replyHash, string $viewerHash)
    {
        $replyId = Id64Bit::decode($this->idEncoder->decode($replyHash));
        $viewerId = Id64Bit::decode($this->idEncoder->decode($viewerHash));

        $reply = $this->nodeRepository->findOne($replyId);
        $ownerId = Id64Bit::decode($this->edgeRepository->findCreatorId($replyId));
        $owner = $this->nodeRepository->findOne($ownerId);

        return new CommentViewModel(
            $this->idEncoder->encode($replyId->getGlobal()),
            $reply,
            $this->mediaService->get($replyId),
            new MinimalProfileViewModel($this->idEncoder->encode($ownerId->getGlobal()), $owner),
            $this->likeService->countFor($replyId, $viewerId),
            0,
            $this->permissions->list($viewerId, $replyId)
        );
    }

    public function create(CreateTopicReplyRequest $request, string $profileHash)
    {
        $profileId = Id64Bit::decode($this->idEncoder->decode($profileHash));
        $topicId = Id64Bit::decode($this->idEncoder->decode($request->getTopic()));
        $commentId = Id64Bit::decode($this->idEncoder->decode($request->getComment()));

        /** @var Topic $topic */
        $topic = $this->nodeRepository->findOne($topicId);

        if ($topic->isLocked()) {
            throw new AccessDeniedException();
        }

        $reply = (new Reply())->setText($request->getText());
        $replyId = $this->nodeRepository->addToShard($reply, $topicId->getShard());

        try {
            $this->edgeRepository->add($profileId, Edge::TYPE_CREATED, $replyId);
            $this->edgeRepository->add($commentId, Edge::TYPE_HOLDS, $replyId);
            $this->edgeRepository->add($topicId, Edge::TYPE_HOLDS, $replyId);

            $this->followService->follow($topicId, $profileId);
            $this->followService->follow($commentId, $profileId);

            if (!empty($request->getMedia())) {
                $this->mediaService->add($request->getMedia(), $replyId, $profileId);
            }
        }  catch (DBALException $ex) {
            throw new ProcessRequestException();
        }
    }

    public function update(EditTopicReplyRequest $request, string $profileHash)
    {
        $profileId = Id64Bit::decode($this->idEncoder->decode($profileHash));
        $topicId = Id64Bit::decode($this->idEncoder->decode($request->getTopic()));
        $replyId = Id64Bit::decode($this->idEncoder->decode($request->getReply()));

        /** @var Topic $topic */
        $topic = $this->nodeRepository->findOne($topicId);

        if ($topic->isLocked()) {
            throw new AccessDeniedException();
        }

        $reply = (new Reply())->setText($request->getText());
        $this->nodeRepository->update($replyId, $reply);

        if (!empty($request->getMedia())) {
            $this->mediaService->add($request->getMedia(), $replyId, $profileId);
        }
    }

    public function delete(DeleteReplyRequest $request, string $profileHash)
    {
        $profileId = Id64Bit::decode($this->idEncoder->decode($profileHash));
        $commentId = Id64Bit::decode($this->idEncoder->decode($request->getComment()));
        $topicId = Id64Bit::decode($this->idEncoder->decode($request->getTopic()));
        $replyId = Id64Bit::decode($this->idEncoder->decode($request->getReply()));

        /** @var Topic $topic */
        $topic = $this->nodeRepository->findOne($topicId);

        if ($topic->isLocked()) {
            throw new AccessDeniedException();
        }

        try {
            $this->edgeRepository->drop($profileId, Edge::TYPE_CREATED, $replyId);
            $this->edgeRepository->drop($topicId, Edge::TYPE_HOLDS, $replyId);
            $this->edgeRepository->drop($commentId, Edge::TYPE_HOLDS, $replyId);
            $this->nodeRepository->drop($replyId);
        } catch (\Exception $ex) {
            throw new ProcessRequestException();
        }
    }

    public function countFor(Id64Bit $nodeId, Id64Bit $viewerGlobalId): int
    {
        $blockList = $this->blockListService->fetchFor($viewerGlobalId->getGlobal());
        $replyIds = $this->edgeRepository->findInverseNodeIds($nodeId, Edge::TYPE_HOLDS, Reply::NODE_TYPE);
        $posterEdges = $this->edgeRepository->findBatch($nodeId->getShard(), $replyIds, Edge::TYPE_CREATED_BY, Profile::NODE_TYPE);

        $count = 0;

        /** @var Edge $edge */
        foreach ($posterEdges as $edge) {
            if (!$blockList->has($edge->getInverseNode())) {
                $count++;
            }
        }

        return $count;
    }
}