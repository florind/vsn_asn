<?php
namespace App\Service\AccessControl;

use App\Edge\Edge;
use App\Exception\AccessDeniedException;
use App\Helper\Id64Bit;
use App\Node\Club;
use App\Node\Profile;
use App\Repository\EdgeRepository;
use App\Service\HashIdService;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class ClubAccessControl
{
    private $hashId;
    private $edgeRepository;

    public function __construct(HashIdService $idEncoder, EdgeRepository $repository)
    {
        $this->hashId = $idEncoder;
        $this->edgeRepository = $repository;
    }

    public function approveBan(string $clubHash, string $viewerHash, string $memberHash)
    {
        $club = Id64Bit::decode($this->hashId->decode($clubHash));
        $viewer = Id64Bit::decode($this->hashId->decode($viewerHash));
        $member = Id64Bit::decode($this->hashId->decode($memberHash));

        try {
            $this->assertType($club, Club::class);
            $this->assertType($viewer, Profile::class);
            $this->assertType($member, Profile::class);
        } catch (\InvalidArgumentException $ex) {
            throw new BadRequestHttpException();
        }

        $viewerClubRelation = $this->edgeRepository->getRelationship($viewer, $club);

        if (
            empty($viewerClubRelation) ||
            !in_array(Edge::TYPE_MODERATES, $viewerClubRelation) ||
            in_array(Edge::TYPE_BLOCKED_BY, $viewerClubRelation)
        ) {
            throw new AccessDeniedException();
        }

        $memberClubRelation = $this->edgeRepository->getRelationship($member, $club);

        if (
            empty($memberClubRelation) ||
            in_array(Edge::TYPE_BLOCKED_BY, $memberClubRelation)
        ) {
            throw new AccessDeniedException();
        }

        if (in_array(Edge::TYPE_CREATED, $memberClubRelation)) {
            throw new AccessDeniedException();
        }

        if (
            in_array(Edge::TYPE_MODERATES, $memberClubRelation) &&
            !in_array(Edge::TYPE_CREATED, $viewerClubRelation)
        ) {
            throw new AccessDeniedException();
        }
    }

    private function assertType(Id64Bit $id, string $node)
    {
        if ($id->getType() !== $node::NODE_TYPE) {
            throw new \InvalidArgumentException();
        }
    }
}