<?php
namespace App\Service\AccessControl;

use App\Edge\Edge;
use App\Exception\AccessDeniedException;
use App\Exception\NotFoundException;
use App\Helper\Id64Bit;
use App\Node\Club;
use App\Node\Profile;
use App\Node\Topic;
use App\Repository\EdgeRepository;
use App\Service\HashIdService;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class TopicAccessControl
{
    private $hashId;
    private $edgeRepository;
    private $permissions;

    public function __construct(HashIdService $idEncoder, EdgeRepository $repository)
    {
        $this->hashId = $idEncoder;
        $this->edgeRepository = $repository;
    }

    public function approveCreate(string $clubHash, string $viewerHash)
    {
        $club = Id64Bit::decode($this->hashId->decode($clubHash));
        $viewer = Id64Bit::decode($this->hashId->decode($viewerHash));

        try {
            $this->assertType($club, Club::class);
            $this->assertType($viewer, Profile::class);
        } catch (\InvalidArgumentException $ex) {
            throw new BadRequestHttpException();
        }

        $edgeTypes = $this->edgeRepository->getRelationship($viewer, $club);

        if (
            in_array(Edge::TYPE_BLOCKED_BY, $edgeTypes) ||
            !in_array(Edge::TYPE_FOLLOWED, $edgeTypes)
        ) {
            throw new AccessDeniedException();
        }
    }

    public function approveList(string $clubHash, string $viewerHash)
    {
        $club = Id64Bit::decode($this->hashId->decode($clubHash));
        $viewer = Id64Bit::decode($this->hashId->decode($viewerHash));

        try {
            $this->assertType($club, Club::class);
            $this->assertType($viewer, Profile::class);
        } catch (\InvalidArgumentException $ex) {
            throw new BadRequestHttpException();
        }

        $edgeTypes = $this->edgeRepository->getRelationship($viewer, $club);

        if (in_array(Edge::TYPE_BLOCKED_BY, $edgeTypes)) {
            throw new AccessDeniedException();
        }
    }

    public function approveView(string $clubHash, string $topicHash, string $viewerHash)
    {
        $club = Id64Bit::decode($this->hashId->decode($clubHash));
        $topic = Id64Bit::decode($this->hashId->decode($topicHash));
        $viewer = Id64Bit::decode($this->hashId->decode($viewerHash));

        $this->permissions->list($viewer, $topic);

        try {
            $this->assertType($club, Club::class);
            $this->assertType($topic, Topic::class);
            $this->assertType($viewer, Profile::class);
        } catch (\InvalidArgumentException $ex) {
            throw new BadRequestHttpException();
        }

        if (!$this->edgeRepository->exists($club, Edge::TYPE_HOLDS, $topic)) {
            throw new NotFoundException();
        }

        $edgeTypes = $this->edgeRepository->getRelationship($viewer, $club);

        if (in_array(Edge::TYPE_BLOCKED_BY, $edgeTypes)) {
            throw new AccessDeniedException();
        }
    }

    public function approveDelete(string $clubHash, string $topicHash, string $viewerHash)
    {
        $club = Id64Bit::decode($this->hashId->decode($clubHash));
        $topic = Id64Bit::decode($this->hashId->decode($topicHash));
        $viewer = Id64Bit::decode($this->hashId->decode($viewerHash));

        try {
            $this->assertType($club, Club::class);
            $this->assertType($topic, Topic::class);
            $this->assertType($viewer, Profile::class);
        } catch (\InvalidArgumentException $ex) {
            throw new BadRequestHttpException();
        }

        if (!$this->edgeRepository->exists($club, Edge::TYPE_HOLDS, $topic)) {
            throw new NotFoundException();
        }

        $clubRelations = $this->edgeRepository->getRelationship($viewer, $club);

        if (
            in_array(Edge::TYPE_BLOCKED_BY, $clubRelations) ||
            !in_array(Edge::TYPE_FOLLOWED, $clubRelations)
        ) {
            throw new AccessDeniedException();
        }

        if (in_array(Edge::TYPE_MODERATES, $clubRelations)) {
            return true;
        }

        $topicRelations = $this->edgeRepository->getRelationship($viewer, $topic);

        if (!in_array(Edge::TYPE_CREATED, $topicRelations)) {
            throw new AccessDeniedException();
        }
    }

    public function approveEdit(string $clubHash, string $topicHash, string $viewerHash)
    {
        $club = Id64Bit::decode($this->hashId->decode($clubHash));
        $topic = Id64Bit::decode($this->hashId->decode($topicHash));
        $viewer = Id64Bit::decode($this->hashId->decode($viewerHash));

        try {
            $this->assertType($club, Club::class);
            $this->assertType($topic, Topic::class);
            $this->assertType($viewer, Profile::class);
        } catch (\InvalidArgumentException $ex) {
            throw new BadRequestHttpException();
        }

        if (!$this->edgeRepository->exists($club, Edge::TYPE_HOLDS, $topic)) {
            throw new NotFoundException();
        }

        $edgeTypes =  $this->edgeRepository->getRelationship($viewer, $topic);

        if (!in_array(Edge::TYPE_CREATED, $edgeTypes)) {
            throw new AccessDeniedException();
        }
    }

    public function approveLock(string $clubHash, string $topicHash, string $viewerHash)
    {
        $club = Id64Bit::decode($this->hashId->decode($clubHash));
        $topic = Id64Bit::decode($this->hashId->decode($topicHash));
        $viewer = Id64Bit::decode($this->hashId->decode($viewerHash));

        try {
            $this->assertType($club, Club::class);
            $this->assertType($topic, Topic::class);
            $this->assertType($viewer, Profile::class);
        } catch (\InvalidArgumentException $ex) {
            throw new BadRequestHttpException();
        }

        if (!$this->edgeRepository->exists($club, Edge::TYPE_HOLDS, $topic)) {
            throw new NotFoundException();
        }

        $clubRelation =  $this->edgeRepository->getRelationship($viewer, $club);

        if (!in_array(Edge::TYPE_MODERATES, $clubRelation)) {
            throw new AccessDeniedException();
        }
    }

    private function assertType(Id64Bit $id, string $node)
    {
        if ($id->getType() !== $node::NODE_TYPE) {
            throw new \InvalidArgumentException();
        }
    }
}