<?php
namespace App\Service\AccessControl;

use App\Edge\Edge;
use App\Exception\AccessDeniedException;
use App\Exception\NotFoundException;
use App\Helper\Id64Bit;
use App\Node\Club;
use App\Node\Comment;
use App\Node\Profile;
use App\Node\Reply;
use App\Node\Topic;
use App\Repository\EdgeRepository;
use App\Service\HashIdService;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class TopicReplyAccessControl
{
    private $hashId;
    private $edgeRepository;

    public function __construct(HashIdService $idEncoder, EdgeRepository $repository)
    {
        $this->hashId = $idEncoder;
        $this->edgeRepository = $repository;
    }

    public function approveView(string $clubHash, string $topicHash, string $commentHash, string $replyHash, string $viewerHash)
    {
        $club = Id64Bit::decode($this->hashId->decode($clubHash));
        $topic = Id64Bit::decode($this->hashId->decode($topicHash));
        $comment = Id64Bit::decode($this->hashId->decode($commentHash));
        $reply = Id64Bit::decode($this->hashId->decode($replyHash));
        $viewer = Id64Bit::decode($this->hashId->decode($viewerHash));

        try {
            $this->assertType($club, Club::class);
            $this->assertType($topic, Topic::class);
            $this->assertType($comment, Comment::class);
            $this->assertType($reply, Reply::class);
            $this->assertType($viewer, Profile::class);
        } catch (\InvalidArgumentException $ex) {
            throw new BadRequestHttpException();
        }

        if (!$this->edgeRepository->exists($club, Edge::TYPE_HOLDS, $topic)) {
            throw new NotFoundException();
        }

        if (!$this->edgeRepository->exists($topic, Edge::TYPE_HOLDS, $comment)) {
            throw new NotFoundException();
        }

        if (!$this->edgeRepository->exists($comment, Edge::TYPE_HOLDS, $reply)) {
            throw new NotFoundException();
        }

        $clubRelations = $this->edgeRepository->getRelationship($viewer, $club);

        if (in_array(Edge::TYPE_BLOCKED_BY, $clubRelations)) {
            throw new AccessDeniedException();
        }
    }

    public function approveCreate(string $clubHash, string $topicHash, string $commentHash, string $viewerHash)
    {
        $club = Id64Bit::decode($this->hashId->decode($clubHash));
        $topic = Id64Bit::decode($this->hashId->decode($topicHash));
        $comment = Id64Bit::decode($this->hashId->decode($commentHash));
        $viewer = Id64Bit::decode($this->hashId->decode($viewerHash));

        try {
            $this->assertType($club, Club::class);
            $this->assertType($topic, Topic::class);
            $this->assertType($comment, Comment::class);
            $this->assertType($viewer, Profile::class);
        } catch (\InvalidArgumentException $ex) {
            throw new BadRequestHttpException();
        }

        if (!$this->edgeRepository->exists($club, Edge::TYPE_HOLDS, $topic)) {
            throw new NotFoundException();
        }

        if (!$this->edgeRepository->exists($topic, Edge::TYPE_HOLDS, $comment)) {
            throw new NotFoundException();
        }

        $edgeTypes = $this->edgeRepository->getRelationship($viewer, $club);

        if (
            in_array(Edge::TYPE_BLOCKED_BY, $edgeTypes) ||
            !in_array(Edge::TYPE_FOLLOWED, $edgeTypes)
        ) {
            throw new AccessDeniedException();
        }
    }

    public function approveList(string $clubHash, string $topicHash, string $commentHash, string $viewerHash)
    {
        $club = Id64Bit::decode($this->hashId->decode($clubHash));
        $topic = Id64Bit::decode($this->hashId->decode($topicHash));
        $comment = Id64Bit::decode($this->hashId->decode($commentHash));
        $viewer = Id64Bit::decode($this->hashId->decode($viewerHash));

        try {
            $this->assertType($club, Club::class);
            $this->assertType($topic, Topic::class);
            $this->assertType($comment, Comment::class);
            $this->assertType($viewer, Profile::class);
        } catch (\InvalidArgumentException $ex) {
            throw new BadRequestHttpException();
        }

        if (!$this->edgeRepository->exists($club, Edge::TYPE_HOLDS, $topic)) {
            throw new NotFoundException();
        }

        if (!$this->edgeRepository->exists($topic, Edge::TYPE_HOLDS, $comment)) {
            throw new NotFoundException();
        }

        $edgeTypes = $this->edgeRepository->getRelationship($viewer, $club);

        if (in_array(Edge::TYPE_BLOCKED_BY, $edgeTypes)) {
            throw new AccessDeniedException();
        }
    }

    public function approveDelete(
        string $clubHash,
        string $topicHash,
        string $commentHash,
        string $replyHash,
        string $viewerHash
    ) {
        $club = Id64Bit::decode($this->hashId->decode($clubHash));
        $topic = Id64Bit::decode($this->hashId->decode($topicHash));
        $comment = Id64Bit::decode($this->hashId->decode($commentHash));
        $reply = Id64Bit::decode($this->hashId->decode($replyHash));
        $viewer = Id64Bit::decode($this->hashId->decode($viewerHash));

        try {
            $this->assertType($club, Club::class);
            $this->assertType($topic, Topic::class);
            $this->assertType($comment, Comment::class);
            $this->assertType($reply, Reply::class);
            $this->assertType($viewer, Profile::class);
        } catch (\InvalidArgumentException $ex) {
            throw new BadRequestHttpException();
        }

        if (!$this->edgeRepository->exists($club, Edge::TYPE_HOLDS, $topic)) {
            throw new NotFoundException();
        }

        if (!$this->edgeRepository->exists($topic, Edge::TYPE_HOLDS, $comment)) {
            throw new NotFoundException();
        }

        if (!$this->edgeRepository->exists($topic, Edge::TYPE_HOLDS, $reply)) {
            throw new NotFoundException();
        }

        $clubRelations = $this->edgeRepository->getRelationship($viewer, $club);

        if (
            in_array(Edge::TYPE_BLOCKED_BY, $clubRelations) ||
            !in_array(Edge::TYPE_FOLLOWED, $clubRelations)
        ) {
            throw new AccessDeniedException();
        }

        if (in_array(Edge::TYPE_MODERATES, $clubRelations)) {
            return true;
        }

        $replyRelation = $this->edgeRepository->getRelationship($viewer, $reply);

        if (!in_array(Edge::TYPE_CREATED, $replyRelation)) {
            throw new AccessDeniedException();
        }
    }

    public function approveEdit(
        string $clubHash,
        string $topicHash,
        string $commentHash,
        string $replyHash,
        string $viewerHash
    ) {
        $club = Id64Bit::decode($this->hashId->decode($clubHash));
        $topic = Id64Bit::decode($this->hashId->decode($topicHash));
        $comment = Id64Bit::decode($this->hashId->decode($commentHash));
        $reply = Id64Bit::decode($this->hashId->decode($replyHash));
        $viewer = Id64Bit::decode($this->hashId->decode($viewerHash));

        try {
            $this->assertType($club, Club::class);
            $this->assertType($topic, Topic::class);
            $this->assertType($comment, Comment::class);
            $this->assertType($reply, Reply::class);
            $this->assertType($viewer, Profile::class);
        } catch (\InvalidArgumentException $ex) {
            throw new BadRequestHttpException();
        }

        if (!$this->edgeRepository->exists($club, Edge::TYPE_HOLDS, $topic)) {
            throw new NotFoundException();
        }

        if (!$this->edgeRepository->exists($topic, Edge::TYPE_HOLDS, $comment)) {
            throw new NotFoundException();
        }

        if (!$this->edgeRepository->exists($comment, Edge::TYPE_HOLDS, $reply)) {
            throw new NotFoundException();
        }

        $edgeTypes =  $this->edgeRepository->getRelationship($viewer, $reply);

        if (!in_array(Edge::TYPE_CREATED, $edgeTypes)) {
            throw new AccessDeniedException();
        }
    }

    private function assertType(Id64Bit $id, string $node)
    {
        if ($id->getType() !== $node::NODE_TYPE) {
            throw new \InvalidArgumentException();
        }
    }
}