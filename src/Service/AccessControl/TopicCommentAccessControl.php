<?php
namespace App\Service\AccessControl;

use App\Edge\Edge;
use App\Exception\AccessDeniedException;
use App\Exception\NotFoundException;
use App\Helper\Id64Bit;
use App\Node\Club;
use App\Node\Comment;
use App\Node\Profile;
use App\Node\Topic;
use App\Repository\EdgeRepository;
use App\Service\HashIdService;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class TopicCommentAccessControl
{
    private $hashId;
    private $edgeRepository;

    public function __construct(HashIdService $idEncoder, EdgeRepository $repository)
    {
        $this->hashId = $idEncoder;
        $this->edgeRepository = $repository;
    }

    public function approveCreate(string $clubHash, string $topicHash, string $viewerHash)
    {
        $clubId = Id64Bit::decode($this->hashId->decode($clubHash));
        $topicId = Id64Bit::decode($this->hashId->decode($topicHash));
        $viewerId = Id64Bit::decode($this->hashId->decode($viewerHash));

        try {
            $this->assertType($clubId, Club::class);
            $this->assertType($topicId, Topic::class);
            $this->assertType($viewerId, Profile::class);
        } catch (\InvalidArgumentException $ex) {
            throw new BadRequestHttpException();
        }

        if (!$this->edgeRepository->exists($clubId, Edge::TYPE_HOLDS, $topicId)) {
            throw new NotFoundException();
        }

        $edgeTypes = $this->edgeRepository->getRelationship($viewerId, $clubId);

        if (
            in_array(Edge::TYPE_BLOCKED_BY, $edgeTypes) ||
            !in_array(Edge::TYPE_FOLLOWED, $edgeTypes)
        ) {
            throw new AccessDeniedException();
        }
    }

    public function approveList(string $clubHash, string $topicHash, string $viewerHash)
    {
        $clubId = Id64Bit::decode($this->hashId->decode($clubHash));
        $topicId = Id64Bit::decode($this->hashId->decode($topicHash));
        $viewerId = Id64Bit::decode($this->hashId->decode($viewerHash));

        try {
            $this->assertType($clubId, Club::class);
            $this->assertType($topicId, Topic::class);
            $this->assertType($viewerId, Profile::class);
        } catch (\InvalidArgumentException $ex) {
            throw new BadRequestHttpException();
        }

        if (!$this->edgeRepository->exists($clubId, Edge::TYPE_HOLDS, $topicId)) {
            throw new NotFoundException();
        }

        $edgeTypes = $this->edgeRepository->getRelationship($viewerId, $clubId);

        if (in_array(Edge::TYPE_BLOCKED_BY, $edgeTypes)) {
            throw new AccessDeniedException();
        }
    }

    public function approveView(string $clubHash, string $topicHash, string $commentHash, string $viewerHash)
    {
        $clubId = Id64Bit::decode($this->hashId->decode($clubHash));
        $topicId = Id64Bit::decode($this->hashId->decode($topicHash));
        $viewerId = Id64Bit::decode($this->hashId->decode($viewerHash));
        $commentId = Id64Bit::decode($this->hashId->decode($commentHash));

        try {
            $this->assertType($clubId, Club::class);
            $this->assertType($topicId, Topic::class);
            $this->assertType($commentId, Comment::class);
            $this->assertType($viewerId, Profile::class);
        } catch (\InvalidArgumentException $ex) {
            throw new BadRequestHttpException();
        }

        if (!$this->edgeRepository->exists($clubId, Edge::TYPE_HOLDS, $topicId)) {
            throw new NotFoundException();
        }

        if (!$this->edgeRepository->exists($topicId, Edge::TYPE_HOLDS, $commentId)) {
            throw new NotFoundException();
        }

        $edgeTypes = $this->edgeRepository->getRelationship($viewerId, $clubId);

        if (in_array(Edge::TYPE_BLOCKED_BY, $edgeTypes)) {
            throw new AccessDeniedException();
        }
    }

    public function approveDelete(string $clubHash, string $topicHash, string $commentHash, string $viewerHash)
    {
        $clubId = Id64Bit::decode($this->hashId->decode($clubHash));
        $topicId = Id64Bit::decode($this->hashId->decode($topicHash));
        $viewerId = Id64Bit::decode($this->hashId->decode($viewerHash));
        $commentId = Id64Bit::decode($this->hashId->decode($commentHash));

        try {
            $this->assertType($clubId, Club::class);
            $this->assertType($topicId, Topic::class);
            $this->assertType($commentId, Comment::class);
            $this->assertType($viewerId, Profile::class);
        } catch (\InvalidArgumentException $ex) {
            throw new BadRequestHttpException();
        }

        if (!$this->edgeRepository->exists($clubId, Edge::TYPE_HOLDS, $topicId)) {
            throw new NotFoundException();
        }

        if (!$this->edgeRepository->exists($topicId, Edge::TYPE_HOLDS, $commentId)) {
            throw new NotFoundException();
        }

        $clubRelations = $this->edgeRepository->getRelationship($viewerId, $clubId);

        if (
            in_array(Edge::TYPE_BLOCKED_BY, $clubRelations) ||
            !in_array(Edge::TYPE_FOLLOWED, $clubRelations)
        ) {
            throw new AccessDeniedException();
        }

        if (in_array(Edge::TYPE_MODERATES, $clubRelations)) {
            return true;
        }

        $topicRelations = $this->edgeRepository->getRelationship($viewerId, $commentId);

        if (!in_array(Edge::TYPE_CREATED, $topicRelations)) {
            throw new AccessDeniedException();
        }
    }

    public function approveEdit(string $clubHash, string $topicHash, string $commentHash, string $viewerHash)
    {
        $clubId = Id64Bit::decode($this->hashId->decode($clubHash));
        $topicId = Id64Bit::decode($this->hashId->decode($topicHash));
        $viewerId = Id64Bit::decode($this->hashId->decode($viewerHash));
        $commentId = Id64Bit::decode($this->hashId->decode($commentHash));

        try {
            $this->assertType($clubId, Club::class);
            $this->assertType($topicId, Topic::class);
            $this->assertType($commentId, Comment::class);
            $this->assertType($viewerId, Profile::class);
        } catch (\InvalidArgumentException $ex) {
            throw new BadRequestHttpException();
        }

        if (!$this->edgeRepository->exists($clubId, Edge::TYPE_HOLDS, $topicId)) {
            throw new NotFoundException();
        }

        if (!$this->edgeRepository->exists($topicId, Edge::TYPE_HOLDS, $commentId)) {
            throw new NotFoundException();
        }

        $edgeTypes =  $this->edgeRepository->getRelationship($viewerId, $commentId);

        if (!in_array(Edge::TYPE_CREATED, $edgeTypes)) {
            throw new AccessDeniedException();
        }
    }

    private function assertType(Id64Bit $id, string $node)
    {
        if ($id->getType() !== $node::NODE_TYPE) {
            throw new \InvalidArgumentException();
        }
    }
}