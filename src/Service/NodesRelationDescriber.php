<?php
namespace App\Service;

use App\Helper\Id64Bit;
use App\Repository\EdgeRepository;

class NodesRelationDescriber
{
    private $edgeRepository;
    private $shardManager;

    public function __construct(
        EdgeRepository $edgeRepository,
        ShardManager $shardManager
    ) {
        $this->edgeRepository = $edgeRepository;
        $this->shardManager = $shardManager;
    }

    public function describe(Id64Bit $viewerGlobalId, Id64Bit $nodeGlobalId)
    {
        return $this->edgeRepository->getRelationship($viewerGlobalId, $nodeGlobalId);
    }
}