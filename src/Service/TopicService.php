<?php
namespace App\Service;

use App\Edge\Edge;
use App\Exception\AccessDeniedException;
use App\Exception\NotFoundException;
use App\Helper\Id64Bit;
use App\Node\Media;
use App\Node\Topic;
use App\Repository\EdgeRepository;
use App\Repository\NodeRepository;
use App\Repository\SearchRepository;
use App\Request\NodeRelatedRequest;
use App\Request\Topic\CreateTopicRequest;
use App\Request\Topic\DeleteTopicRequest;
use App\Request\Topic\TopicRequest;
use App\Request\Topic\ListTopicsRequest;
use App\Request\Topic\UpdateTopicRequest;
use App\Service\Permissions\TopicPermissions;
use App\ViewModel\HashViewModel;
use App\ViewModel\MinimalProfileViewModel;
use App\ViewModel\TopicListViewModel;
use App\ViewModel\TopicViewModel;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;

class TopicService
{
    private $nodeRepository;
    private $edgeRepository;
    private $idEncoder;
    private $likeService;
    private $commentService;
    private $searchRepository;
    private $blockListService;
    private $mediaService;
    private $permissions;
    private $followService;

    public function __construct(
        HashIdService $hashIdService,
        NodeRepository $nodeRepository,
        EdgeRepository $edgeRepository,
        LikeService $likeService,
        CommentService $commentService,
        SearchRepository $searchRepository,
        BlockListService $blockListService,
        MediaService $mediaService,
        TopicPermissions $topicPermissions,
        FollowService $followService
    ) {
        $this->nodeRepository = $nodeRepository;
        $this->edgeRepository = $edgeRepository;
        $this->idEncoder = $hashIdService;
        $this->likeService = $likeService;
        $this->commentService = $commentService;
        $this->searchRepository = $searchRepository;
        $this->blockListService = $blockListService;
        $this->mediaService = $mediaService;
        $this->permissions = $topicPermissions;
        $this->followService = $followService;
    }

    public function view(TopicRequest $model, string $viewerHash): TopicViewModel
    {
        $topicId = Id64Bit::decode($this->idEncoder->decode($model->getTopic()));
        $viewerId = Id64Bit::decode($this->idEncoder->decode($viewerHash));

        if ($topicId->getType() !== Topic::NODE_TYPE) {
            throw new NotFoundException();
        }

        /** @var Topic $node */
        $node = $this->nodeRepository->findOne($topicId);

        $ownerId = $this->edgeRepository->findCreatorId($topicId);
        $ownerNode = $this->nodeRepository->findOne(Id64Bit::decode($ownerId));
        $owner = new MinimalProfileViewModel(
            $this->idEncoder->encode($ownerId),
            $ownerNode
        );

        return new TopicViewModel(
            $model->getTopic(),
            $node,
            $owner,
            $this->mediaService->get($topicId),
            $this->likeService->countFor($topicId, $viewerId),
            $this->commentService->countFor($topicId, $viewerId),
            $this->permissions->list($viewerId, $topicId)
        );
    }

    public function list(ListTopicsRequest $request, string $viewerHash)
    {
        $clubId = Id64Bit::decode($this->idEncoder->decode($request->getClub()));
        $viewerId = Id64Bit::decode($this->idEncoder->decode($viewerHash));

        $topicEdges = $this->edgeRepository->find(
            $clubId,
            Edge::TYPE_HOLDS,
            Topic::NODE_TYPE,
            $request->getPage(),
            $request->getLimit()
        );

        $topicsCount = $this->edgeRepository->count($clubId, Edge::TYPE_HOLDS, Topic::NODE_TYPE);

        if (empty($topicEdges)) {
            return [];
        }

        $topicIds = [];

        /** @var Edge $edge */
        foreach ($topicEdges as $edge) {
            $id = Id64Bit::decode($edge->getInverseNode());
            $topicIds[] = $id;
        }

        $topics = [];
        $topicNodes = $this->nodeRepository->find($topicIds);

        uasort($topicNodes, function($a, $b){
            if ($a->getCreated() === $b->getCreated()) {
                return 0;
            }

            return $a->getCreated() < $b->getCreated() ? -1 : 1;
        });

        /** @var Topic $node */
        foreach ($topicNodes as $id => $node) {
            $id64Bit = Id64Bit::decode($id);
            $encodedId = $this->idEncoder->encode($id);

            $ownerId = $this->edgeRepository->findCreatorId($id64Bit);
            $ownerNode = $this->nodeRepository->findOne(Id64Bit::decode($ownerId));

            $owner = new MinimalProfileViewModel(
                $this->idEncoder->encode($ownerId),
                $ownerNode
            );

            $topics[] = new TopicViewModel(
                $encodedId,
                $node,
                $owner,
                $this->mediaService->get($id64Bit),
                $this->likeService->countFor($id64Bit, $viewerId),
                $this->commentService->countFor($id64Bit, $viewerId),
                $this->permissions->list($viewerId, $id64Bit)
            );
        }

        return new TopicListViewModel($topicsCount, $topics);
    }

    public function create(CreateTopicRequest $request, string $profileHash)
    {
        $profileId = Id64Bit::decode($this->idEncoder->decode($profileHash));
        $clubId = Id64Bit::decode($this->idEncoder->decode($request->getClub()));

        $topic = (new Topic())
            ->setTitle($request->getTitle())
            ->setText($request->getText());

        $topicId = $this->nodeRepository->addToShard($topic, $profileId->getShard());
        $this->edgeRepository->add($clubId, Edge::TYPE_HOLDS, $topicId);
        $this->edgeRepository->add($profileId, Edge::TYPE_CREATED, $topicId);
        $this->followService->follow($topicId, $profileId);

        $this->searchRepository->index($topicId, $topic, $profileId->getGlobal());

        if (!empty($request->getMedia())) {
            $this->mediaService->add($request->getMedia(), $topicId, $profileId);
        }

        return new HashViewModel($this->idEncoder->encode($topicId->getGlobal()));
    }

    public function update(UpdateTopicRequest $request, string $profileHash)
    {
        $profileId = Id64Bit::decode($this->idEncoder->decode($profileHash));
        $topicId = Id64Bit::decode($this->idEncoder->decode($request->getTopic()));

        /** @var Topic $topic */
        $topic = $this->nodeRepository->findOne($topicId);

        if ($topic->isLocked()) {
            throw new AccessDeniedException();
        }

        $topic->setTitle($request->getTitle())
            ->setText($request->getText());

        $this->nodeRepository->update($topicId, $topic);
        $this->searchRepository->updateIndex($topicId, $topic);

        if (!empty($request->getMedia())) {
            $this->mediaService->add($request->getMedia(), $topicId, $profileId);
        }
    }

    public function unlock(string $topicHash, string $viewerHash)
    {
        $topicId = Id64Bit::decode($this->idEncoder->decode($topicHash));
        $viewerId = Id64Bit::decode($this->idEncoder->decode($viewerHash));
        /** @var Topic $topic */
        $topic = $this->nodeRepository->findOne($topicId);

        if ($topic->isLocked()) {
            $topic->unlock();
            $this->nodeRepository->update($topicId, $topic);
        }

        return $this->permissions->list($viewerId, $topicId);
    }

    public function lock(string $topicHash, string $viewerHash)
    {
        $topicId = Id64Bit::decode($this->idEncoder->decode($topicHash));
        $viewerId = Id64Bit::decode($this->idEncoder->decode($viewerHash));
        /** @var Topic $topic */
        $topic = $this->nodeRepository->findOne($topicId);

        if (!$topic->isLocked()) {
            $topic->lock();
            $this->nodeRepository->update($topicId, $topic);
        }

        return $this->permissions->list($viewerId, $topicId);
    }

    public function follow(string $topic, string $viewer)
    {
        $topicId = Id64Bit::decode($this->idEncoder->decode($topic));
        $viewerId = Id64Bit::decode($this->idEncoder->decode($viewer));
        $this->followService->follow($topicId, $viewerId);

        return $this->permissions->list($viewerId, $topicId);
    }

    public function unfollow(string $topic, string $viewer)
    {
        $topicId = Id64Bit::decode($this->idEncoder->decode($topic));
        $viewerId = Id64Bit::decode($this->idEncoder->decode($viewer));

        $this->followService->unfollow($topic, $viewer);

        return $this->permissions->list($viewerId, $topicId);
    }

    public function delete(TopicRequest $request, string $profileHash)
    {
        //@todo - implement
    }
}