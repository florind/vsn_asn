<?php
namespace App\Service;

use App\Edge\Edge;
use App\Exception\ProcessRequestException;
use App\Helper\Id64Bit;
use App\Node\Media;
use App\Repository\EdgeRepository;
use App\Repository\NodeRepository;
use App\ViewModel\MediaViewModel;
use Doctrine\DBAL\DBALException;

class MediaService
{
    private $idEncoder;
    private $edgeRepository;
    private $nodeRepository;

    public function __construct(
        HashIdService $hashIdService,
        EdgeRepository $edgeRepository,
        NodeRepository $nodeRepository
    ) {
        $this->idEncoder = $hashIdService;
        $this->edgeRepository = $edgeRepository;
        $this->nodeRepository = $nodeRepository;
    }

    public function add(array $media, Id64Bit $nodeId, Id64Bit $profileId)
    {
        foreach ($media as $filename) {
            $mediaId = $this->nodeRepository->addToShard(
                (new Media())->setFilename($filename),
                $nodeId->getShard()
            );

            try {
                $this->edgeRepository->add($profileId, Edge::TYPE_CREATED, $mediaId);
                $this->edgeRepository->add($nodeId, Edge::TYPE_HOLDS, $mediaId);
            } catch (DBALException $ex) {
                throw new ProcessRequestException();
            }
        }
    }

    public function get(Id64Bit $nodeId)
    {
        $edges = $this->edgeRepository->find($nodeId, Edge::TYPE_HOLDS, Media::NODE_TYPE);
        $ids = $media = [];

        /** @var Edge $edge */
        foreach ($edges as $edge) {
            $ids[] = Id64Bit::decode($edge->getInverseNode());
        }

        $nodes = $this->nodeRepository->find($ids);

        /** @var Media $node */
        foreach ($nodes as $id => $node) {
            $encodedId = $this->idEncoder->encode($id);
            $media[] = new MediaViewModel($encodedId, $node);
        }

        return $media;
    }

    public function delete(Id64Bit $mediaId, Id64Bit $profileId, Id64Bit $nodeId)
    {
        try {
            $this->edgeRepository->drop($profileId, Edge::TYPE_CREATED, $mediaId);
            $this->edgeRepository->drop($nodeId, Edge::TYPE_HOLDS, $mediaId);
            $this->nodeRepository->drop($mediaId);
        } catch (DBALException $ex) {
            throw new ProcessRequestException();
        }
    }
}