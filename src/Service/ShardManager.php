<?php
namespace App\Service;

use Doctrine\Bundle\DoctrineBundle\ConnectionFactory;
use Doctrine\DBAL\Connection;

class ShardManager
{
    private $connectionFactory;
    private $config;
    private $shards = [];
    private $closed = [];
    private $delta;
    private $openConnections = [];

    public function __construct(ConnectionFactory $connectionFactory, array $config)
    {
        $this->connectionFactory = $connectionFactory;
        $this->config = $config;

        foreach ($config['shards'] as $index => $entry) {
            foreach (range($entry['range'][0], $entry['range'][1]) as $shard) {
                $this->shards[] = $shard;
            }
        }

        sort($this->shards);
        $this->delta = (max($this->shards) + 1);
        $this->closed = array_key_exists('closed', $config) ? $config['closed'] : [];
    }

    public function getStorageShard(int $id)
    {
        $shard = $id % $this->delta;

        if (!$this->available($shard)) {
            $shardsCopy = $this->shards;
            $shard = array_pop($shardsCopy);

            while (count($shardsCopy) && !$this->available($shard)) {
                $shard = array_pop($shardsCopy);
            }

            if (!$this->available($shard)) {
                throw new \RuntimeException('Shard misconfiguration! Either no shards are defined or they are all closed!');
            }
        }

        return $shard;
    }

    public function connect(int $shard): Connection
    {
        if (array_key_exists($shard, $this->openConnections)) {
            return $this->openConnections[$shard];
        }

        $connection = null;

        if (!in_array($shard, $this->shards)) {
            throw new \RuntimeException(sprintf('Shard %d not found!', $shard));
        }

        foreach ($this->config['shards'] as $host) {
            if (in_array($shard, $host['range'])) {
                $connection = $host['connection'];
                break;
            }
        }

        $shardUrl = sprintf($connection, $shard);
        $shardConnection = $this->connectionFactory->createConnection(['url' => $shardUrl]);

        $this->openConnections[$shard] = $shardConnection;

        return $shardConnection;
    }

    public function available($shard)
    {
        return in_array($shard, $this->shards) && !in_array($shard, $this->closed);
    }
}