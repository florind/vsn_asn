<?php
namespace App\Service;

use Hashids\Hashids;

class HashIdService
{
    private $encoder;

    public function __construct(array $config)
    {
        $this->encoder = new Hashids($config['salt'], $config['length']);
    }

    public function encode(int $id): string
    {
        return $this->encoder->encode($id);
    }

    public function decode(string $hash): int
    {
        $result = $this->encoder->decode($hash);
        return reset($result);
    }
}