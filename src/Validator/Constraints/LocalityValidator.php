<?php
namespace App\Validator\Constraints;

use App\Repository\LocalityRepository;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class LocalityValidator extends ConstraintValidator
{
    private $repository;

    public function __construct(LocalityRepository $repository)
    {
        $this->repository = $repository;
    }

    public function validate($value, Constraint $constraint)
    {
        if (null === $value) {
            return;
        }

        if (!$this->repository->exists($value)) {
            $this->context->buildViolation($constraint->message)
                ->atPath('locality')
                ->addViolation();
        }
    }
}