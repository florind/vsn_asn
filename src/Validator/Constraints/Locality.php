<?php
namespace App\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class Locality extends Constraint
{
    public $message = 'ASN_ERROR_LOCALITY';
}