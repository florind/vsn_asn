<?php
namespace App\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class Vehicle extends Constraint
{
    public $message = 'ASN_ERROR_VEHICLE';

    public function getTargets()
    {
        return self::CLASS_CONSTRAINT;
    }
}