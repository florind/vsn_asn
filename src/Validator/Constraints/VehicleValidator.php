<?php
namespace App\Validator\Constraints;

use App\Repository\LocalityRepository;
use App\Repository\VehicleRepository;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class VehicleValidator extends ConstraintValidator
{
    private $repository;

    public function __construct(VehicleRepository $repository)
    {
        $this->repository = $repository;
    }

    public function validate($vehicle, Constraint $constraint)
    {
        if (
            null === $vehicle->getType() ||
            null === $vehicle->getBrand() ||
            null === $vehicle->getModel()
        ) {
            return;
        }

        if (!$this->repository->exists($vehicle->getType(), $vehicle->getBrand(), $vehicle->getModel())) {
            $this->context->buildViolation($constraint->message)
                ->atPath('vehicle')
                ->addViolation();
        }
    }
}